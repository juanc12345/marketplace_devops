/*
 * generated by Xtext
 */
package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.xtext.example.mydsl.services.PetrinetDslGrammarAccess;

@SuppressWarnings("all")
public abstract class AbstractPetrinetDslSyntacticSequencer extends AbstractSyntacticSequencer {

	protected PetrinetDslGrammarAccess grammarAccess;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (PetrinetDslGrammarAccess) access;
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if(ruleCall.getRule() == grammarAccess.getCOLONRule())
			return getCOLONToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getEQUALSRule())
			return getEQUALSToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getGREATERRule())
			return getGREATERToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getIDRule())
			return getIDToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getLESSRule())
			return getLESSToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getMAXDELAYRule())
			return getMAXDELAYToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getMINDELAYRule())
			return getMINDELAYToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getNETRule())
			return getNETToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getPNMLRule())
			return getPNMLToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getQUESTIONRule())
			return getQUESTIONToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getSLASHRule())
			return getSLASHToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getSOURCERule())
			return getSOURCEToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getSTRINGRule())
			return getSTRINGToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTARGETRule())
			return getTARGETToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTINPUTARCRule())
			return getTINPUTARCToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTOUTPUTARCRule())
			return getTOUTPUTARCToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTPLACERule())
			return getTPLACEToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTTRANSITIONRule())
			return getTTRANSITIONToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getTYPERule())
			return getTYPEToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getVERSIONRule())
			return getVERSIONToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getXMLRule())
			return getXMLToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getXMLNSRule())
			return getXMLNSToken(semanticObject, ruleCall, node);
		return "";
	}
	
	/**
	 * terminal COLON:
	 * ":";
	 */
	protected String getCOLONToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return ":";
	}
	
	/**
	 * terminal EQUALS:
	 * "=";
	 */
	protected String getEQUALSToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "=";
	}
	
	/**
	 * terminal GREATER:
	 * ">";
	 */
	protected String getGREATERToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return ">";
	}
	
	/**
	 * terminal ID:
	 * "id";
	 */
	protected String getIDToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "id";
	}
	
	/**
	 * terminal LESS:
	 * "<";
	 */
	protected String getLESSToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "<";
	}
	
	/**
	 * terminal MAXDELAY:
	 * "maxDelay";
	 */
	protected String getMAXDELAYToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "maxDelay";
	}
	
	/**
	 * terminal MINDELAY:
	 * "minDelay";
	 */
	protected String getMINDELAYToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "minDelay";
	}
	
	/**
	 * terminal NET:
	 * "net";
	 */
	protected String getNETToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "net";
	}
	
	/**
	 * terminal PNML:
	 * "pnml";
	 */
	protected String getPNMLToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "pnml";
	}
	
	/**
	 * terminal QUESTION:
	 * "?";
	 */
	protected String getQUESTIONToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "?";
	}
	
	/**
	 * terminal SLASH:
	 * "/";
	 */
	protected String getSLASHToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "/";
	}
	
	/**
	 * terminal SOURCE:
	 * "source";
	 */
	protected String getSOURCEToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "source";
	}
	
	/**
	 * terminal STRING	: 
	 * 			'"' ( '\\' .  | !('\\'|'"') )* '"' |
	 * 			"'" ( '\\' .  | !('\\'|"'") )* "'"
	 * 		;
	 */
	protected String getSTRINGToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "\"\"";
	}
	
	/**
	 * terminal TARGET:
	 * "target";
	 */
	protected String getTARGETToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "target";
	}
	
	/**
	 * terminal TINPUTARC:
	 * "inputarc";
	 */
	protected String getTINPUTARCToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "inputarc";
	}
	
	/**
	 * terminal TOUTPUTARC:
	 * "outputarc";
	 */
	protected String getTOUTPUTARCToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "outputarc";
	}
	
	/**
	 * terminal TPLACE:
	 * "place";
	 */
	protected String getTPLACEToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "place";
	}
	
	/**
	 * terminal TTRANSITION:
	 * "transition";
	 */
	protected String getTTRANSITIONToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "transition";
	}
	
	/**
	 * terminal TYPE:
	 * "type";
	 */
	protected String getTYPEToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "type";
	}
	
	/**
	 * terminal VERSION:
	 * "version";
	 */
	protected String getVERSIONToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "version";
	}
	
	/**
	 * terminal XML:
	 * "xml";
	 */
	protected String getXMLToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "xml";
	}
	
	/**
	 * terminal XMLNS:
	 * "xmlns";
	 */
	protected String getXMLNSToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "xmlns";
	}
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

}
