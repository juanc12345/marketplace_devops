package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.PetrinetDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPetrinetDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_LESS", "RULE_QUESTION", "RULE_XML", "RULE_VERSION", "RULE_EQUALS", "RULE_STRING", "RULE_GREATER", "RULE_PNML", "RULE_COLON", "RULE_XMLNS", "RULE_NET", "RULE_TYPE", "RULE_SLASH", "RULE_TINPUTARC", "RULE_ID", "RULE_SOURCE", "RULE_TARGET", "RULE_TPLACE", "RULE_TTRANSITION", "RULE_MAXDELAY", "RULE_MINDELAY", "RULE_TOUTPUTARC", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'.'"
    };
    public static final int RULE_MAXDELAY=23;
    public static final int RULE_NET=14;
    public static final int RULE_TPLACE=21;
    public static final int RULE_XMLNS=13;
    public static final int RULE_SOURCE=19;
    public static final int RULE_STRING=9;
    public static final int RULE_XML=6;
    public static final int RULE_GREATER=10;
    public static final int RULE_TINPUTARC=17;
    public static final int RULE_SL_COMMENT=28;
    public static final int RULE_TOUTPUTARC=25;
    public static final int RULE_VERSION=7;
    public static final int RULE_EQUALS=8;
    public static final int RULE_TARGET=20;
    public static final int RULE_LESS=4;
    public static final int EOF=-1;
    public static final int T__31=31;
    public static final int RULE_ID=18;
    public static final int RULE_WS=29;
    public static final int RULE_PNML=11;
    public static final int RULE_COLON=12;
    public static final int RULE_ANY_OTHER=30;
    public static final int RULE_SLASH=16;
    public static final int RULE_TYPE=15;
    public static final int RULE_INT=26;
    public static final int RULE_MINDELAY=24;
    public static final int RULE_QUESTION=5;
    public static final int RULE_ML_COMMENT=27;
    public static final int RULE_TTRANSITION=22;

    // delegates
    // delegators


        public InternalPetrinetDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPetrinetDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPetrinetDslParser.tokenNames; }
    public String getGrammarFileName() { return "../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g"; }



     	private PetrinetDslGrammarAccess grammarAccess;
     	
        public InternalPetrinetDslParser(TokenStream input, PetrinetDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "PetriNet";	
       	}
       	
       	@Override
       	protected PetrinetDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulePetriNet"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:67:1: entryRulePetriNet returns [EObject current=null] : iv_rulePetriNet= rulePetriNet EOF ;
    public final EObject entryRulePetriNet() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePetriNet = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:68:2: (iv_rulePetriNet= rulePetriNet EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:69:2: iv_rulePetriNet= rulePetriNet EOF
            {
             newCompositeNode(grammarAccess.getPetriNetRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePetriNet_in_entryRulePetriNet75);
            iv_rulePetriNet=rulePetriNet();

            state._fsp--;

             current =iv_rulePetriNet; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePetriNet85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePetriNet"


    // $ANTLR start "rulePetriNet"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:76:1: rulePetriNet returns [EObject current=null] : ( () this_LESS_1= RULE_LESS this_QUESTION_2= RULE_QUESTION this_XML_3= RULE_XML this_VERSION_4= RULE_VERSION this_EQUALS_5= RULE_EQUALS this_STRING_6= RULE_STRING this_QUESTION_7= RULE_QUESTION this_GREATER_8= RULE_GREATER this_LESS_9= RULE_LESS this_PNML_10= RULE_PNML this_COLON_11= RULE_COLON this_PNML_12= RULE_PNML this_XMLNS_13= RULE_XMLNS this_COLON_14= RULE_COLON this_PNML_15= RULE_PNML this_EQUALS_16= RULE_EQUALS this_STRING_17= RULE_STRING this_GREATER_18= RULE_GREATER this_LESS_19= RULE_LESS this_NET_20= RULE_NET this_TYPE_21= RULE_TYPE this_EQUALS_22= RULE_EQUALS this_STRING_23= RULE_STRING this_GREATER_24= RULE_GREATER ( (lv_elements_25_0= ruleElement ) )* this_LESS_26= RULE_LESS this_SLASH_27= RULE_SLASH this_NET_28= RULE_NET this_GREATER_29= RULE_GREATER this_LESS_30= RULE_LESS this_SLASH_31= RULE_SLASH this_PNML_32= RULE_PNML this_COLON_33= RULE_COLON this_PNML_34= RULE_PNML this_GREATER_35= RULE_GREATER ) ;
    public final EObject rulePetriNet() throws RecognitionException {
        EObject current = null;

        Token this_LESS_1=null;
        Token this_QUESTION_2=null;
        Token this_XML_3=null;
        Token this_VERSION_4=null;
        Token this_EQUALS_5=null;
        Token this_STRING_6=null;
        Token this_QUESTION_7=null;
        Token this_GREATER_8=null;
        Token this_LESS_9=null;
        Token this_PNML_10=null;
        Token this_COLON_11=null;
        Token this_PNML_12=null;
        Token this_XMLNS_13=null;
        Token this_COLON_14=null;
        Token this_PNML_15=null;
        Token this_EQUALS_16=null;
        Token this_STRING_17=null;
        Token this_GREATER_18=null;
        Token this_LESS_19=null;
        Token this_NET_20=null;
        Token this_TYPE_21=null;
        Token this_EQUALS_22=null;
        Token this_STRING_23=null;
        Token this_GREATER_24=null;
        Token this_LESS_26=null;
        Token this_SLASH_27=null;
        Token this_NET_28=null;
        Token this_GREATER_29=null;
        Token this_LESS_30=null;
        Token this_SLASH_31=null;
        Token this_PNML_32=null;
        Token this_COLON_33=null;
        Token this_PNML_34=null;
        Token this_GREATER_35=null;
        EObject lv_elements_25_0 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:79:28: ( ( () this_LESS_1= RULE_LESS this_QUESTION_2= RULE_QUESTION this_XML_3= RULE_XML this_VERSION_4= RULE_VERSION this_EQUALS_5= RULE_EQUALS this_STRING_6= RULE_STRING this_QUESTION_7= RULE_QUESTION this_GREATER_8= RULE_GREATER this_LESS_9= RULE_LESS this_PNML_10= RULE_PNML this_COLON_11= RULE_COLON this_PNML_12= RULE_PNML this_XMLNS_13= RULE_XMLNS this_COLON_14= RULE_COLON this_PNML_15= RULE_PNML this_EQUALS_16= RULE_EQUALS this_STRING_17= RULE_STRING this_GREATER_18= RULE_GREATER this_LESS_19= RULE_LESS this_NET_20= RULE_NET this_TYPE_21= RULE_TYPE this_EQUALS_22= RULE_EQUALS this_STRING_23= RULE_STRING this_GREATER_24= RULE_GREATER ( (lv_elements_25_0= ruleElement ) )* this_LESS_26= RULE_LESS this_SLASH_27= RULE_SLASH this_NET_28= RULE_NET this_GREATER_29= RULE_GREATER this_LESS_30= RULE_LESS this_SLASH_31= RULE_SLASH this_PNML_32= RULE_PNML this_COLON_33= RULE_COLON this_PNML_34= RULE_PNML this_GREATER_35= RULE_GREATER ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:80:1: ( () this_LESS_1= RULE_LESS this_QUESTION_2= RULE_QUESTION this_XML_3= RULE_XML this_VERSION_4= RULE_VERSION this_EQUALS_5= RULE_EQUALS this_STRING_6= RULE_STRING this_QUESTION_7= RULE_QUESTION this_GREATER_8= RULE_GREATER this_LESS_9= RULE_LESS this_PNML_10= RULE_PNML this_COLON_11= RULE_COLON this_PNML_12= RULE_PNML this_XMLNS_13= RULE_XMLNS this_COLON_14= RULE_COLON this_PNML_15= RULE_PNML this_EQUALS_16= RULE_EQUALS this_STRING_17= RULE_STRING this_GREATER_18= RULE_GREATER this_LESS_19= RULE_LESS this_NET_20= RULE_NET this_TYPE_21= RULE_TYPE this_EQUALS_22= RULE_EQUALS this_STRING_23= RULE_STRING this_GREATER_24= RULE_GREATER ( (lv_elements_25_0= ruleElement ) )* this_LESS_26= RULE_LESS this_SLASH_27= RULE_SLASH this_NET_28= RULE_NET this_GREATER_29= RULE_GREATER this_LESS_30= RULE_LESS this_SLASH_31= RULE_SLASH this_PNML_32= RULE_PNML this_COLON_33= RULE_COLON this_PNML_34= RULE_PNML this_GREATER_35= RULE_GREATER )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:80:1: ( () this_LESS_1= RULE_LESS this_QUESTION_2= RULE_QUESTION this_XML_3= RULE_XML this_VERSION_4= RULE_VERSION this_EQUALS_5= RULE_EQUALS this_STRING_6= RULE_STRING this_QUESTION_7= RULE_QUESTION this_GREATER_8= RULE_GREATER this_LESS_9= RULE_LESS this_PNML_10= RULE_PNML this_COLON_11= RULE_COLON this_PNML_12= RULE_PNML this_XMLNS_13= RULE_XMLNS this_COLON_14= RULE_COLON this_PNML_15= RULE_PNML this_EQUALS_16= RULE_EQUALS this_STRING_17= RULE_STRING this_GREATER_18= RULE_GREATER this_LESS_19= RULE_LESS this_NET_20= RULE_NET this_TYPE_21= RULE_TYPE this_EQUALS_22= RULE_EQUALS this_STRING_23= RULE_STRING this_GREATER_24= RULE_GREATER ( (lv_elements_25_0= ruleElement ) )* this_LESS_26= RULE_LESS this_SLASH_27= RULE_SLASH this_NET_28= RULE_NET this_GREATER_29= RULE_GREATER this_LESS_30= RULE_LESS this_SLASH_31= RULE_SLASH this_PNML_32= RULE_PNML this_COLON_33= RULE_COLON this_PNML_34= RULE_PNML this_GREATER_35= RULE_GREATER )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:80:2: () this_LESS_1= RULE_LESS this_QUESTION_2= RULE_QUESTION this_XML_3= RULE_XML this_VERSION_4= RULE_VERSION this_EQUALS_5= RULE_EQUALS this_STRING_6= RULE_STRING this_QUESTION_7= RULE_QUESTION this_GREATER_8= RULE_GREATER this_LESS_9= RULE_LESS this_PNML_10= RULE_PNML this_COLON_11= RULE_COLON this_PNML_12= RULE_PNML this_XMLNS_13= RULE_XMLNS this_COLON_14= RULE_COLON this_PNML_15= RULE_PNML this_EQUALS_16= RULE_EQUALS this_STRING_17= RULE_STRING this_GREATER_18= RULE_GREATER this_LESS_19= RULE_LESS this_NET_20= RULE_NET this_TYPE_21= RULE_TYPE this_EQUALS_22= RULE_EQUALS this_STRING_23= RULE_STRING this_GREATER_24= RULE_GREATER ( (lv_elements_25_0= ruleElement ) )* this_LESS_26= RULE_LESS this_SLASH_27= RULE_SLASH this_NET_28= RULE_NET this_GREATER_29= RULE_GREATER this_LESS_30= RULE_LESS this_SLASH_31= RULE_SLASH this_PNML_32= RULE_PNML this_COLON_33= RULE_COLON this_PNML_34= RULE_PNML this_GREATER_35= RULE_GREATER
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:80:2: ()
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getPetriNetAccess().getPetriNetAction_0(),
                        current);
                

            }

            this_LESS_1=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePetriNet130); 
             
                newLeafNode(this_LESS_1, grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_1()); 
                
            this_QUESTION_2=(Token)match(input,RULE_QUESTION,FollowSets000.FOLLOW_RULE_QUESTION_in_rulePetriNet140); 
             
                newLeafNode(this_QUESTION_2, grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_2()); 
                
            this_XML_3=(Token)match(input,RULE_XML,FollowSets000.FOLLOW_RULE_XML_in_rulePetriNet150); 
             
                newLeafNode(this_XML_3, grammarAccess.getPetriNetAccess().getXMLTerminalRuleCall_3()); 
                
            this_VERSION_4=(Token)match(input,RULE_VERSION,FollowSets000.FOLLOW_RULE_VERSION_in_rulePetriNet160); 
             
                newLeafNode(this_VERSION_4, grammarAccess.getPetriNetAccess().getVERSIONTerminalRuleCall_4()); 
                
            this_EQUALS_5=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rulePetriNet170); 
             
                newLeafNode(this_EQUALS_5, grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_5()); 
                
            this_STRING_6=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rulePetriNet180); 
             
                newLeafNode(this_STRING_6, grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_6()); 
                
            this_QUESTION_7=(Token)match(input,RULE_QUESTION,FollowSets000.FOLLOW_RULE_QUESTION_in_rulePetriNet190); 
             
                newLeafNode(this_QUESTION_7, grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_7()); 
                
            this_GREATER_8=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePetriNet200); 
             
                newLeafNode(this_GREATER_8, grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_8()); 
                
            this_LESS_9=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePetriNet210); 
             
                newLeafNode(this_LESS_9, grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_9()); 
                
            this_PNML_10=(Token)match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rulePetriNet220); 
             
                newLeafNode(this_PNML_10, grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_10()); 
                
            this_COLON_11=(Token)match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rulePetriNet230); 
             
                newLeafNode(this_COLON_11, grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_11()); 
                
            this_PNML_12=(Token)match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rulePetriNet240); 
             
                newLeafNode(this_PNML_12, grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_12()); 
                
            this_XMLNS_13=(Token)match(input,RULE_XMLNS,FollowSets000.FOLLOW_RULE_XMLNS_in_rulePetriNet250); 
             
                newLeafNode(this_XMLNS_13, grammarAccess.getPetriNetAccess().getXMLNSTerminalRuleCall_13()); 
                
            this_COLON_14=(Token)match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rulePetriNet260); 
             
                newLeafNode(this_COLON_14, grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_14()); 
                
            this_PNML_15=(Token)match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rulePetriNet270); 
             
                newLeafNode(this_PNML_15, grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_15()); 
                
            this_EQUALS_16=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rulePetriNet280); 
             
                newLeafNode(this_EQUALS_16, grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_16()); 
                
            this_STRING_17=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rulePetriNet290); 
             
                newLeafNode(this_STRING_17, grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_17()); 
                
            this_GREATER_18=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePetriNet300); 
             
                newLeafNode(this_GREATER_18, grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_18()); 
                
            this_LESS_19=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePetriNet310); 
             
                newLeafNode(this_LESS_19, grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_19()); 
                
            this_NET_20=(Token)match(input,RULE_NET,FollowSets000.FOLLOW_RULE_NET_in_rulePetriNet320); 
             
                newLeafNode(this_NET_20, grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_20()); 
                
            this_TYPE_21=(Token)match(input,RULE_TYPE,FollowSets000.FOLLOW_RULE_TYPE_in_rulePetriNet330); 
             
                newLeafNode(this_TYPE_21, grammarAccess.getPetriNetAccess().getTYPETerminalRuleCall_21()); 
                
            this_EQUALS_22=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rulePetriNet340); 
             
                newLeafNode(this_EQUALS_22, grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_22()); 
                
            this_STRING_23=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rulePetriNet350); 
             
                newLeafNode(this_STRING_23, grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_23()); 
                
            this_GREATER_24=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePetriNet360); 
             
                newLeafNode(this_GREATER_24, grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_24()); 
                
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:182:1: ( (lv_elements_25_0= ruleElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_LESS) ) {
                    int LA1_1 = input.LA(2);

                    if ( (LA1_1==RULE_TINPUTARC||(LA1_1>=RULE_TPLACE && LA1_1<=RULE_TTRANSITION)||LA1_1==RULE_TOUTPUTARC) ) {
                        alt1=1;
                    }


                }


                switch (alt1) {
            	case 1 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:183:1: (lv_elements_25_0= ruleElement )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:183:1: (lv_elements_25_0= ruleElement )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:184:3: lv_elements_25_0= ruleElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPetriNetAccess().getElementsElementParserRuleCall_25_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleElement_in_rulePetriNet380);
            	    lv_elements_25_0=ruleElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPetriNetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_25_0, 
            	            		"Element");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            this_LESS_26=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePetriNet392); 
             
                newLeafNode(this_LESS_26, grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_26()); 
                
            this_SLASH_27=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rulePetriNet402); 
             
                newLeafNode(this_SLASH_27, grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_27()); 
                
            this_NET_28=(Token)match(input,RULE_NET,FollowSets000.FOLLOW_RULE_NET_in_rulePetriNet412); 
             
                newLeafNode(this_NET_28, grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_28()); 
                
            this_GREATER_29=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePetriNet422); 
             
                newLeafNode(this_GREATER_29, grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_29()); 
                
            this_LESS_30=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePetriNet432); 
             
                newLeafNode(this_LESS_30, grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_30()); 
                
            this_SLASH_31=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rulePetriNet442); 
             
                newLeafNode(this_SLASH_31, grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_31()); 
                
            this_PNML_32=(Token)match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rulePetriNet452); 
             
                newLeafNode(this_PNML_32, grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_32()); 
                
            this_COLON_33=(Token)match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rulePetriNet462); 
             
                newLeafNode(this_COLON_33, grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_33()); 
                
            this_PNML_34=(Token)match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rulePetriNet472); 
             
                newLeafNode(this_PNML_34, grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_34()); 
                
            this_GREATER_35=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePetriNet482); 
             
                newLeafNode(this_GREATER_35, grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_35()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePetriNet"


    // $ANTLR start "entryRuleElement"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:248:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:249:2: (iv_ruleElement= ruleElement EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:250:2: iv_ruleElement= ruleElement EOF
            {
             newCompositeNode(grammarAccess.getElementRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleElement_in_entryRuleElement517);
            iv_ruleElement=ruleElement();

            state._fsp--;

             current =iv_ruleElement; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleElement527); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:257:1: ruleElement returns [EObject current=null] : (this_OutputArc_0= ruleOutputArc | this_InputArc_1= ruleInputArc | this_Transition_2= ruleTransition | this_Place_3= rulePlace ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        EObject this_OutputArc_0 = null;

        EObject this_InputArc_1 = null;

        EObject this_Transition_2 = null;

        EObject this_Place_3 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:260:28: ( (this_OutputArc_0= ruleOutputArc | this_InputArc_1= ruleInputArc | this_Transition_2= ruleTransition | this_Place_3= rulePlace ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:261:1: (this_OutputArc_0= ruleOutputArc | this_InputArc_1= ruleInputArc | this_Transition_2= ruleTransition | this_Place_3= rulePlace )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:261:1: (this_OutputArc_0= ruleOutputArc | this_InputArc_1= ruleInputArc | this_Transition_2= ruleTransition | this_Place_3= rulePlace )
            int alt2=4;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_LESS) ) {
                switch ( input.LA(2) ) {
                case RULE_TINPUTARC:
                    {
                    alt2=2;
                    }
                    break;
                case RULE_TPLACE:
                    {
                    alt2=4;
                    }
                    break;
                case RULE_TTRANSITION:
                    {
                    alt2=3;
                    }
                    break;
                case RULE_TOUTPUTARC:
                    {
                    alt2=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:262:5: this_OutputArc_0= ruleOutputArc
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getOutputArcParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleOutputArc_in_ruleElement574);
                    this_OutputArc_0=ruleOutputArc();

                    state._fsp--;

                     
                            current = this_OutputArc_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:272:5: this_InputArc_1= ruleInputArc
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getInputArcParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleInputArc_in_ruleElement601);
                    this_InputArc_1=ruleInputArc();

                    state._fsp--;

                     
                            current = this_InputArc_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:282:5: this_Transition_2= ruleTransition
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getTransitionParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleTransition_in_ruleElement628);
                    this_Transition_2=ruleTransition();

                    state._fsp--;

                     
                            current = this_Transition_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:292:5: this_Place_3= rulePlace
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getPlaceParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_rulePlace_in_ruleElement655);
                    this_Place_3=rulePlace();

                    state._fsp--;

                     
                            current = this_Place_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleInputArc"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:308:1: entryRuleInputArc returns [EObject current=null] : iv_ruleInputArc= ruleInputArc EOF ;
    public final EObject entryRuleInputArc() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputArc = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:309:2: (iv_ruleInputArc= ruleInputArc EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:310:2: iv_ruleInputArc= ruleInputArc EOF
            {
             newCompositeNode(grammarAccess.getInputArcRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleInputArc_in_entryRuleInputArc690);
            iv_ruleInputArc=ruleInputArc();

            state._fsp--;

             current =iv_ruleInputArc; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleInputArc700); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputArc"


    // $ANTLR start "ruleInputArc"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:317:1: ruleInputArc returns [EObject current=null] : (this_LESS_0= RULE_LESS this_TINPUTARC_1= RULE_TINPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TINPUTARC_15= RULE_TINPUTARC this_GREATER_16= RULE_GREATER ) ;
    public final EObject ruleInputArc() throws RecognitionException {
        EObject current = null;

        Token this_LESS_0=null;
        Token this_TINPUTARC_1=null;
        Token this_ID_3=null;
        Token this_EQUALS_4=null;
        Token lv_name_5_0=null;
        Token this_SOURCE_6=null;
        Token this_EQUALS_7=null;
        Token otherlv_8=null;
        Token this_TARGET_9=null;
        Token this_EQUALS_10=null;
        Token otherlv_11=null;
        Token this_GREATER_12=null;
        Token this_LESS_13=null;
        Token this_SLASH_14=null;
        Token this_TINPUTARC_15=null;
        Token this_GREATER_16=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:320:28: ( (this_LESS_0= RULE_LESS this_TINPUTARC_1= RULE_TINPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TINPUTARC_15= RULE_TINPUTARC this_GREATER_16= RULE_GREATER ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:321:1: (this_LESS_0= RULE_LESS this_TINPUTARC_1= RULE_TINPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TINPUTARC_15= RULE_TINPUTARC this_GREATER_16= RULE_GREATER )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:321:1: (this_LESS_0= RULE_LESS this_TINPUTARC_1= RULE_TINPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TINPUTARC_15= RULE_TINPUTARC this_GREATER_16= RULE_GREATER )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:321:2: this_LESS_0= RULE_LESS this_TINPUTARC_1= RULE_TINPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TINPUTARC_15= RULE_TINPUTARC this_GREATER_16= RULE_GREATER
            {
            this_LESS_0=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleInputArc736); 
             
                newLeafNode(this_LESS_0, grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_0()); 
                
            this_TINPUTARC_1=(Token)match(input,RULE_TINPUTARC,FollowSets000.FOLLOW_RULE_TINPUTARC_in_ruleInputArc746); 
             
                newLeafNode(this_TINPUTARC_1, grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_1()); 
                
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:329:1: ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:331:1: ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:331:1: ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:332:2: ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?)
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:335:2: ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?)
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:336:3: ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:336:3: ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=4;
                int LA3_0 = input.LA(1);

                if ( LA3_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
                    alt3=1;
                }
                else if ( LA3_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
                    alt3=2;
                }
                else if ( LA3_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
                    alt3=3;
                }


                switch (alt3) {
            	case 1 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:338:4: ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:338:4: ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:339:5: {...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:339:105: ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:340:6: ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:343:6: ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:343:7: {...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:343:16: (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:343:17: this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) )
            	    {
            	    this_ID_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleInputArc802); 
            	     
            	        newLeafNode(this_ID_3, grammarAccess.getInputArcAccess().getIDTerminalRuleCall_2_0_0()); 
            	        
            	    this_EQUALS_4=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleInputArc812); 
            	     
            	        newLeafNode(this_EQUALS_4, grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:351:1: ( (lv_name_5_0= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:352:1: (lv_name_5_0= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:352:1: (lv_name_5_0= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:353:3: lv_name_5_0= RULE_STRING
            	    {
            	    lv_name_5_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleInputArc828); 

            	    			newLeafNode(lv_name_5_0, grammarAccess.getInputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getInputArcRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"name",
            	            		lv_name_5_0, 
            	            		"STRING");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:376:4: ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:376:4: ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:377:5: {...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:377:105: ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:378:6: ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:381:6: ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:381:7: {...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:381:16: (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:381:17: this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) )
            	    {
            	    this_SOURCE_6=(Token)match(input,RULE_SOURCE,FollowSets000.FOLLOW_RULE_SOURCE_in_ruleInputArc900); 
            	     
            	        newLeafNode(this_SOURCE_6, grammarAccess.getInputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 
            	        
            	    this_EQUALS_7=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleInputArc910); 
            	     
            	        newLeafNode(this_EQUALS_7, grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:389:1: ( (otherlv_8= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:390:1: (otherlv_8= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:390:1: (otherlv_8= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:391:3: otherlv_8= RULE_STRING
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getInputArcRule());
            	    	        }
            	            
            	    otherlv_8=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleInputArc929); 

            	    		newLeafNode(otherlv_8, grammarAccess.getInputArcAccess().getFromPlaceCrossReference_2_1_2_0()); 
            	    	

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:409:4: ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:409:4: ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:410:5: {...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:410:105: ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:411:6: ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:414:6: ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:414:7: {...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:414:16: (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:414:17: this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) )
            	    {
            	    this_TARGET_9=(Token)match(input,RULE_TARGET,FollowSets000.FOLLOW_RULE_TARGET_in_ruleInputArc996); 
            	     
            	        newLeafNode(this_TARGET_9, grammarAccess.getInputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 
            	        
            	    this_EQUALS_10=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleInputArc1006); 
            	     
            	        newLeafNode(this_EQUALS_10, grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:422:1: ( (otherlv_11= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:423:1: (otherlv_11= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:423:1: (otherlv_11= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:424:3: otherlv_11= RULE_STRING
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getInputArcRule());
            	    	        }
            	            
            	    otherlv_11=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleInputArc1025); 

            	    		newLeafNode(otherlv_11, grammarAccess.getInputArcAccess().getToTransitionCrossReference_2_2_2_0()); 
            	    	

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getInputArcAccess().getUnorderedGroup_2()) ) {
                throw new FailedPredicateException(input, "ruleInputArc", "getUnorderedGroupHelper().canLeave(grammarAccess.getInputArcAccess().getUnorderedGroup_2())");
            }

            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	

            }

            this_GREATER_12=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleInputArc1083); 
             
                newLeafNode(this_GREATER_12, grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_3()); 
                
            this_LESS_13=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleInputArc1093); 
             
                newLeafNode(this_LESS_13, grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_4()); 
                
            this_SLASH_14=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_ruleInputArc1103); 
             
                newLeafNode(this_SLASH_14, grammarAccess.getInputArcAccess().getSLASHTerminalRuleCall_5()); 
                
            this_TINPUTARC_15=(Token)match(input,RULE_TINPUTARC,FollowSets000.FOLLOW_RULE_TINPUTARC_in_ruleInputArc1113); 
             
                newLeafNode(this_TINPUTARC_15, grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_6()); 
                
            this_GREATER_16=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleInputArc1123); 
             
                newLeafNode(this_GREATER_16, grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_7()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputArc"


    // $ANTLR start "entryRulePlace"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:478:1: entryRulePlace returns [EObject current=null] : iv_rulePlace= rulePlace EOF ;
    public final EObject entryRulePlace() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlace = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:479:2: (iv_rulePlace= rulePlace EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:480:2: iv_rulePlace= rulePlace EOF
            {
             newCompositeNode(grammarAccess.getPlaceRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePlace_in_entryRulePlace1158);
            iv_rulePlace=rulePlace();

            state._fsp--;

             current =iv_rulePlace; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePlace1168); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:487:1: rulePlace returns [EObject current=null] : (this_LESS_0= RULE_LESS this_TPLACE_1= RULE_TPLACE this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) this_GREATER_5= RULE_GREATER this_LESS_6= RULE_LESS this_SLASH_7= RULE_SLASH this_TPLACE_8= RULE_TPLACE this_GREATER_9= RULE_GREATER ) ;
    public final EObject rulePlace() throws RecognitionException {
        EObject current = null;

        Token this_LESS_0=null;
        Token this_TPLACE_1=null;
        Token this_ID_2=null;
        Token this_EQUALS_3=null;
        Token lv_name_4_0=null;
        Token this_GREATER_5=null;
        Token this_LESS_6=null;
        Token this_SLASH_7=null;
        Token this_TPLACE_8=null;
        Token this_GREATER_9=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:490:28: ( (this_LESS_0= RULE_LESS this_TPLACE_1= RULE_TPLACE this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) this_GREATER_5= RULE_GREATER this_LESS_6= RULE_LESS this_SLASH_7= RULE_SLASH this_TPLACE_8= RULE_TPLACE this_GREATER_9= RULE_GREATER ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:491:1: (this_LESS_0= RULE_LESS this_TPLACE_1= RULE_TPLACE this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) this_GREATER_5= RULE_GREATER this_LESS_6= RULE_LESS this_SLASH_7= RULE_SLASH this_TPLACE_8= RULE_TPLACE this_GREATER_9= RULE_GREATER )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:491:1: (this_LESS_0= RULE_LESS this_TPLACE_1= RULE_TPLACE this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) this_GREATER_5= RULE_GREATER this_LESS_6= RULE_LESS this_SLASH_7= RULE_SLASH this_TPLACE_8= RULE_TPLACE this_GREATER_9= RULE_GREATER )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:491:2: this_LESS_0= RULE_LESS this_TPLACE_1= RULE_TPLACE this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) this_GREATER_5= RULE_GREATER this_LESS_6= RULE_LESS this_SLASH_7= RULE_SLASH this_TPLACE_8= RULE_TPLACE this_GREATER_9= RULE_GREATER
            {
            this_LESS_0=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePlace1204); 
             
                newLeafNode(this_LESS_0, grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_0()); 
                
            this_TPLACE_1=(Token)match(input,RULE_TPLACE,FollowSets000.FOLLOW_RULE_TPLACE_in_rulePlace1214); 
             
                newLeafNode(this_TPLACE_1, grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_1()); 
                
            this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rulePlace1224); 
             
                newLeafNode(this_ID_2, grammarAccess.getPlaceAccess().getIDTerminalRuleCall_2()); 
                
            this_EQUALS_3=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rulePlace1234); 
             
                newLeafNode(this_EQUALS_3, grammarAccess.getPlaceAccess().getEQUALSTerminalRuleCall_3()); 
                
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:507:1: ( (lv_name_4_0= RULE_STRING ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:508:1: (lv_name_4_0= RULE_STRING )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:508:1: (lv_name_4_0= RULE_STRING )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:509:3: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rulePlace1250); 

            			newLeafNode(lv_name_4_0, grammarAccess.getPlaceAccess().getNameSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPlaceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"STRING");
            	    

            }


            }

            this_GREATER_5=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePlace1266); 
             
                newLeafNode(this_GREATER_5, grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_5()); 
                
            this_LESS_6=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rulePlace1276); 
             
                newLeafNode(this_LESS_6, grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_6()); 
                
            this_SLASH_7=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rulePlace1286); 
             
                newLeafNode(this_SLASH_7, grammarAccess.getPlaceAccess().getSLASHTerminalRuleCall_7()); 
                
            this_TPLACE_8=(Token)match(input,RULE_TPLACE,FollowSets000.FOLLOW_RULE_TPLACE_in_rulePlace1296); 
             
                newLeafNode(this_TPLACE_8, grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_8()); 
                
            this_GREATER_9=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rulePlace1306); 
             
                newLeafNode(this_GREATER_9, grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_9()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "entryRuleTransition"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:553:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:554:2: (iv_ruleTransition= ruleTransition EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:555:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTransition_in_entryRuleTransition1341);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTransition1351); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:562:1: ruleTransition returns [EObject current=null] : (this_LESS_0= RULE_LESS this_TTRANSITION_1= RULE_TTRANSITION this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )? (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )? this_GREATER_11= RULE_GREATER this_LESS_12= RULE_LESS this_SLASH_13= RULE_SLASH this_TTRANSITION_14= RULE_TTRANSITION this_GREATER_15= RULE_GREATER ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token this_LESS_0=null;
        Token this_TTRANSITION_1=null;
        Token this_ID_2=null;
        Token this_EQUALS_3=null;
        Token lv_name_4_0=null;
        Token this_MAXDELAY_5=null;
        Token this_EQUALS_6=null;
        Token this_MINDELAY_8=null;
        Token this_EQUALS_9=null;
        Token this_GREATER_11=null;
        Token this_LESS_12=null;
        Token this_SLASH_13=null;
        Token this_TTRANSITION_14=null;
        Token this_GREATER_15=null;
        AntlrDatatypeRuleToken lv_maxDelay_7_0 = null;

        AntlrDatatypeRuleToken lv_minDelay_10_0 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:565:28: ( (this_LESS_0= RULE_LESS this_TTRANSITION_1= RULE_TTRANSITION this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )? (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )? this_GREATER_11= RULE_GREATER this_LESS_12= RULE_LESS this_SLASH_13= RULE_SLASH this_TTRANSITION_14= RULE_TTRANSITION this_GREATER_15= RULE_GREATER ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:566:1: (this_LESS_0= RULE_LESS this_TTRANSITION_1= RULE_TTRANSITION this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )? (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )? this_GREATER_11= RULE_GREATER this_LESS_12= RULE_LESS this_SLASH_13= RULE_SLASH this_TTRANSITION_14= RULE_TTRANSITION this_GREATER_15= RULE_GREATER )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:566:1: (this_LESS_0= RULE_LESS this_TTRANSITION_1= RULE_TTRANSITION this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )? (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )? this_GREATER_11= RULE_GREATER this_LESS_12= RULE_LESS this_SLASH_13= RULE_SLASH this_TTRANSITION_14= RULE_TTRANSITION this_GREATER_15= RULE_GREATER )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:566:2: this_LESS_0= RULE_LESS this_TTRANSITION_1= RULE_TTRANSITION this_ID_2= RULE_ID this_EQUALS_3= RULE_EQUALS ( (lv_name_4_0= RULE_STRING ) ) (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )? (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )? this_GREATER_11= RULE_GREATER this_LESS_12= RULE_LESS this_SLASH_13= RULE_SLASH this_TTRANSITION_14= RULE_TTRANSITION this_GREATER_15= RULE_GREATER
            {
            this_LESS_0=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleTransition1387); 
             
                newLeafNode(this_LESS_0, grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_0()); 
                
            this_TTRANSITION_1=(Token)match(input,RULE_TTRANSITION,FollowSets000.FOLLOW_RULE_TTRANSITION_in_ruleTransition1397); 
             
                newLeafNode(this_TTRANSITION_1, grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_1()); 
                
            this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleTransition1407); 
             
                newLeafNode(this_ID_2, grammarAccess.getTransitionAccess().getIDTerminalRuleCall_2()); 
                
            this_EQUALS_3=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleTransition1417); 
             
                newLeafNode(this_EQUALS_3, grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_3()); 
                
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:582:1: ( (lv_name_4_0= RULE_STRING ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:583:1: (lv_name_4_0= RULE_STRING )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:583:1: (lv_name_4_0= RULE_STRING )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:584:3: lv_name_4_0= RULE_STRING
            {
            lv_name_4_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleTransition1433); 

            			newLeafNode(lv_name_4_0, grammarAccess.getTransitionAccess().getNameSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTransitionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"STRING");
            	    

            }


            }

            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:600:2: (this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_MAXDELAY) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:600:3: this_MAXDELAY_5= RULE_MAXDELAY this_EQUALS_6= RULE_EQUALS ( (lv_maxDelay_7_0= ruleDOUBLE ) )
                    {
                    this_MAXDELAY_5=(Token)match(input,RULE_MAXDELAY,FollowSets000.FOLLOW_RULE_MAXDELAY_in_ruleTransition1450); 
                     
                        newLeafNode(this_MAXDELAY_5, grammarAccess.getTransitionAccess().getMAXDELAYTerminalRuleCall_5_0()); 
                        
                    this_EQUALS_6=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleTransition1460); 
                     
                        newLeafNode(this_EQUALS_6, grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_5_1()); 
                        
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:608:1: ( (lv_maxDelay_7_0= ruleDOUBLE ) )
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:609:1: (lv_maxDelay_7_0= ruleDOUBLE )
                    {
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:609:1: (lv_maxDelay_7_0= ruleDOUBLE )
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:610:3: lv_maxDelay_7_0= ruleDOUBLE
                    {
                     
                    	        newCompositeNode(grammarAccess.getTransitionAccess().getMaxDelayDOUBLEParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_ruleTransition1480);
                    lv_maxDelay_7_0=ruleDOUBLE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTransitionRule());
                    	        }
                           		set(
                           			current, 
                           			"maxDelay",
                            		lv_maxDelay_7_0, 
                            		"DOUBLE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:626:4: (this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_MINDELAY) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:626:5: this_MINDELAY_8= RULE_MINDELAY this_EQUALS_9= RULE_EQUALS ( (lv_minDelay_10_0= ruleDOUBLE ) )
                    {
                    this_MINDELAY_8=(Token)match(input,RULE_MINDELAY,FollowSets000.FOLLOW_RULE_MINDELAY_in_ruleTransition1494); 
                     
                        newLeafNode(this_MINDELAY_8, grammarAccess.getTransitionAccess().getMINDELAYTerminalRuleCall_6_0()); 
                        
                    this_EQUALS_9=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleTransition1504); 
                     
                        newLeafNode(this_EQUALS_9, grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_6_1()); 
                        
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:634:1: ( (lv_minDelay_10_0= ruleDOUBLE ) )
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:635:1: (lv_minDelay_10_0= ruleDOUBLE )
                    {
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:635:1: (lv_minDelay_10_0= ruleDOUBLE )
                    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:636:3: lv_minDelay_10_0= ruleDOUBLE
                    {
                     
                    	        newCompositeNode(grammarAccess.getTransitionAccess().getMinDelayDOUBLEParserRuleCall_6_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_ruleTransition1524);
                    lv_minDelay_10_0=ruleDOUBLE();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTransitionRule());
                    	        }
                           		set(
                           			current, 
                           			"minDelay",
                            		lv_minDelay_10_0, 
                            		"DOUBLE");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            this_GREATER_11=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleTransition1537); 
             
                newLeafNode(this_GREATER_11, grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_7()); 
                
            this_LESS_12=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleTransition1547); 
             
                newLeafNode(this_LESS_12, grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_8()); 
                
            this_SLASH_13=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_ruleTransition1557); 
             
                newLeafNode(this_SLASH_13, grammarAccess.getTransitionAccess().getSLASHTerminalRuleCall_9()); 
                
            this_TTRANSITION_14=(Token)match(input,RULE_TTRANSITION,FollowSets000.FOLLOW_RULE_TTRANSITION_in_ruleTransition1567); 
             
                newLeafNode(this_TTRANSITION_14, grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_10()); 
                
            this_GREATER_15=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleTransition1577); 
             
                newLeafNode(this_GREATER_15, grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_11()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleOutputArc"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:680:1: entryRuleOutputArc returns [EObject current=null] : iv_ruleOutputArc= ruleOutputArc EOF ;
    public final EObject entryRuleOutputArc() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputArc = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:681:2: (iv_ruleOutputArc= ruleOutputArc EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:682:2: iv_ruleOutputArc= ruleOutputArc EOF
            {
             newCompositeNode(grammarAccess.getOutputArcRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleOutputArc_in_entryRuleOutputArc1612);
            iv_ruleOutputArc=ruleOutputArc();

            state._fsp--;

             current =iv_ruleOutputArc; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleOutputArc1622); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputArc"


    // $ANTLR start "ruleOutputArc"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:689:1: ruleOutputArc returns [EObject current=null] : (this_LESS_0= RULE_LESS this_TOUTPUTARC_1= RULE_TOUTPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TOUTPUTARC_15= RULE_TOUTPUTARC this_GREATER_16= RULE_GREATER ) ;
    public final EObject ruleOutputArc() throws RecognitionException {
        EObject current = null;

        Token this_LESS_0=null;
        Token this_TOUTPUTARC_1=null;
        Token this_ID_3=null;
        Token this_EQUALS_4=null;
        Token lv_name_5_0=null;
        Token this_SOURCE_6=null;
        Token this_EQUALS_7=null;
        Token otherlv_8=null;
        Token this_TARGET_9=null;
        Token this_EQUALS_10=null;
        Token otherlv_11=null;
        Token this_GREATER_12=null;
        Token this_LESS_13=null;
        Token this_SLASH_14=null;
        Token this_TOUTPUTARC_15=null;
        Token this_GREATER_16=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:692:28: ( (this_LESS_0= RULE_LESS this_TOUTPUTARC_1= RULE_TOUTPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TOUTPUTARC_15= RULE_TOUTPUTARC this_GREATER_16= RULE_GREATER ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:693:1: (this_LESS_0= RULE_LESS this_TOUTPUTARC_1= RULE_TOUTPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TOUTPUTARC_15= RULE_TOUTPUTARC this_GREATER_16= RULE_GREATER )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:693:1: (this_LESS_0= RULE_LESS this_TOUTPUTARC_1= RULE_TOUTPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TOUTPUTARC_15= RULE_TOUTPUTARC this_GREATER_16= RULE_GREATER )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:693:2: this_LESS_0= RULE_LESS this_TOUTPUTARC_1= RULE_TOUTPUTARC ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) ) this_GREATER_12= RULE_GREATER this_LESS_13= RULE_LESS this_SLASH_14= RULE_SLASH this_TOUTPUTARC_15= RULE_TOUTPUTARC this_GREATER_16= RULE_GREATER
            {
            this_LESS_0=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleOutputArc1658); 
             
                newLeafNode(this_LESS_0, grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_0()); 
                
            this_TOUTPUTARC_1=(Token)match(input,RULE_TOUTPUTARC,FollowSets000.FOLLOW_RULE_TOUTPUTARC_in_ruleOutputArc1668); 
             
                newLeafNode(this_TOUTPUTARC_1, grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_1()); 
                
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:701:1: ( ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:703:1: ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:703:1: ( ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:704:2: ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?)
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:707:2: ( ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?)
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:708:3: ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+ {...}?
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:708:3: ( ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=4;
                int LA6_0 = input.LA(1);

                if ( LA6_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
                    alt6=1;
                }
                else if ( LA6_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
                    alt6=2;
                }
                else if ( LA6_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
                    alt6=3;
                }


                switch (alt6) {
            	case 1 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:710:4: ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:710:4: ({...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:711:5: {...}? => ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:711:106: ( ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:712:6: ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:715:6: ({...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:715:7: {...}? => (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:715:16: (this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:715:17: this_ID_3= RULE_ID this_EQUALS_4= RULE_EQUALS ( (lv_name_5_0= RULE_STRING ) )
            	    {
            	    this_ID_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleOutputArc1724); 
            	     
            	        newLeafNode(this_ID_3, grammarAccess.getOutputArcAccess().getIDTerminalRuleCall_2_0_0()); 
            	        
            	    this_EQUALS_4=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleOutputArc1734); 
            	     
            	        newLeafNode(this_EQUALS_4, grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:723:1: ( (lv_name_5_0= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:724:1: (lv_name_5_0= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:724:1: (lv_name_5_0= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:725:3: lv_name_5_0= RULE_STRING
            	    {
            	    lv_name_5_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleOutputArc1750); 

            	    			newLeafNode(lv_name_5_0, grammarAccess.getOutputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getOutputArcRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"name",
            	            		lv_name_5_0, 
            	            		"STRING");
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:748:4: ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:748:4: ({...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:749:5: {...}? => ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:749:106: ( ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:750:6: ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:753:6: ({...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:753:7: {...}? => (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:753:16: (this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:753:17: this_SOURCE_6= RULE_SOURCE this_EQUALS_7= RULE_EQUALS ( (otherlv_8= RULE_STRING ) )
            	    {
            	    this_SOURCE_6=(Token)match(input,RULE_SOURCE,FollowSets000.FOLLOW_RULE_SOURCE_in_ruleOutputArc1822); 
            	     
            	        newLeafNode(this_SOURCE_6, grammarAccess.getOutputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 
            	        
            	    this_EQUALS_7=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleOutputArc1832); 
            	     
            	        newLeafNode(this_EQUALS_7, grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:761:1: ( (otherlv_8= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:762:1: (otherlv_8= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:762:1: (otherlv_8= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:763:3: otherlv_8= RULE_STRING
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getOutputArcRule());
            	    	        }
            	            
            	    otherlv_8=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleOutputArc1851); 

            	    		newLeafNode(otherlv_8, grammarAccess.getOutputArcAccess().getFromTransitionCrossReference_2_1_2_0()); 
            	    	

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:781:4: ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:781:4: ({...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:782:5: {...}? => ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2)");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:782:106: ( ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:783:6: ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2);
            	    	 				
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:786:6: ({...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:786:7: {...}? => (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleOutputArc", "true");
            	    }
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:786:16: (this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:786:17: this_TARGET_9= RULE_TARGET this_EQUALS_10= RULE_EQUALS ( (otherlv_11= RULE_STRING ) )
            	    {
            	    this_TARGET_9=(Token)match(input,RULE_TARGET,FollowSets000.FOLLOW_RULE_TARGET_in_ruleOutputArc1918); 
            	     
            	        newLeafNode(this_TARGET_9, grammarAccess.getOutputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 
            	        
            	    this_EQUALS_10=(Token)match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_ruleOutputArc1928); 
            	     
            	        newLeafNode(this_EQUALS_10, grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 
            	        
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:794:1: ( (otherlv_11= RULE_STRING ) )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:795:1: (otherlv_11= RULE_STRING )
            	    {
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:795:1: (otherlv_11= RULE_STRING )
            	    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:796:3: otherlv_11= RULE_STRING
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getOutputArcRule());
            	    	        }
            	            
            	    otherlv_11=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleOutputArc1947); 

            	    		newLeafNode(otherlv_11, grammarAccess.getOutputArcAccess().getToPlaceCrossReference_2_2_2_0()); 
            	    	

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2()) ) {
                throw new FailedPredicateException(input, "ruleOutputArc", "getUnorderedGroupHelper().canLeave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2())");
            }

            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	

            }

            this_GREATER_12=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleOutputArc2005); 
             
                newLeafNode(this_GREATER_12, grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_3()); 
                
            this_LESS_13=(Token)match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_ruleOutputArc2015); 
             
                newLeafNode(this_LESS_13, grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_4()); 
                
            this_SLASH_14=(Token)match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_ruleOutputArc2025); 
             
                newLeafNode(this_SLASH_14, grammarAccess.getOutputArcAccess().getSLASHTerminalRuleCall_5()); 
                
            this_TOUTPUTARC_15=(Token)match(input,RULE_TOUTPUTARC,FollowSets000.FOLLOW_RULE_TOUTPUTARC_in_ruleOutputArc2035); 
             
                newLeafNode(this_TOUTPUTARC_15, grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_6()); 
                
            this_GREATER_16=(Token)match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_ruleOutputArc2045); 
             
                newLeafNode(this_GREATER_16, grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_7()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputArc"


    // $ANTLR start "entryRuleDOUBLE"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:850:1: entryRuleDOUBLE returns [String current=null] : iv_ruleDOUBLE= ruleDOUBLE EOF ;
    public final String entryRuleDOUBLE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDOUBLE = null;


        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:851:2: (iv_ruleDOUBLE= ruleDOUBLE EOF )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:852:2: iv_ruleDOUBLE= ruleDOUBLE EOF
            {
             newCompositeNode(grammarAccess.getDOUBLERule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_entryRuleDOUBLE2081);
            iv_ruleDOUBLE=ruleDOUBLE();

            state._fsp--;

             current =iv_ruleDOUBLE.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDOUBLE2092); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOUBLE"


    // $ANTLR start "ruleDOUBLE"
    // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:859:1: ruleDOUBLE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleDOUBLE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:862:28: ( (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:863:1: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            {
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:863:1: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            // ../uniandes.mdd.example.language/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalPetrinetDsl.g:863:6: this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleDOUBLE2132); 

            		current.merge(this_INT_0);
                
             
                newLeafNode(this_INT_0, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_0()); 
                
            kw=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleDOUBLE2150); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getDOUBLEAccess().getFullStopKeyword_1()); 
                
            this_INT_2=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleDOUBLE2165); 

            		current.merge(this_INT_2);
                
             
                newLeafNode(this_INT_2, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_2()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOUBLE"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_rulePetriNet_in_entryRulePetriNet75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePetriNet85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePetriNet130 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_QUESTION_in_rulePetriNet140 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_XML_in_rulePetriNet150 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_RULE_VERSION_in_rulePetriNet160 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rulePetriNet170 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_rulePetriNet180 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_QUESTION_in_rulePetriNet190 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePetriNet200 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePetriNet210 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_RULE_PNML_in_rulePetriNet220 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_RULE_COLON_in_rulePetriNet230 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_RULE_PNML_in_rulePetriNet240 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_RULE_XMLNS_in_rulePetriNet250 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_RULE_COLON_in_rulePetriNet260 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_RULE_PNML_in_rulePetriNet270 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rulePetriNet280 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_rulePetriNet290 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePetriNet300 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePetriNet310 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_RULE_NET_in_rulePetriNet320 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_RULE_TYPE_in_rulePetriNet330 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rulePetriNet340 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_rulePetriNet350 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePetriNet360 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_ruleElement_in_rulePetriNet380 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePetriNet392 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rulePetriNet402 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_RULE_NET_in_rulePetriNet412 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePetriNet422 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePetriNet432 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rulePetriNet442 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_RULE_PNML_in_rulePetriNet452 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_RULE_COLON_in_rulePetriNet462 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_RULE_PNML_in_rulePetriNet472 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePetriNet482 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElement_in_entryRuleElement517 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleElement527 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOutputArc_in_ruleElement574 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInputArc_in_ruleElement601 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTransition_in_ruleElement628 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePlace_in_ruleElement655 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInputArc_in_entryRuleInputArc690 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleInputArc700 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleInputArc736 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_RULE_TINPUTARC_in_ruleInputArc746 = new BitSet(new long[]{0x00000000001C0000L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleInputArc802 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleInputArc812 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleInputArc828 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_SOURCE_in_ruleInputArc900 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleInputArc910 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleInputArc929 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_TARGET_in_ruleInputArc996 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleInputArc1006 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleInputArc1025 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleInputArc1083 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleInputArc1093 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_ruleInputArc1103 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_RULE_TINPUTARC_in_ruleInputArc1113 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleInputArc1123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePlace_in_entryRulePlace1158 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePlace1168 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePlace1204 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_RULE_TPLACE_in_rulePlace1214 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_RULE_ID_in_rulePlace1224 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rulePlace1234 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_rulePlace1250 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePlace1266 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_rulePlace1276 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rulePlace1286 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_RULE_TPLACE_in_rulePlace1296 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rulePlace1306 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition1341 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTransition1351 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleTransition1387 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_RULE_TTRANSITION_in_ruleTransition1397 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleTransition1407 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleTransition1417 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleTransition1433 = new BitSet(new long[]{0x0000000001800400L});
        public static final BitSet FOLLOW_RULE_MAXDELAY_in_ruleTransition1450 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleTransition1460 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_ruleTransition1480 = new BitSet(new long[]{0x0000000001000400L});
        public static final BitSet FOLLOW_RULE_MINDELAY_in_ruleTransition1494 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleTransition1504 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_ruleTransition1524 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleTransition1537 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleTransition1547 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_ruleTransition1557 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_RULE_TTRANSITION_in_ruleTransition1567 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleTransition1577 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOutputArc_in_entryRuleOutputArc1612 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleOutputArc1622 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleOutputArc1658 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_RULE_TOUTPUTARC_in_ruleOutputArc1668 = new BitSet(new long[]{0x00000000001C0000L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleOutputArc1724 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleOutputArc1734 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleOutputArc1750 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_SOURCE_in_ruleOutputArc1822 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleOutputArc1832 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleOutputArc1851 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_TARGET_in_ruleOutputArc1918 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_ruleOutputArc1928 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleOutputArc1947 = new BitSet(new long[]{0x00000000001C0400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleOutputArc2005 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_LESS_in_ruleOutputArc2015 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_RULE_SLASH_in_ruleOutputArc2025 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_RULE_TOUTPUTARC_in_ruleOutputArc2035 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_RULE_GREATER_in_ruleOutputArc2045 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_entryRuleDOUBLE2081 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDOUBLE2092 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleDOUBLE2132 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_31_in_ruleDOUBLE2150 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleDOUBLE2165 = new BitSet(new long[]{0x0000000000000002L});
    }


}