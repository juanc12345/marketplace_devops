package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.PetrinetDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPetrinetDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_LESS", "RULE_QUESTION", "RULE_XML", "RULE_VERSION", "RULE_EQUALS", "RULE_STRING", "RULE_GREATER", "RULE_PNML", "RULE_COLON", "RULE_XMLNS", "RULE_NET", "RULE_TYPE", "RULE_SLASH", "RULE_TINPUTARC", "RULE_ID", "RULE_SOURCE", "RULE_TARGET", "RULE_TPLACE", "RULE_TTRANSITION", "RULE_MAXDELAY", "RULE_MINDELAY", "RULE_TOUTPUTARC", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'.'"
    };
    public static final int RULE_MAXDELAY=23;
    public static final int RULE_NET=14;
    public static final int RULE_TPLACE=21;
    public static final int RULE_XMLNS=13;
    public static final int RULE_SOURCE=19;
    public static final int RULE_STRING=9;
    public static final int RULE_XML=6;
    public static final int RULE_GREATER=10;
    public static final int RULE_TINPUTARC=17;
    public static final int RULE_SL_COMMENT=28;
    public static final int RULE_TOUTPUTARC=25;
    public static final int RULE_VERSION=7;
    public static final int RULE_EQUALS=8;
    public static final int RULE_TARGET=20;
    public static final int RULE_LESS=4;
    public static final int EOF=-1;
    public static final int T__31=31;
    public static final int RULE_ID=18;
    public static final int RULE_WS=29;
    public static final int RULE_PNML=11;
    public static final int RULE_COLON=12;
    public static final int RULE_ANY_OTHER=30;
    public static final int RULE_SLASH=16;
    public static final int RULE_TYPE=15;
    public static final int RULE_INT=26;
    public static final int RULE_MINDELAY=24;
    public static final int RULE_QUESTION=5;
    public static final int RULE_ML_COMMENT=27;
    public static final int RULE_TTRANSITION=22;

    // delegates
    // delegators


        public InternalPetrinetDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPetrinetDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPetrinetDslParser.tokenNames; }
    public String getGrammarFileName() { return "../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g"; }


     
     	private PetrinetDslGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(PetrinetDslGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRulePetriNet"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:60:1: entryRulePetriNet : rulePetriNet EOF ;
    public final void entryRulePetriNet() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:61:1: ( rulePetriNet EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:62:1: rulePetriNet EOF
            {
             before(grammarAccess.getPetriNetRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePetriNet_in_entryRulePetriNet61);
            rulePetriNet();

            state._fsp--;

             after(grammarAccess.getPetriNetRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePetriNet68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePetriNet"


    // $ANTLR start "rulePetriNet"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:69:1: rulePetriNet : ( ( rule__PetriNet__Group__0 ) ) ;
    public final void rulePetriNet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:73:2: ( ( ( rule__PetriNet__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:74:1: ( ( rule__PetriNet__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:74:1: ( ( rule__PetriNet__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:75:1: ( rule__PetriNet__Group__0 )
            {
             before(grammarAccess.getPetriNetAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:76:1: ( rule__PetriNet__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:76:2: rule__PetriNet__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__0_in_rulePetriNet94);
            rule__PetriNet__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPetriNetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePetriNet"


    // $ANTLR start "entryRuleElement"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:88:1: entryRuleElement : ruleElement EOF ;
    public final void entryRuleElement() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:89:1: ( ruleElement EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:90:1: ruleElement EOF
            {
             before(grammarAccess.getElementRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleElement_in_entryRuleElement121);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getElementRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleElement128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:97:1: ruleElement : ( ( rule__Element__Alternatives ) ) ;
    public final void ruleElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:101:2: ( ( ( rule__Element__Alternatives ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:102:1: ( ( rule__Element__Alternatives ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:102:1: ( ( rule__Element__Alternatives ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:103:1: ( rule__Element__Alternatives )
            {
             before(grammarAccess.getElementAccess().getAlternatives()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:104:1: ( rule__Element__Alternatives )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:104:2: rule__Element__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Element__Alternatives_in_ruleElement154);
            rule__Element__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleInputArc"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:116:1: entryRuleInputArc : ruleInputArc EOF ;
    public final void entryRuleInputArc() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:117:1: ( ruleInputArc EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:118:1: ruleInputArc EOF
            {
             before(grammarAccess.getInputArcRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleInputArc_in_entryRuleInputArc181);
            ruleInputArc();

            state._fsp--;

             after(grammarAccess.getInputArcRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleInputArc188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInputArc"


    // $ANTLR start "ruleInputArc"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:125:1: ruleInputArc : ( ( rule__InputArc__Group__0 ) ) ;
    public final void ruleInputArc() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:129:2: ( ( ( rule__InputArc__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:130:1: ( ( rule__InputArc__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:130:1: ( ( rule__InputArc__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:131:1: ( rule__InputArc__Group__0 )
            {
             before(grammarAccess.getInputArcAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:132:1: ( rule__InputArc__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:132:2: rule__InputArc__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__0_in_ruleInputArc214);
            rule__InputArc__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInputArcAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInputArc"


    // $ANTLR start "entryRulePlace"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:144:1: entryRulePlace : rulePlace EOF ;
    public final void entryRulePlace() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:145:1: ( rulePlace EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:146:1: rulePlace EOF
            {
             before(grammarAccess.getPlaceRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePlace_in_entryRulePlace241);
            rulePlace();

            state._fsp--;

             after(grammarAccess.getPlaceRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePlace248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePlace"


    // $ANTLR start "rulePlace"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:153:1: rulePlace : ( ( rule__Place__Group__0 ) ) ;
    public final void rulePlace() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:157:2: ( ( ( rule__Place__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:158:1: ( ( rule__Place__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:158:1: ( ( rule__Place__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:159:1: ( rule__Place__Group__0 )
            {
             before(grammarAccess.getPlaceAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:160:1: ( rule__Place__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:160:2: rule__Place__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__0_in_rulePlace274);
            rule__Place__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePlace"


    // $ANTLR start "entryRuleTransition"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:172:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:173:1: ( ruleTransition EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:174:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTransition_in_entryRuleTransition301);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTransition308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:181:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:185:2: ( ( ( rule__Transition__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:186:1: ( ( rule__Transition__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:186:1: ( ( rule__Transition__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:187:1: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:188:1: ( rule__Transition__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:188:2: rule__Transition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__0_in_ruleTransition334);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleOutputArc"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:200:1: entryRuleOutputArc : ruleOutputArc EOF ;
    public final void entryRuleOutputArc() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:201:1: ( ruleOutputArc EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:202:1: ruleOutputArc EOF
            {
             before(grammarAccess.getOutputArcRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleOutputArc_in_entryRuleOutputArc361);
            ruleOutputArc();

            state._fsp--;

             after(grammarAccess.getOutputArcRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleOutputArc368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutputArc"


    // $ANTLR start "ruleOutputArc"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:209:1: ruleOutputArc : ( ( rule__OutputArc__Group__0 ) ) ;
    public final void ruleOutputArc() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:213:2: ( ( ( rule__OutputArc__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:214:1: ( ( rule__OutputArc__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:214:1: ( ( rule__OutputArc__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:215:1: ( rule__OutputArc__Group__0 )
            {
             before(grammarAccess.getOutputArcAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:216:1: ( rule__OutputArc__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:216:2: rule__OutputArc__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__0_in_ruleOutputArc394);
            rule__OutputArc__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutputArcAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutputArc"


    // $ANTLR start "entryRuleDOUBLE"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:228:1: entryRuleDOUBLE : ruleDOUBLE EOF ;
    public final void entryRuleDOUBLE() throws RecognitionException {
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:229:1: ( ruleDOUBLE EOF )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:230:1: ruleDOUBLE EOF
            {
             before(grammarAccess.getDOUBLERule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_entryRuleDOUBLE421);
            ruleDOUBLE();

            state._fsp--;

             after(grammarAccess.getDOUBLERule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDOUBLE428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDOUBLE"


    // $ANTLR start "ruleDOUBLE"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:237:1: ruleDOUBLE : ( ( rule__DOUBLE__Group__0 ) ) ;
    public final void ruleDOUBLE() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:241:2: ( ( ( rule__DOUBLE__Group__0 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:242:1: ( ( rule__DOUBLE__Group__0 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:242:1: ( ( rule__DOUBLE__Group__0 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:243:1: ( rule__DOUBLE__Group__0 )
            {
             before(grammarAccess.getDOUBLEAccess().getGroup()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:244:1: ( rule__DOUBLE__Group__0 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:244:2: rule__DOUBLE__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__0_in_ruleDOUBLE454);
            rule__DOUBLE__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDOUBLEAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDOUBLE"


    // $ANTLR start "rule__Element__Alternatives"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:256:1: rule__Element__Alternatives : ( ( ruleOutputArc ) | ( ruleInputArc ) | ( ruleTransition ) | ( rulePlace ) );
    public final void rule__Element__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:260:1: ( ( ruleOutputArc ) | ( ruleInputArc ) | ( ruleTransition ) | ( rulePlace ) )
            int alt1=4;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_LESS) ) {
                switch ( input.LA(2) ) {
                case RULE_TINPUTARC:
                    {
                    alt1=2;
                    }
                    break;
                case RULE_TTRANSITION:
                    {
                    alt1=3;
                    }
                    break;
                case RULE_TPLACE:
                    {
                    alt1=4;
                    }
                    break;
                case RULE_TOUTPUTARC:
                    {
                    alt1=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:261:1: ( ruleOutputArc )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:261:1: ( ruleOutputArc )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:262:1: ruleOutputArc
                    {
                     before(grammarAccess.getElementAccess().getOutputArcParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleOutputArc_in_rule__Element__Alternatives490);
                    ruleOutputArc();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getOutputArcParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:267:6: ( ruleInputArc )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:267:6: ( ruleInputArc )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:268:1: ruleInputArc
                    {
                     before(grammarAccess.getElementAccess().getInputArcParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleInputArc_in_rule__Element__Alternatives507);
                    ruleInputArc();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getInputArcParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:273:6: ( ruleTransition )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:273:6: ( ruleTransition )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:274:1: ruleTransition
                    {
                     before(grammarAccess.getElementAccess().getTransitionParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleTransition_in_rule__Element__Alternatives524);
                    ruleTransition();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getTransitionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:279:6: ( rulePlace )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:279:6: ( rulePlace )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:280:1: rulePlace
                    {
                     before(grammarAccess.getElementAccess().getPlaceParserRuleCall_3()); 
                    pushFollow(FollowSets000.FOLLOW_rulePlace_in_rule__Element__Alternatives541);
                    rulePlace();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getPlaceParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Alternatives"


    // $ANTLR start "rule__PetriNet__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:292:1: rule__PetriNet__Group__0 : rule__PetriNet__Group__0__Impl rule__PetriNet__Group__1 ;
    public final void rule__PetriNet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:296:1: ( rule__PetriNet__Group__0__Impl rule__PetriNet__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:297:2: rule__PetriNet__Group__0__Impl rule__PetriNet__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__0__Impl_in_rule__PetriNet__Group__0571);
            rule__PetriNet__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__1_in_rule__PetriNet__Group__0574);
            rule__PetriNet__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__0"


    // $ANTLR start "rule__PetriNet__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:304:1: rule__PetriNet__Group__0__Impl : ( () ) ;
    public final void rule__PetriNet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:308:1: ( ( () ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:309:1: ( () )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:309:1: ( () )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:310:1: ()
            {
             before(grammarAccess.getPetriNetAccess().getPetriNetAction_0()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:311:1: ()
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:313:1: 
            {
            }

             after(grammarAccess.getPetriNetAccess().getPetriNetAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__0__Impl"


    // $ANTLR start "rule__PetriNet__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:323:1: rule__PetriNet__Group__1 : rule__PetriNet__Group__1__Impl rule__PetriNet__Group__2 ;
    public final void rule__PetriNet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:327:1: ( rule__PetriNet__Group__1__Impl rule__PetriNet__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:328:2: rule__PetriNet__Group__1__Impl rule__PetriNet__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__1__Impl_in_rule__PetriNet__Group__1632);
            rule__PetriNet__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__2_in_rule__PetriNet__Group__1635);
            rule__PetriNet__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__1"


    // $ANTLR start "rule__PetriNet__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:335:1: rule__PetriNet__Group__1__Impl : ( RULE_LESS ) ;
    public final void rule__PetriNet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:339:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:340:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:340:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:341:1: RULE_LESS
            {
             before(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_1()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__PetriNet__Group__1__Impl662); 
             after(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__1__Impl"


    // $ANTLR start "rule__PetriNet__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:352:1: rule__PetriNet__Group__2 : rule__PetriNet__Group__2__Impl rule__PetriNet__Group__3 ;
    public final void rule__PetriNet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:356:1: ( rule__PetriNet__Group__2__Impl rule__PetriNet__Group__3 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:357:2: rule__PetriNet__Group__2__Impl rule__PetriNet__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__2__Impl_in_rule__PetriNet__Group__2691);
            rule__PetriNet__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__3_in_rule__PetriNet__Group__2694);
            rule__PetriNet__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__2"


    // $ANTLR start "rule__PetriNet__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:364:1: rule__PetriNet__Group__2__Impl : ( RULE_QUESTION ) ;
    public final void rule__PetriNet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:368:1: ( ( RULE_QUESTION ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:369:1: ( RULE_QUESTION )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:369:1: ( RULE_QUESTION )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:370:1: RULE_QUESTION
            {
             before(grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_2()); 
            match(input,RULE_QUESTION,FollowSets000.FOLLOW_RULE_QUESTION_in_rule__PetriNet__Group__2__Impl721); 
             after(grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__2__Impl"


    // $ANTLR start "rule__PetriNet__Group__3"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:381:1: rule__PetriNet__Group__3 : rule__PetriNet__Group__3__Impl rule__PetriNet__Group__4 ;
    public final void rule__PetriNet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:385:1: ( rule__PetriNet__Group__3__Impl rule__PetriNet__Group__4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:386:2: rule__PetriNet__Group__3__Impl rule__PetriNet__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__3__Impl_in_rule__PetriNet__Group__3750);
            rule__PetriNet__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__4_in_rule__PetriNet__Group__3753);
            rule__PetriNet__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__3"


    // $ANTLR start "rule__PetriNet__Group__3__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:393:1: rule__PetriNet__Group__3__Impl : ( RULE_XML ) ;
    public final void rule__PetriNet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:397:1: ( ( RULE_XML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:398:1: ( RULE_XML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:398:1: ( RULE_XML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:399:1: RULE_XML
            {
             before(grammarAccess.getPetriNetAccess().getXMLTerminalRuleCall_3()); 
            match(input,RULE_XML,FollowSets000.FOLLOW_RULE_XML_in_rule__PetriNet__Group__3__Impl780); 
             after(grammarAccess.getPetriNetAccess().getXMLTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__3__Impl"


    // $ANTLR start "rule__PetriNet__Group__4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:410:1: rule__PetriNet__Group__4 : rule__PetriNet__Group__4__Impl rule__PetriNet__Group__5 ;
    public final void rule__PetriNet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:414:1: ( rule__PetriNet__Group__4__Impl rule__PetriNet__Group__5 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:415:2: rule__PetriNet__Group__4__Impl rule__PetriNet__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__4__Impl_in_rule__PetriNet__Group__4809);
            rule__PetriNet__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__5_in_rule__PetriNet__Group__4812);
            rule__PetriNet__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__4"


    // $ANTLR start "rule__PetriNet__Group__4__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:422:1: rule__PetriNet__Group__4__Impl : ( RULE_VERSION ) ;
    public final void rule__PetriNet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:426:1: ( ( RULE_VERSION ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:427:1: ( RULE_VERSION )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:427:1: ( RULE_VERSION )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:428:1: RULE_VERSION
            {
             before(grammarAccess.getPetriNetAccess().getVERSIONTerminalRuleCall_4()); 
            match(input,RULE_VERSION,FollowSets000.FOLLOW_RULE_VERSION_in_rule__PetriNet__Group__4__Impl839); 
             after(grammarAccess.getPetriNetAccess().getVERSIONTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__4__Impl"


    // $ANTLR start "rule__PetriNet__Group__5"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:439:1: rule__PetriNet__Group__5 : rule__PetriNet__Group__5__Impl rule__PetriNet__Group__6 ;
    public final void rule__PetriNet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:443:1: ( rule__PetriNet__Group__5__Impl rule__PetriNet__Group__6 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:444:2: rule__PetriNet__Group__5__Impl rule__PetriNet__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__5__Impl_in_rule__PetriNet__Group__5868);
            rule__PetriNet__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__6_in_rule__PetriNet__Group__5871);
            rule__PetriNet__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__5"


    // $ANTLR start "rule__PetriNet__Group__5__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:451:1: rule__PetriNet__Group__5__Impl : ( RULE_EQUALS ) ;
    public final void rule__PetriNet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:455:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:456:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:456:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:457:1: RULE_EQUALS
            {
             before(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_5()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__5__Impl898); 
             after(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__5__Impl"


    // $ANTLR start "rule__PetriNet__Group__6"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:468:1: rule__PetriNet__Group__6 : rule__PetriNet__Group__6__Impl rule__PetriNet__Group__7 ;
    public final void rule__PetriNet__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:472:1: ( rule__PetriNet__Group__6__Impl rule__PetriNet__Group__7 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:473:2: rule__PetriNet__Group__6__Impl rule__PetriNet__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__6__Impl_in_rule__PetriNet__Group__6927);
            rule__PetriNet__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__7_in_rule__PetriNet__Group__6930);
            rule__PetriNet__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__6"


    // $ANTLR start "rule__PetriNet__Group__6__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:480:1: rule__PetriNet__Group__6__Impl : ( RULE_STRING ) ;
    public final void rule__PetriNet__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:484:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:485:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:485:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:486:1: RULE_STRING
            {
             before(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_6()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__PetriNet__Group__6__Impl957); 
             after(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__6__Impl"


    // $ANTLR start "rule__PetriNet__Group__7"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:497:1: rule__PetriNet__Group__7 : rule__PetriNet__Group__7__Impl rule__PetriNet__Group__8 ;
    public final void rule__PetriNet__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:501:1: ( rule__PetriNet__Group__7__Impl rule__PetriNet__Group__8 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:502:2: rule__PetriNet__Group__7__Impl rule__PetriNet__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__7__Impl_in_rule__PetriNet__Group__7986);
            rule__PetriNet__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__8_in_rule__PetriNet__Group__7989);
            rule__PetriNet__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__7"


    // $ANTLR start "rule__PetriNet__Group__7__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:509:1: rule__PetriNet__Group__7__Impl : ( RULE_QUESTION ) ;
    public final void rule__PetriNet__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:513:1: ( ( RULE_QUESTION ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:514:1: ( RULE_QUESTION )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:514:1: ( RULE_QUESTION )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:515:1: RULE_QUESTION
            {
             before(grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_7()); 
            match(input,RULE_QUESTION,FollowSets000.FOLLOW_RULE_QUESTION_in_rule__PetriNet__Group__7__Impl1016); 
             after(grammarAccess.getPetriNetAccess().getQUESTIONTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__7__Impl"


    // $ANTLR start "rule__PetriNet__Group__8"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:526:1: rule__PetriNet__Group__8 : rule__PetriNet__Group__8__Impl rule__PetriNet__Group__9 ;
    public final void rule__PetriNet__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:530:1: ( rule__PetriNet__Group__8__Impl rule__PetriNet__Group__9 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:531:2: rule__PetriNet__Group__8__Impl rule__PetriNet__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__8__Impl_in_rule__PetriNet__Group__81045);
            rule__PetriNet__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__9_in_rule__PetriNet__Group__81048);
            rule__PetriNet__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__8"


    // $ANTLR start "rule__PetriNet__Group__8__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:538:1: rule__PetriNet__Group__8__Impl : ( RULE_GREATER ) ;
    public final void rule__PetriNet__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:542:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:543:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:543:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:544:1: RULE_GREATER
            {
             before(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_8()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__8__Impl1075); 
             after(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__8__Impl"


    // $ANTLR start "rule__PetriNet__Group__9"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:555:1: rule__PetriNet__Group__9 : rule__PetriNet__Group__9__Impl rule__PetriNet__Group__10 ;
    public final void rule__PetriNet__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:559:1: ( rule__PetriNet__Group__9__Impl rule__PetriNet__Group__10 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:560:2: rule__PetriNet__Group__9__Impl rule__PetriNet__Group__10
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__9__Impl_in_rule__PetriNet__Group__91104);
            rule__PetriNet__Group__9__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__10_in_rule__PetriNet__Group__91107);
            rule__PetriNet__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__9"


    // $ANTLR start "rule__PetriNet__Group__9__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:567:1: rule__PetriNet__Group__9__Impl : ( RULE_LESS ) ;
    public final void rule__PetriNet__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:571:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:572:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:572:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:573:1: RULE_LESS
            {
             before(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_9()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__PetriNet__Group__9__Impl1134); 
             after(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__9__Impl"


    // $ANTLR start "rule__PetriNet__Group__10"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:584:1: rule__PetriNet__Group__10 : rule__PetriNet__Group__10__Impl rule__PetriNet__Group__11 ;
    public final void rule__PetriNet__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:588:1: ( rule__PetriNet__Group__10__Impl rule__PetriNet__Group__11 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:589:2: rule__PetriNet__Group__10__Impl rule__PetriNet__Group__11
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__10__Impl_in_rule__PetriNet__Group__101163);
            rule__PetriNet__Group__10__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__11_in_rule__PetriNet__Group__101166);
            rule__PetriNet__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__10"


    // $ANTLR start "rule__PetriNet__Group__10__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:596:1: rule__PetriNet__Group__10__Impl : ( RULE_PNML ) ;
    public final void rule__PetriNet__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:600:1: ( ( RULE_PNML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:601:1: ( RULE_PNML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:601:1: ( RULE_PNML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:602:1: RULE_PNML
            {
             before(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_10()); 
            match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rule__PetriNet__Group__10__Impl1193); 
             after(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__10__Impl"


    // $ANTLR start "rule__PetriNet__Group__11"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:613:1: rule__PetriNet__Group__11 : rule__PetriNet__Group__11__Impl rule__PetriNet__Group__12 ;
    public final void rule__PetriNet__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:617:1: ( rule__PetriNet__Group__11__Impl rule__PetriNet__Group__12 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:618:2: rule__PetriNet__Group__11__Impl rule__PetriNet__Group__12
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__11__Impl_in_rule__PetriNet__Group__111222);
            rule__PetriNet__Group__11__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__12_in_rule__PetriNet__Group__111225);
            rule__PetriNet__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__11"


    // $ANTLR start "rule__PetriNet__Group__11__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:625:1: rule__PetriNet__Group__11__Impl : ( RULE_COLON ) ;
    public final void rule__PetriNet__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:629:1: ( ( RULE_COLON ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:630:1: ( RULE_COLON )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:630:1: ( RULE_COLON )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:631:1: RULE_COLON
            {
             before(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_11()); 
            match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rule__PetriNet__Group__11__Impl1252); 
             after(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__11__Impl"


    // $ANTLR start "rule__PetriNet__Group__12"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:642:1: rule__PetriNet__Group__12 : rule__PetriNet__Group__12__Impl rule__PetriNet__Group__13 ;
    public final void rule__PetriNet__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:646:1: ( rule__PetriNet__Group__12__Impl rule__PetriNet__Group__13 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:647:2: rule__PetriNet__Group__12__Impl rule__PetriNet__Group__13
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__12__Impl_in_rule__PetriNet__Group__121281);
            rule__PetriNet__Group__12__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__13_in_rule__PetriNet__Group__121284);
            rule__PetriNet__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__12"


    // $ANTLR start "rule__PetriNet__Group__12__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:654:1: rule__PetriNet__Group__12__Impl : ( RULE_PNML ) ;
    public final void rule__PetriNet__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:658:1: ( ( RULE_PNML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:659:1: ( RULE_PNML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:659:1: ( RULE_PNML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:660:1: RULE_PNML
            {
             before(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_12()); 
            match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rule__PetriNet__Group__12__Impl1311); 
             after(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__12__Impl"


    // $ANTLR start "rule__PetriNet__Group__13"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:671:1: rule__PetriNet__Group__13 : rule__PetriNet__Group__13__Impl rule__PetriNet__Group__14 ;
    public final void rule__PetriNet__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:675:1: ( rule__PetriNet__Group__13__Impl rule__PetriNet__Group__14 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:676:2: rule__PetriNet__Group__13__Impl rule__PetriNet__Group__14
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__13__Impl_in_rule__PetriNet__Group__131340);
            rule__PetriNet__Group__13__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__14_in_rule__PetriNet__Group__131343);
            rule__PetriNet__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__13"


    // $ANTLR start "rule__PetriNet__Group__13__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:683:1: rule__PetriNet__Group__13__Impl : ( RULE_XMLNS ) ;
    public final void rule__PetriNet__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:687:1: ( ( RULE_XMLNS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:688:1: ( RULE_XMLNS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:688:1: ( RULE_XMLNS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:689:1: RULE_XMLNS
            {
             before(grammarAccess.getPetriNetAccess().getXMLNSTerminalRuleCall_13()); 
            match(input,RULE_XMLNS,FollowSets000.FOLLOW_RULE_XMLNS_in_rule__PetriNet__Group__13__Impl1370); 
             after(grammarAccess.getPetriNetAccess().getXMLNSTerminalRuleCall_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__13__Impl"


    // $ANTLR start "rule__PetriNet__Group__14"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:700:1: rule__PetriNet__Group__14 : rule__PetriNet__Group__14__Impl rule__PetriNet__Group__15 ;
    public final void rule__PetriNet__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:704:1: ( rule__PetriNet__Group__14__Impl rule__PetriNet__Group__15 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:705:2: rule__PetriNet__Group__14__Impl rule__PetriNet__Group__15
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__14__Impl_in_rule__PetriNet__Group__141399);
            rule__PetriNet__Group__14__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__15_in_rule__PetriNet__Group__141402);
            rule__PetriNet__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__14"


    // $ANTLR start "rule__PetriNet__Group__14__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:712:1: rule__PetriNet__Group__14__Impl : ( RULE_COLON ) ;
    public final void rule__PetriNet__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:716:1: ( ( RULE_COLON ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:717:1: ( RULE_COLON )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:717:1: ( RULE_COLON )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:718:1: RULE_COLON
            {
             before(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_14()); 
            match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rule__PetriNet__Group__14__Impl1429); 
             after(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__14__Impl"


    // $ANTLR start "rule__PetriNet__Group__15"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:729:1: rule__PetriNet__Group__15 : rule__PetriNet__Group__15__Impl rule__PetriNet__Group__16 ;
    public final void rule__PetriNet__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:733:1: ( rule__PetriNet__Group__15__Impl rule__PetriNet__Group__16 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:734:2: rule__PetriNet__Group__15__Impl rule__PetriNet__Group__16
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__15__Impl_in_rule__PetriNet__Group__151458);
            rule__PetriNet__Group__15__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__16_in_rule__PetriNet__Group__151461);
            rule__PetriNet__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__15"


    // $ANTLR start "rule__PetriNet__Group__15__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:741:1: rule__PetriNet__Group__15__Impl : ( RULE_PNML ) ;
    public final void rule__PetriNet__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:745:1: ( ( RULE_PNML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:746:1: ( RULE_PNML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:746:1: ( RULE_PNML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:747:1: RULE_PNML
            {
             before(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_15()); 
            match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rule__PetriNet__Group__15__Impl1488); 
             after(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__15__Impl"


    // $ANTLR start "rule__PetriNet__Group__16"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:758:1: rule__PetriNet__Group__16 : rule__PetriNet__Group__16__Impl rule__PetriNet__Group__17 ;
    public final void rule__PetriNet__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:762:1: ( rule__PetriNet__Group__16__Impl rule__PetriNet__Group__17 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:763:2: rule__PetriNet__Group__16__Impl rule__PetriNet__Group__17
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__16__Impl_in_rule__PetriNet__Group__161517);
            rule__PetriNet__Group__16__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__17_in_rule__PetriNet__Group__161520);
            rule__PetriNet__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__16"


    // $ANTLR start "rule__PetriNet__Group__16__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:770:1: rule__PetriNet__Group__16__Impl : ( RULE_EQUALS ) ;
    public final void rule__PetriNet__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:774:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:775:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:775:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:776:1: RULE_EQUALS
            {
             before(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_16()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__16__Impl1547); 
             after(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__16__Impl"


    // $ANTLR start "rule__PetriNet__Group__17"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:787:1: rule__PetriNet__Group__17 : rule__PetriNet__Group__17__Impl rule__PetriNet__Group__18 ;
    public final void rule__PetriNet__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:791:1: ( rule__PetriNet__Group__17__Impl rule__PetriNet__Group__18 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:792:2: rule__PetriNet__Group__17__Impl rule__PetriNet__Group__18
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__17__Impl_in_rule__PetriNet__Group__171576);
            rule__PetriNet__Group__17__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__18_in_rule__PetriNet__Group__171579);
            rule__PetriNet__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__17"


    // $ANTLR start "rule__PetriNet__Group__17__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:799:1: rule__PetriNet__Group__17__Impl : ( RULE_STRING ) ;
    public final void rule__PetriNet__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:803:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:804:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:804:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:805:1: RULE_STRING
            {
             before(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_17()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__PetriNet__Group__17__Impl1606); 
             after(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__17__Impl"


    // $ANTLR start "rule__PetriNet__Group__18"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:816:1: rule__PetriNet__Group__18 : rule__PetriNet__Group__18__Impl rule__PetriNet__Group__19 ;
    public final void rule__PetriNet__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:820:1: ( rule__PetriNet__Group__18__Impl rule__PetriNet__Group__19 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:821:2: rule__PetriNet__Group__18__Impl rule__PetriNet__Group__19
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__18__Impl_in_rule__PetriNet__Group__181635);
            rule__PetriNet__Group__18__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__19_in_rule__PetriNet__Group__181638);
            rule__PetriNet__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__18"


    // $ANTLR start "rule__PetriNet__Group__18__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:828:1: rule__PetriNet__Group__18__Impl : ( RULE_GREATER ) ;
    public final void rule__PetriNet__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:832:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:833:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:833:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:834:1: RULE_GREATER
            {
             before(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_18()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__18__Impl1665); 
             after(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__18__Impl"


    // $ANTLR start "rule__PetriNet__Group__19"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:845:1: rule__PetriNet__Group__19 : rule__PetriNet__Group__19__Impl rule__PetriNet__Group__20 ;
    public final void rule__PetriNet__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:849:1: ( rule__PetriNet__Group__19__Impl rule__PetriNet__Group__20 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:850:2: rule__PetriNet__Group__19__Impl rule__PetriNet__Group__20
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__19__Impl_in_rule__PetriNet__Group__191694);
            rule__PetriNet__Group__19__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__20_in_rule__PetriNet__Group__191697);
            rule__PetriNet__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__19"


    // $ANTLR start "rule__PetriNet__Group__19__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:857:1: rule__PetriNet__Group__19__Impl : ( RULE_LESS ) ;
    public final void rule__PetriNet__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:861:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:862:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:862:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:863:1: RULE_LESS
            {
             before(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_19()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__PetriNet__Group__19__Impl1724); 
             after(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__19__Impl"


    // $ANTLR start "rule__PetriNet__Group__20"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:874:1: rule__PetriNet__Group__20 : rule__PetriNet__Group__20__Impl rule__PetriNet__Group__21 ;
    public final void rule__PetriNet__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:878:1: ( rule__PetriNet__Group__20__Impl rule__PetriNet__Group__21 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:879:2: rule__PetriNet__Group__20__Impl rule__PetriNet__Group__21
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__20__Impl_in_rule__PetriNet__Group__201753);
            rule__PetriNet__Group__20__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__21_in_rule__PetriNet__Group__201756);
            rule__PetriNet__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__20"


    // $ANTLR start "rule__PetriNet__Group__20__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:886:1: rule__PetriNet__Group__20__Impl : ( RULE_NET ) ;
    public final void rule__PetriNet__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:890:1: ( ( RULE_NET ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:891:1: ( RULE_NET )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:891:1: ( RULE_NET )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:892:1: RULE_NET
            {
             before(grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_20()); 
            match(input,RULE_NET,FollowSets000.FOLLOW_RULE_NET_in_rule__PetriNet__Group__20__Impl1783); 
             after(grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__20__Impl"


    // $ANTLR start "rule__PetriNet__Group__21"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:903:1: rule__PetriNet__Group__21 : rule__PetriNet__Group__21__Impl rule__PetriNet__Group__22 ;
    public final void rule__PetriNet__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:907:1: ( rule__PetriNet__Group__21__Impl rule__PetriNet__Group__22 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:908:2: rule__PetriNet__Group__21__Impl rule__PetriNet__Group__22
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__21__Impl_in_rule__PetriNet__Group__211812);
            rule__PetriNet__Group__21__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__22_in_rule__PetriNet__Group__211815);
            rule__PetriNet__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__21"


    // $ANTLR start "rule__PetriNet__Group__21__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:915:1: rule__PetriNet__Group__21__Impl : ( RULE_TYPE ) ;
    public final void rule__PetriNet__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:919:1: ( ( RULE_TYPE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:920:1: ( RULE_TYPE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:920:1: ( RULE_TYPE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:921:1: RULE_TYPE
            {
             before(grammarAccess.getPetriNetAccess().getTYPETerminalRuleCall_21()); 
            match(input,RULE_TYPE,FollowSets000.FOLLOW_RULE_TYPE_in_rule__PetriNet__Group__21__Impl1842); 
             after(grammarAccess.getPetriNetAccess().getTYPETerminalRuleCall_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__21__Impl"


    // $ANTLR start "rule__PetriNet__Group__22"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:932:1: rule__PetriNet__Group__22 : rule__PetriNet__Group__22__Impl rule__PetriNet__Group__23 ;
    public final void rule__PetriNet__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:936:1: ( rule__PetriNet__Group__22__Impl rule__PetriNet__Group__23 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:937:2: rule__PetriNet__Group__22__Impl rule__PetriNet__Group__23
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__22__Impl_in_rule__PetriNet__Group__221871);
            rule__PetriNet__Group__22__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__23_in_rule__PetriNet__Group__221874);
            rule__PetriNet__Group__23();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__22"


    // $ANTLR start "rule__PetriNet__Group__22__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:944:1: rule__PetriNet__Group__22__Impl : ( RULE_EQUALS ) ;
    public final void rule__PetriNet__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:948:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:949:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:949:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:950:1: RULE_EQUALS
            {
             before(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_22()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__22__Impl1901); 
             after(grammarAccess.getPetriNetAccess().getEQUALSTerminalRuleCall_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__22__Impl"


    // $ANTLR start "rule__PetriNet__Group__23"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:961:1: rule__PetriNet__Group__23 : rule__PetriNet__Group__23__Impl rule__PetriNet__Group__24 ;
    public final void rule__PetriNet__Group__23() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:965:1: ( rule__PetriNet__Group__23__Impl rule__PetriNet__Group__24 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:966:2: rule__PetriNet__Group__23__Impl rule__PetriNet__Group__24
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__23__Impl_in_rule__PetriNet__Group__231930);
            rule__PetriNet__Group__23__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__24_in_rule__PetriNet__Group__231933);
            rule__PetriNet__Group__24();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__23"


    // $ANTLR start "rule__PetriNet__Group__23__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:973:1: rule__PetriNet__Group__23__Impl : ( RULE_STRING ) ;
    public final void rule__PetriNet__Group__23__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:977:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:978:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:978:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:979:1: RULE_STRING
            {
             before(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_23()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__PetriNet__Group__23__Impl1960); 
             after(grammarAccess.getPetriNetAccess().getSTRINGTerminalRuleCall_23()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__23__Impl"


    // $ANTLR start "rule__PetriNet__Group__24"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:990:1: rule__PetriNet__Group__24 : rule__PetriNet__Group__24__Impl rule__PetriNet__Group__25 ;
    public final void rule__PetriNet__Group__24() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:994:1: ( rule__PetriNet__Group__24__Impl rule__PetriNet__Group__25 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:995:2: rule__PetriNet__Group__24__Impl rule__PetriNet__Group__25
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__24__Impl_in_rule__PetriNet__Group__241989);
            rule__PetriNet__Group__24__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__25_in_rule__PetriNet__Group__241992);
            rule__PetriNet__Group__25();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__24"


    // $ANTLR start "rule__PetriNet__Group__24__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1002:1: rule__PetriNet__Group__24__Impl : ( RULE_GREATER ) ;
    public final void rule__PetriNet__Group__24__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1006:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1007:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1007:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1008:1: RULE_GREATER
            {
             before(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_24()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__24__Impl2019); 
             after(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_24()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__24__Impl"


    // $ANTLR start "rule__PetriNet__Group__25"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1019:1: rule__PetriNet__Group__25 : rule__PetriNet__Group__25__Impl rule__PetriNet__Group__26 ;
    public final void rule__PetriNet__Group__25() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1023:1: ( rule__PetriNet__Group__25__Impl rule__PetriNet__Group__26 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1024:2: rule__PetriNet__Group__25__Impl rule__PetriNet__Group__26
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__25__Impl_in_rule__PetriNet__Group__252048);
            rule__PetriNet__Group__25__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__26_in_rule__PetriNet__Group__252051);
            rule__PetriNet__Group__26();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__25"


    // $ANTLR start "rule__PetriNet__Group__25__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1031:1: rule__PetriNet__Group__25__Impl : ( ( rule__PetriNet__ElementsAssignment_25 )* ) ;
    public final void rule__PetriNet__Group__25__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1035:1: ( ( ( rule__PetriNet__ElementsAssignment_25 )* ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1036:1: ( ( rule__PetriNet__ElementsAssignment_25 )* )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1036:1: ( ( rule__PetriNet__ElementsAssignment_25 )* )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1037:1: ( rule__PetriNet__ElementsAssignment_25 )*
            {
             before(grammarAccess.getPetriNetAccess().getElementsAssignment_25()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1038:1: ( rule__PetriNet__ElementsAssignment_25 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_LESS) ) {
                    int LA2_1 = input.LA(2);

                    if ( (LA2_1==RULE_TINPUTARC||(LA2_1>=RULE_TPLACE && LA2_1<=RULE_TTRANSITION)||LA2_1==RULE_TOUTPUTARC) ) {
                        alt2=1;
                    }


                }


                switch (alt2) {
            	case 1 :
            	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1038:2: rule__PetriNet__ElementsAssignment_25
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__PetriNet__ElementsAssignment_25_in_rule__PetriNet__Group__25__Impl2078);
            	    rule__PetriNet__ElementsAssignment_25();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getPetriNetAccess().getElementsAssignment_25()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__25__Impl"


    // $ANTLR start "rule__PetriNet__Group__26"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1048:1: rule__PetriNet__Group__26 : rule__PetriNet__Group__26__Impl rule__PetriNet__Group__27 ;
    public final void rule__PetriNet__Group__26() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1052:1: ( rule__PetriNet__Group__26__Impl rule__PetriNet__Group__27 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1053:2: rule__PetriNet__Group__26__Impl rule__PetriNet__Group__27
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__26__Impl_in_rule__PetriNet__Group__262109);
            rule__PetriNet__Group__26__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__27_in_rule__PetriNet__Group__262112);
            rule__PetriNet__Group__27();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__26"


    // $ANTLR start "rule__PetriNet__Group__26__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1060:1: rule__PetriNet__Group__26__Impl : ( RULE_LESS ) ;
    public final void rule__PetriNet__Group__26__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1064:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1065:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1065:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1066:1: RULE_LESS
            {
             before(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_26()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__PetriNet__Group__26__Impl2139); 
             after(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_26()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__26__Impl"


    // $ANTLR start "rule__PetriNet__Group__27"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1077:1: rule__PetriNet__Group__27 : rule__PetriNet__Group__27__Impl rule__PetriNet__Group__28 ;
    public final void rule__PetriNet__Group__27() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1081:1: ( rule__PetriNet__Group__27__Impl rule__PetriNet__Group__28 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1082:2: rule__PetriNet__Group__27__Impl rule__PetriNet__Group__28
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__27__Impl_in_rule__PetriNet__Group__272168);
            rule__PetriNet__Group__27__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__28_in_rule__PetriNet__Group__272171);
            rule__PetriNet__Group__28();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__27"


    // $ANTLR start "rule__PetriNet__Group__27__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1089:1: rule__PetriNet__Group__27__Impl : ( RULE_SLASH ) ;
    public final void rule__PetriNet__Group__27__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1093:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1094:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1094:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1095:1: RULE_SLASH
            {
             before(grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_27()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__PetriNet__Group__27__Impl2198); 
             after(grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_27()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__27__Impl"


    // $ANTLR start "rule__PetriNet__Group__28"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1106:1: rule__PetriNet__Group__28 : rule__PetriNet__Group__28__Impl rule__PetriNet__Group__29 ;
    public final void rule__PetriNet__Group__28() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1110:1: ( rule__PetriNet__Group__28__Impl rule__PetriNet__Group__29 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1111:2: rule__PetriNet__Group__28__Impl rule__PetriNet__Group__29
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__28__Impl_in_rule__PetriNet__Group__282227);
            rule__PetriNet__Group__28__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__29_in_rule__PetriNet__Group__282230);
            rule__PetriNet__Group__29();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__28"


    // $ANTLR start "rule__PetriNet__Group__28__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1118:1: rule__PetriNet__Group__28__Impl : ( RULE_NET ) ;
    public final void rule__PetriNet__Group__28__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1122:1: ( ( RULE_NET ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1123:1: ( RULE_NET )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1123:1: ( RULE_NET )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1124:1: RULE_NET
            {
             before(grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_28()); 
            match(input,RULE_NET,FollowSets000.FOLLOW_RULE_NET_in_rule__PetriNet__Group__28__Impl2257); 
             after(grammarAccess.getPetriNetAccess().getNETTerminalRuleCall_28()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__28__Impl"


    // $ANTLR start "rule__PetriNet__Group__29"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1135:1: rule__PetriNet__Group__29 : rule__PetriNet__Group__29__Impl rule__PetriNet__Group__30 ;
    public final void rule__PetriNet__Group__29() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1139:1: ( rule__PetriNet__Group__29__Impl rule__PetriNet__Group__30 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1140:2: rule__PetriNet__Group__29__Impl rule__PetriNet__Group__30
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__29__Impl_in_rule__PetriNet__Group__292286);
            rule__PetriNet__Group__29__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__30_in_rule__PetriNet__Group__292289);
            rule__PetriNet__Group__30();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__29"


    // $ANTLR start "rule__PetriNet__Group__29__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1147:1: rule__PetriNet__Group__29__Impl : ( RULE_GREATER ) ;
    public final void rule__PetriNet__Group__29__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1151:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1152:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1152:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1153:1: RULE_GREATER
            {
             before(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_29()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__29__Impl2316); 
             after(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_29()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__29__Impl"


    // $ANTLR start "rule__PetriNet__Group__30"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1164:1: rule__PetriNet__Group__30 : rule__PetriNet__Group__30__Impl rule__PetriNet__Group__31 ;
    public final void rule__PetriNet__Group__30() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1168:1: ( rule__PetriNet__Group__30__Impl rule__PetriNet__Group__31 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1169:2: rule__PetriNet__Group__30__Impl rule__PetriNet__Group__31
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__30__Impl_in_rule__PetriNet__Group__302345);
            rule__PetriNet__Group__30__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__31_in_rule__PetriNet__Group__302348);
            rule__PetriNet__Group__31();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__30"


    // $ANTLR start "rule__PetriNet__Group__30__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1176:1: rule__PetriNet__Group__30__Impl : ( RULE_LESS ) ;
    public final void rule__PetriNet__Group__30__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1180:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1181:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1181:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1182:1: RULE_LESS
            {
             before(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_30()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__PetriNet__Group__30__Impl2375); 
             after(grammarAccess.getPetriNetAccess().getLESSTerminalRuleCall_30()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__30__Impl"


    // $ANTLR start "rule__PetriNet__Group__31"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1193:1: rule__PetriNet__Group__31 : rule__PetriNet__Group__31__Impl rule__PetriNet__Group__32 ;
    public final void rule__PetriNet__Group__31() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1197:1: ( rule__PetriNet__Group__31__Impl rule__PetriNet__Group__32 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1198:2: rule__PetriNet__Group__31__Impl rule__PetriNet__Group__32
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__31__Impl_in_rule__PetriNet__Group__312404);
            rule__PetriNet__Group__31__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__32_in_rule__PetriNet__Group__312407);
            rule__PetriNet__Group__32();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__31"


    // $ANTLR start "rule__PetriNet__Group__31__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1205:1: rule__PetriNet__Group__31__Impl : ( RULE_SLASH ) ;
    public final void rule__PetriNet__Group__31__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1209:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1210:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1210:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1211:1: RULE_SLASH
            {
             before(grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_31()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__PetriNet__Group__31__Impl2434); 
             after(grammarAccess.getPetriNetAccess().getSLASHTerminalRuleCall_31()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__31__Impl"


    // $ANTLR start "rule__PetriNet__Group__32"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1222:1: rule__PetriNet__Group__32 : rule__PetriNet__Group__32__Impl rule__PetriNet__Group__33 ;
    public final void rule__PetriNet__Group__32() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1226:1: ( rule__PetriNet__Group__32__Impl rule__PetriNet__Group__33 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1227:2: rule__PetriNet__Group__32__Impl rule__PetriNet__Group__33
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__32__Impl_in_rule__PetriNet__Group__322463);
            rule__PetriNet__Group__32__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__33_in_rule__PetriNet__Group__322466);
            rule__PetriNet__Group__33();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__32"


    // $ANTLR start "rule__PetriNet__Group__32__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1234:1: rule__PetriNet__Group__32__Impl : ( RULE_PNML ) ;
    public final void rule__PetriNet__Group__32__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1238:1: ( ( RULE_PNML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1239:1: ( RULE_PNML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1239:1: ( RULE_PNML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1240:1: RULE_PNML
            {
             before(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_32()); 
            match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rule__PetriNet__Group__32__Impl2493); 
             after(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_32()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__32__Impl"


    // $ANTLR start "rule__PetriNet__Group__33"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1251:1: rule__PetriNet__Group__33 : rule__PetriNet__Group__33__Impl rule__PetriNet__Group__34 ;
    public final void rule__PetriNet__Group__33() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1255:1: ( rule__PetriNet__Group__33__Impl rule__PetriNet__Group__34 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1256:2: rule__PetriNet__Group__33__Impl rule__PetriNet__Group__34
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__33__Impl_in_rule__PetriNet__Group__332522);
            rule__PetriNet__Group__33__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__34_in_rule__PetriNet__Group__332525);
            rule__PetriNet__Group__34();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__33"


    // $ANTLR start "rule__PetriNet__Group__33__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1263:1: rule__PetriNet__Group__33__Impl : ( RULE_COLON ) ;
    public final void rule__PetriNet__Group__33__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1267:1: ( ( RULE_COLON ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1268:1: ( RULE_COLON )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1268:1: ( RULE_COLON )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1269:1: RULE_COLON
            {
             before(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_33()); 
            match(input,RULE_COLON,FollowSets000.FOLLOW_RULE_COLON_in_rule__PetriNet__Group__33__Impl2552); 
             after(grammarAccess.getPetriNetAccess().getCOLONTerminalRuleCall_33()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__33__Impl"


    // $ANTLR start "rule__PetriNet__Group__34"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1280:1: rule__PetriNet__Group__34 : rule__PetriNet__Group__34__Impl rule__PetriNet__Group__35 ;
    public final void rule__PetriNet__Group__34() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1284:1: ( rule__PetriNet__Group__34__Impl rule__PetriNet__Group__35 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1285:2: rule__PetriNet__Group__34__Impl rule__PetriNet__Group__35
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__34__Impl_in_rule__PetriNet__Group__342581);
            rule__PetriNet__Group__34__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__35_in_rule__PetriNet__Group__342584);
            rule__PetriNet__Group__35();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__34"


    // $ANTLR start "rule__PetriNet__Group__34__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1292:1: rule__PetriNet__Group__34__Impl : ( RULE_PNML ) ;
    public final void rule__PetriNet__Group__34__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1296:1: ( ( RULE_PNML ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1297:1: ( RULE_PNML )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1297:1: ( RULE_PNML )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1298:1: RULE_PNML
            {
             before(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_34()); 
            match(input,RULE_PNML,FollowSets000.FOLLOW_RULE_PNML_in_rule__PetriNet__Group__34__Impl2611); 
             after(grammarAccess.getPetriNetAccess().getPNMLTerminalRuleCall_34()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__34__Impl"


    // $ANTLR start "rule__PetriNet__Group__35"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1309:1: rule__PetriNet__Group__35 : rule__PetriNet__Group__35__Impl ;
    public final void rule__PetriNet__Group__35() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1313:1: ( rule__PetriNet__Group__35__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1314:2: rule__PetriNet__Group__35__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__PetriNet__Group__35__Impl_in_rule__PetriNet__Group__352640);
            rule__PetriNet__Group__35__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__35"


    // $ANTLR start "rule__PetriNet__Group__35__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1320:1: rule__PetriNet__Group__35__Impl : ( RULE_GREATER ) ;
    public final void rule__PetriNet__Group__35__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1324:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1325:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1325:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1326:1: RULE_GREATER
            {
             before(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_35()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__35__Impl2667); 
             after(grammarAccess.getPetriNetAccess().getGREATERTerminalRuleCall_35()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__Group__35__Impl"


    // $ANTLR start "rule__InputArc__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1409:1: rule__InputArc__Group__0 : rule__InputArc__Group__0__Impl rule__InputArc__Group__1 ;
    public final void rule__InputArc__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1413:1: ( rule__InputArc__Group__0__Impl rule__InputArc__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1414:2: rule__InputArc__Group__0__Impl rule__InputArc__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__0__Impl_in_rule__InputArc__Group__02768);
            rule__InputArc__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__1_in_rule__InputArc__Group__02771);
            rule__InputArc__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__0"


    // $ANTLR start "rule__InputArc__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1421:1: rule__InputArc__Group__0__Impl : ( RULE_LESS ) ;
    public final void rule__InputArc__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1425:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1426:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1426:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1427:1: RULE_LESS
            {
             before(grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_0()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__InputArc__Group__0__Impl2798); 
             after(grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__0__Impl"


    // $ANTLR start "rule__InputArc__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1438:1: rule__InputArc__Group__1 : rule__InputArc__Group__1__Impl rule__InputArc__Group__2 ;
    public final void rule__InputArc__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1442:1: ( rule__InputArc__Group__1__Impl rule__InputArc__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1443:2: rule__InputArc__Group__1__Impl rule__InputArc__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__1__Impl_in_rule__InputArc__Group__12827);
            rule__InputArc__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__2_in_rule__InputArc__Group__12830);
            rule__InputArc__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__1"


    // $ANTLR start "rule__InputArc__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1450:1: rule__InputArc__Group__1__Impl : ( RULE_TINPUTARC ) ;
    public final void rule__InputArc__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1454:1: ( ( RULE_TINPUTARC ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1455:1: ( RULE_TINPUTARC )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1455:1: ( RULE_TINPUTARC )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1456:1: RULE_TINPUTARC
            {
             before(grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_1()); 
            match(input,RULE_TINPUTARC,FollowSets000.FOLLOW_RULE_TINPUTARC_in_rule__InputArc__Group__1__Impl2857); 
             after(grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__1__Impl"


    // $ANTLR start "rule__InputArc__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1467:1: rule__InputArc__Group__2 : rule__InputArc__Group__2__Impl rule__InputArc__Group__3 ;
    public final void rule__InputArc__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1471:1: ( rule__InputArc__Group__2__Impl rule__InputArc__Group__3 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1472:2: rule__InputArc__Group__2__Impl rule__InputArc__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__2__Impl_in_rule__InputArc__Group__22886);
            rule__InputArc__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__3_in_rule__InputArc__Group__22889);
            rule__InputArc__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__2"


    // $ANTLR start "rule__InputArc__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1479:1: rule__InputArc__Group__2__Impl : ( ( rule__InputArc__UnorderedGroup_2 ) ) ;
    public final void rule__InputArc__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1483:1: ( ( ( rule__InputArc__UnorderedGroup_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1484:1: ( ( rule__InputArc__UnorderedGroup_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1484:1: ( ( rule__InputArc__UnorderedGroup_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1485:1: ( rule__InputArc__UnorderedGroup_2 )
            {
             before(grammarAccess.getInputArcAccess().getUnorderedGroup_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1486:1: ( rule__InputArc__UnorderedGroup_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1486:2: rule__InputArc__UnorderedGroup_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2_in_rule__InputArc__Group__2__Impl2916);
            rule__InputArc__UnorderedGroup_2();

            state._fsp--;


            }

             after(grammarAccess.getInputArcAccess().getUnorderedGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__2__Impl"


    // $ANTLR start "rule__InputArc__Group__3"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1496:1: rule__InputArc__Group__3 : rule__InputArc__Group__3__Impl rule__InputArc__Group__4 ;
    public final void rule__InputArc__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1500:1: ( rule__InputArc__Group__3__Impl rule__InputArc__Group__4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1501:2: rule__InputArc__Group__3__Impl rule__InputArc__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__3__Impl_in_rule__InputArc__Group__32946);
            rule__InputArc__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__4_in_rule__InputArc__Group__32949);
            rule__InputArc__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__3"


    // $ANTLR start "rule__InputArc__Group__3__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1508:1: rule__InputArc__Group__3__Impl : ( RULE_GREATER ) ;
    public final void rule__InputArc__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1512:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1513:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1513:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1514:1: RULE_GREATER
            {
             before(grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_3()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__InputArc__Group__3__Impl2976); 
             after(grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__3__Impl"


    // $ANTLR start "rule__InputArc__Group__4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1525:1: rule__InputArc__Group__4 : rule__InputArc__Group__4__Impl rule__InputArc__Group__5 ;
    public final void rule__InputArc__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1529:1: ( rule__InputArc__Group__4__Impl rule__InputArc__Group__5 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1530:2: rule__InputArc__Group__4__Impl rule__InputArc__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__4__Impl_in_rule__InputArc__Group__43005);
            rule__InputArc__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__5_in_rule__InputArc__Group__43008);
            rule__InputArc__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__4"


    // $ANTLR start "rule__InputArc__Group__4__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1537:1: rule__InputArc__Group__4__Impl : ( RULE_LESS ) ;
    public final void rule__InputArc__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1541:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1542:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1542:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1543:1: RULE_LESS
            {
             before(grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_4()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__InputArc__Group__4__Impl3035); 
             after(grammarAccess.getInputArcAccess().getLESSTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__4__Impl"


    // $ANTLR start "rule__InputArc__Group__5"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1554:1: rule__InputArc__Group__5 : rule__InputArc__Group__5__Impl rule__InputArc__Group__6 ;
    public final void rule__InputArc__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1558:1: ( rule__InputArc__Group__5__Impl rule__InputArc__Group__6 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1559:2: rule__InputArc__Group__5__Impl rule__InputArc__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__5__Impl_in_rule__InputArc__Group__53064);
            rule__InputArc__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__6_in_rule__InputArc__Group__53067);
            rule__InputArc__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__5"


    // $ANTLR start "rule__InputArc__Group__5__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1566:1: rule__InputArc__Group__5__Impl : ( RULE_SLASH ) ;
    public final void rule__InputArc__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1570:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1571:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1571:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1572:1: RULE_SLASH
            {
             before(grammarAccess.getInputArcAccess().getSLASHTerminalRuleCall_5()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__InputArc__Group__5__Impl3094); 
             after(grammarAccess.getInputArcAccess().getSLASHTerminalRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__5__Impl"


    // $ANTLR start "rule__InputArc__Group__6"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1583:1: rule__InputArc__Group__6 : rule__InputArc__Group__6__Impl rule__InputArc__Group__7 ;
    public final void rule__InputArc__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1587:1: ( rule__InputArc__Group__6__Impl rule__InputArc__Group__7 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1588:2: rule__InputArc__Group__6__Impl rule__InputArc__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__6__Impl_in_rule__InputArc__Group__63123);
            rule__InputArc__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__7_in_rule__InputArc__Group__63126);
            rule__InputArc__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__6"


    // $ANTLR start "rule__InputArc__Group__6__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1595:1: rule__InputArc__Group__6__Impl : ( RULE_TINPUTARC ) ;
    public final void rule__InputArc__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1599:1: ( ( RULE_TINPUTARC ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1600:1: ( RULE_TINPUTARC )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1600:1: ( RULE_TINPUTARC )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1601:1: RULE_TINPUTARC
            {
             before(grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_6()); 
            match(input,RULE_TINPUTARC,FollowSets000.FOLLOW_RULE_TINPUTARC_in_rule__InputArc__Group__6__Impl3153); 
             after(grammarAccess.getInputArcAccess().getTINPUTARCTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__6__Impl"


    // $ANTLR start "rule__InputArc__Group__7"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1612:1: rule__InputArc__Group__7 : rule__InputArc__Group__7__Impl ;
    public final void rule__InputArc__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1616:1: ( rule__InputArc__Group__7__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1617:2: rule__InputArc__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group__7__Impl_in_rule__InputArc__Group__73182);
            rule__InputArc__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__7"


    // $ANTLR start "rule__InputArc__Group__7__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1623:1: rule__InputArc__Group__7__Impl : ( RULE_GREATER ) ;
    public final void rule__InputArc__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1627:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1628:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1628:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1629:1: RULE_GREATER
            {
             before(grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_7()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__InputArc__Group__7__Impl3209); 
             after(grammarAccess.getInputArcAccess().getGREATERTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group__7__Impl"


    // $ANTLR start "rule__InputArc__Group_2_0__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1656:1: rule__InputArc__Group_2_0__0 : rule__InputArc__Group_2_0__0__Impl rule__InputArc__Group_2_0__1 ;
    public final void rule__InputArc__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1660:1: ( rule__InputArc__Group_2_0__0__Impl rule__InputArc__Group_2_0__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1661:2: rule__InputArc__Group_2_0__0__Impl rule__InputArc__Group_2_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__0__Impl_in_rule__InputArc__Group_2_0__03254);
            rule__InputArc__Group_2_0__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__1_in_rule__InputArc__Group_2_0__03257);
            rule__InputArc__Group_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__0"


    // $ANTLR start "rule__InputArc__Group_2_0__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1668:1: rule__InputArc__Group_2_0__0__Impl : ( RULE_ID ) ;
    public final void rule__InputArc__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1672:1: ( ( RULE_ID ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1673:1: ( RULE_ID )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1673:1: ( RULE_ID )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1674:1: RULE_ID
            {
             before(grammarAccess.getInputArcAccess().getIDTerminalRuleCall_2_0_0()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__InputArc__Group_2_0__0__Impl3284); 
             after(grammarAccess.getInputArcAccess().getIDTerminalRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__0__Impl"


    // $ANTLR start "rule__InputArc__Group_2_0__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1685:1: rule__InputArc__Group_2_0__1 : rule__InputArc__Group_2_0__1__Impl rule__InputArc__Group_2_0__2 ;
    public final void rule__InputArc__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1689:1: ( rule__InputArc__Group_2_0__1__Impl rule__InputArc__Group_2_0__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1690:2: rule__InputArc__Group_2_0__1__Impl rule__InputArc__Group_2_0__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__1__Impl_in_rule__InputArc__Group_2_0__13313);
            rule__InputArc__Group_2_0__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__2_in_rule__InputArc__Group_2_0__13316);
            rule__InputArc__Group_2_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__1"


    // $ANTLR start "rule__InputArc__Group_2_0__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1697:1: rule__InputArc__Group_2_0__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__InputArc__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1701:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1702:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1702:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1703:1: RULE_EQUALS
            {
             before(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_0__1__Impl3343); 
             after(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__1__Impl"


    // $ANTLR start "rule__InputArc__Group_2_0__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1714:1: rule__InputArc__Group_2_0__2 : rule__InputArc__Group_2_0__2__Impl ;
    public final void rule__InputArc__Group_2_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1718:1: ( rule__InputArc__Group_2_0__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1719:2: rule__InputArc__Group_2_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__2__Impl_in_rule__InputArc__Group_2_0__23372);
            rule__InputArc__Group_2_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__2"


    // $ANTLR start "rule__InputArc__Group_2_0__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1725:1: rule__InputArc__Group_2_0__2__Impl : ( ( rule__InputArc__NameAssignment_2_0_2 ) ) ;
    public final void rule__InputArc__Group_2_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1729:1: ( ( ( rule__InputArc__NameAssignment_2_0_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1730:1: ( ( rule__InputArc__NameAssignment_2_0_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1730:1: ( ( rule__InputArc__NameAssignment_2_0_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1731:1: ( rule__InputArc__NameAssignment_2_0_2 )
            {
             before(grammarAccess.getInputArcAccess().getNameAssignment_2_0_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1732:1: ( rule__InputArc__NameAssignment_2_0_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1732:2: rule__InputArc__NameAssignment_2_0_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__NameAssignment_2_0_2_in_rule__InputArc__Group_2_0__2__Impl3399);
            rule__InputArc__NameAssignment_2_0_2();

            state._fsp--;


            }

             after(grammarAccess.getInputArcAccess().getNameAssignment_2_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_0__2__Impl"


    // $ANTLR start "rule__InputArc__Group_2_1__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1748:1: rule__InputArc__Group_2_1__0 : rule__InputArc__Group_2_1__0__Impl rule__InputArc__Group_2_1__1 ;
    public final void rule__InputArc__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1752:1: ( rule__InputArc__Group_2_1__0__Impl rule__InputArc__Group_2_1__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1753:2: rule__InputArc__Group_2_1__0__Impl rule__InputArc__Group_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__0__Impl_in_rule__InputArc__Group_2_1__03435);
            rule__InputArc__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__1_in_rule__InputArc__Group_2_1__03438);
            rule__InputArc__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__0"


    // $ANTLR start "rule__InputArc__Group_2_1__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1760:1: rule__InputArc__Group_2_1__0__Impl : ( RULE_SOURCE ) ;
    public final void rule__InputArc__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1764:1: ( ( RULE_SOURCE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1765:1: ( RULE_SOURCE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1765:1: ( RULE_SOURCE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1766:1: RULE_SOURCE
            {
             before(grammarAccess.getInputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 
            match(input,RULE_SOURCE,FollowSets000.FOLLOW_RULE_SOURCE_in_rule__InputArc__Group_2_1__0__Impl3465); 
             after(grammarAccess.getInputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__0__Impl"


    // $ANTLR start "rule__InputArc__Group_2_1__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1777:1: rule__InputArc__Group_2_1__1 : rule__InputArc__Group_2_1__1__Impl rule__InputArc__Group_2_1__2 ;
    public final void rule__InputArc__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1781:1: ( rule__InputArc__Group_2_1__1__Impl rule__InputArc__Group_2_1__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1782:2: rule__InputArc__Group_2_1__1__Impl rule__InputArc__Group_2_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__1__Impl_in_rule__InputArc__Group_2_1__13494);
            rule__InputArc__Group_2_1__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__2_in_rule__InputArc__Group_2_1__13497);
            rule__InputArc__Group_2_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__1"


    // $ANTLR start "rule__InputArc__Group_2_1__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1789:1: rule__InputArc__Group_2_1__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__InputArc__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1793:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1794:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1794:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1795:1: RULE_EQUALS
            {
             before(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_1__1__Impl3524); 
             after(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__1__Impl"


    // $ANTLR start "rule__InputArc__Group_2_1__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1806:1: rule__InputArc__Group_2_1__2 : rule__InputArc__Group_2_1__2__Impl ;
    public final void rule__InputArc__Group_2_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1810:1: ( rule__InputArc__Group_2_1__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1811:2: rule__InputArc__Group_2_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__2__Impl_in_rule__InputArc__Group_2_1__23553);
            rule__InputArc__Group_2_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__2"


    // $ANTLR start "rule__InputArc__Group_2_1__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1817:1: rule__InputArc__Group_2_1__2__Impl : ( ( rule__InputArc__FromAssignment_2_1_2 ) ) ;
    public final void rule__InputArc__Group_2_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1821:1: ( ( ( rule__InputArc__FromAssignment_2_1_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1822:1: ( ( rule__InputArc__FromAssignment_2_1_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1822:1: ( ( rule__InputArc__FromAssignment_2_1_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1823:1: ( rule__InputArc__FromAssignment_2_1_2 )
            {
             before(grammarAccess.getInputArcAccess().getFromAssignment_2_1_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1824:1: ( rule__InputArc__FromAssignment_2_1_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1824:2: rule__InputArc__FromAssignment_2_1_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__FromAssignment_2_1_2_in_rule__InputArc__Group_2_1__2__Impl3580);
            rule__InputArc__FromAssignment_2_1_2();

            state._fsp--;


            }

             after(grammarAccess.getInputArcAccess().getFromAssignment_2_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_1__2__Impl"


    // $ANTLR start "rule__InputArc__Group_2_2__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1840:1: rule__InputArc__Group_2_2__0 : rule__InputArc__Group_2_2__0__Impl rule__InputArc__Group_2_2__1 ;
    public final void rule__InputArc__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1844:1: ( rule__InputArc__Group_2_2__0__Impl rule__InputArc__Group_2_2__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1845:2: rule__InputArc__Group_2_2__0__Impl rule__InputArc__Group_2_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__0__Impl_in_rule__InputArc__Group_2_2__03616);
            rule__InputArc__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__1_in_rule__InputArc__Group_2_2__03619);
            rule__InputArc__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__0"


    // $ANTLR start "rule__InputArc__Group_2_2__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1852:1: rule__InputArc__Group_2_2__0__Impl : ( RULE_TARGET ) ;
    public final void rule__InputArc__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1856:1: ( ( RULE_TARGET ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1857:1: ( RULE_TARGET )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1857:1: ( RULE_TARGET )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1858:1: RULE_TARGET
            {
             before(grammarAccess.getInputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 
            match(input,RULE_TARGET,FollowSets000.FOLLOW_RULE_TARGET_in_rule__InputArc__Group_2_2__0__Impl3646); 
             after(grammarAccess.getInputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__0__Impl"


    // $ANTLR start "rule__InputArc__Group_2_2__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1869:1: rule__InputArc__Group_2_2__1 : rule__InputArc__Group_2_2__1__Impl rule__InputArc__Group_2_2__2 ;
    public final void rule__InputArc__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1873:1: ( rule__InputArc__Group_2_2__1__Impl rule__InputArc__Group_2_2__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1874:2: rule__InputArc__Group_2_2__1__Impl rule__InputArc__Group_2_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__1__Impl_in_rule__InputArc__Group_2_2__13675);
            rule__InputArc__Group_2_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__2_in_rule__InputArc__Group_2_2__13678);
            rule__InputArc__Group_2_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__1"


    // $ANTLR start "rule__InputArc__Group_2_2__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1881:1: rule__InputArc__Group_2_2__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__InputArc__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1885:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1886:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1886:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1887:1: RULE_EQUALS
            {
             before(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_2__1__Impl3705); 
             after(grammarAccess.getInputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__1__Impl"


    // $ANTLR start "rule__InputArc__Group_2_2__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1898:1: rule__InputArc__Group_2_2__2 : rule__InputArc__Group_2_2__2__Impl ;
    public final void rule__InputArc__Group_2_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1902:1: ( rule__InputArc__Group_2_2__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1903:2: rule__InputArc__Group_2_2__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__2__Impl_in_rule__InputArc__Group_2_2__23734);
            rule__InputArc__Group_2_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__2"


    // $ANTLR start "rule__InputArc__Group_2_2__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1909:1: rule__InputArc__Group_2_2__2__Impl : ( ( rule__InputArc__ToAssignment_2_2_2 ) ) ;
    public final void rule__InputArc__Group_2_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1913:1: ( ( ( rule__InputArc__ToAssignment_2_2_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1914:1: ( ( rule__InputArc__ToAssignment_2_2_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1914:1: ( ( rule__InputArc__ToAssignment_2_2_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1915:1: ( rule__InputArc__ToAssignment_2_2_2 )
            {
             before(grammarAccess.getInputArcAccess().getToAssignment_2_2_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1916:1: ( rule__InputArc__ToAssignment_2_2_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1916:2: rule__InputArc__ToAssignment_2_2_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__ToAssignment_2_2_2_in_rule__InputArc__Group_2_2__2__Impl3761);
            rule__InputArc__ToAssignment_2_2_2();

            state._fsp--;


            }

             after(grammarAccess.getInputArcAccess().getToAssignment_2_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__Group_2_2__2__Impl"


    // $ANTLR start "rule__Place__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1932:1: rule__Place__Group__0 : rule__Place__Group__0__Impl rule__Place__Group__1 ;
    public final void rule__Place__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1936:1: ( rule__Place__Group__0__Impl rule__Place__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1937:2: rule__Place__Group__0__Impl rule__Place__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__0__Impl_in_rule__Place__Group__03797);
            rule__Place__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__1_in_rule__Place__Group__03800);
            rule__Place__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0"


    // $ANTLR start "rule__Place__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1944:1: rule__Place__Group__0__Impl : ( RULE_LESS ) ;
    public final void rule__Place__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1948:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1949:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1949:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1950:1: RULE_LESS
            {
             before(grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_0()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__Place__Group__0__Impl3827); 
             after(grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__0__Impl"


    // $ANTLR start "rule__Place__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1961:1: rule__Place__Group__1 : rule__Place__Group__1__Impl rule__Place__Group__2 ;
    public final void rule__Place__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1965:1: ( rule__Place__Group__1__Impl rule__Place__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1966:2: rule__Place__Group__1__Impl rule__Place__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__1__Impl_in_rule__Place__Group__13856);
            rule__Place__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__2_in_rule__Place__Group__13859);
            rule__Place__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1"


    // $ANTLR start "rule__Place__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1973:1: rule__Place__Group__1__Impl : ( RULE_TPLACE ) ;
    public final void rule__Place__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1977:1: ( ( RULE_TPLACE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1978:1: ( RULE_TPLACE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1978:1: ( RULE_TPLACE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1979:1: RULE_TPLACE
            {
             before(grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_1()); 
            match(input,RULE_TPLACE,FollowSets000.FOLLOW_RULE_TPLACE_in_rule__Place__Group__1__Impl3886); 
             after(grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__1__Impl"


    // $ANTLR start "rule__Place__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1990:1: rule__Place__Group__2 : rule__Place__Group__2__Impl rule__Place__Group__3 ;
    public final void rule__Place__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1994:1: ( rule__Place__Group__2__Impl rule__Place__Group__3 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1995:2: rule__Place__Group__2__Impl rule__Place__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__2__Impl_in_rule__Place__Group__23915);
            rule__Place__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__3_in_rule__Place__Group__23918);
            rule__Place__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2"


    // $ANTLR start "rule__Place__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2002:1: rule__Place__Group__2__Impl : ( RULE_ID ) ;
    public final void rule__Place__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2006:1: ( ( RULE_ID ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2007:1: ( RULE_ID )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2007:1: ( RULE_ID )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2008:1: RULE_ID
            {
             before(grammarAccess.getPlaceAccess().getIDTerminalRuleCall_2()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Place__Group__2__Impl3945); 
             after(grammarAccess.getPlaceAccess().getIDTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__2__Impl"


    // $ANTLR start "rule__Place__Group__3"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2019:1: rule__Place__Group__3 : rule__Place__Group__3__Impl rule__Place__Group__4 ;
    public final void rule__Place__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2023:1: ( rule__Place__Group__3__Impl rule__Place__Group__4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2024:2: rule__Place__Group__3__Impl rule__Place__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__3__Impl_in_rule__Place__Group__33974);
            rule__Place__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__4_in_rule__Place__Group__33977);
            rule__Place__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3"


    // $ANTLR start "rule__Place__Group__3__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2031:1: rule__Place__Group__3__Impl : ( RULE_EQUALS ) ;
    public final void rule__Place__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2035:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2036:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2036:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2037:1: RULE_EQUALS
            {
             before(grammarAccess.getPlaceAccess().getEQUALSTerminalRuleCall_3()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__Place__Group__3__Impl4004); 
             after(grammarAccess.getPlaceAccess().getEQUALSTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__3__Impl"


    // $ANTLR start "rule__Place__Group__4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2048:1: rule__Place__Group__4 : rule__Place__Group__4__Impl rule__Place__Group__5 ;
    public final void rule__Place__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2052:1: ( rule__Place__Group__4__Impl rule__Place__Group__5 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2053:2: rule__Place__Group__4__Impl rule__Place__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__4__Impl_in_rule__Place__Group__44033);
            rule__Place__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__5_in_rule__Place__Group__44036);
            rule__Place__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4"


    // $ANTLR start "rule__Place__Group__4__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2060:1: rule__Place__Group__4__Impl : ( ( rule__Place__NameAssignment_4 ) ) ;
    public final void rule__Place__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2064:1: ( ( ( rule__Place__NameAssignment_4 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2065:1: ( ( rule__Place__NameAssignment_4 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2065:1: ( ( rule__Place__NameAssignment_4 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2066:1: ( rule__Place__NameAssignment_4 )
            {
             before(grammarAccess.getPlaceAccess().getNameAssignment_4()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2067:1: ( rule__Place__NameAssignment_4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2067:2: rule__Place__NameAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__NameAssignment_4_in_rule__Place__Group__4__Impl4063);
            rule__Place__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPlaceAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__4__Impl"


    // $ANTLR start "rule__Place__Group__5"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2077:1: rule__Place__Group__5 : rule__Place__Group__5__Impl rule__Place__Group__6 ;
    public final void rule__Place__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2081:1: ( rule__Place__Group__5__Impl rule__Place__Group__6 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2082:2: rule__Place__Group__5__Impl rule__Place__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__5__Impl_in_rule__Place__Group__54093);
            rule__Place__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__6_in_rule__Place__Group__54096);
            rule__Place__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__5"


    // $ANTLR start "rule__Place__Group__5__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2089:1: rule__Place__Group__5__Impl : ( RULE_GREATER ) ;
    public final void rule__Place__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2093:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2094:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2094:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2095:1: RULE_GREATER
            {
             before(grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_5()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__Place__Group__5__Impl4123); 
             after(grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__5__Impl"


    // $ANTLR start "rule__Place__Group__6"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2106:1: rule__Place__Group__6 : rule__Place__Group__6__Impl rule__Place__Group__7 ;
    public final void rule__Place__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2110:1: ( rule__Place__Group__6__Impl rule__Place__Group__7 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2111:2: rule__Place__Group__6__Impl rule__Place__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__6__Impl_in_rule__Place__Group__64152);
            rule__Place__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__7_in_rule__Place__Group__64155);
            rule__Place__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__6"


    // $ANTLR start "rule__Place__Group__6__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2118:1: rule__Place__Group__6__Impl : ( RULE_LESS ) ;
    public final void rule__Place__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2122:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2123:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2123:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2124:1: RULE_LESS
            {
             before(grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_6()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__Place__Group__6__Impl4182); 
             after(grammarAccess.getPlaceAccess().getLESSTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__6__Impl"


    // $ANTLR start "rule__Place__Group__7"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2135:1: rule__Place__Group__7 : rule__Place__Group__7__Impl rule__Place__Group__8 ;
    public final void rule__Place__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2139:1: ( rule__Place__Group__7__Impl rule__Place__Group__8 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2140:2: rule__Place__Group__7__Impl rule__Place__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__7__Impl_in_rule__Place__Group__74211);
            rule__Place__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__8_in_rule__Place__Group__74214);
            rule__Place__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__7"


    // $ANTLR start "rule__Place__Group__7__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2147:1: rule__Place__Group__7__Impl : ( RULE_SLASH ) ;
    public final void rule__Place__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2151:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2152:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2152:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2153:1: RULE_SLASH
            {
             before(grammarAccess.getPlaceAccess().getSLASHTerminalRuleCall_7()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__Place__Group__7__Impl4241); 
             after(grammarAccess.getPlaceAccess().getSLASHTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__7__Impl"


    // $ANTLR start "rule__Place__Group__8"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2164:1: rule__Place__Group__8 : rule__Place__Group__8__Impl rule__Place__Group__9 ;
    public final void rule__Place__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2168:1: ( rule__Place__Group__8__Impl rule__Place__Group__9 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2169:2: rule__Place__Group__8__Impl rule__Place__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__8__Impl_in_rule__Place__Group__84270);
            rule__Place__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__9_in_rule__Place__Group__84273);
            rule__Place__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__8"


    // $ANTLR start "rule__Place__Group__8__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2176:1: rule__Place__Group__8__Impl : ( RULE_TPLACE ) ;
    public final void rule__Place__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2180:1: ( ( RULE_TPLACE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2181:1: ( RULE_TPLACE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2181:1: ( RULE_TPLACE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2182:1: RULE_TPLACE
            {
             before(grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_8()); 
            match(input,RULE_TPLACE,FollowSets000.FOLLOW_RULE_TPLACE_in_rule__Place__Group__8__Impl4300); 
             after(grammarAccess.getPlaceAccess().getTPLACETerminalRuleCall_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__8__Impl"


    // $ANTLR start "rule__Place__Group__9"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2193:1: rule__Place__Group__9 : rule__Place__Group__9__Impl ;
    public final void rule__Place__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2197:1: ( rule__Place__Group__9__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2198:2: rule__Place__Group__9__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Place__Group__9__Impl_in_rule__Place__Group__94329);
            rule__Place__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__9"


    // $ANTLR start "rule__Place__Group__9__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2204:1: rule__Place__Group__9__Impl : ( RULE_GREATER ) ;
    public final void rule__Place__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2208:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2209:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2209:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2210:1: RULE_GREATER
            {
             before(grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_9()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__Place__Group__9__Impl4356); 
             after(grammarAccess.getPlaceAccess().getGREATERTerminalRuleCall_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__Group__9__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2241:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2245:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2246:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__04405);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__04408);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2253:1: rule__Transition__Group__0__Impl : ( RULE_LESS ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2257:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2258:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2258:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2259:1: RULE_LESS
            {
             before(grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_0()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__Transition__Group__0__Impl4435); 
             after(grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2270:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2274:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2275:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__14464);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__14467);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2282:1: rule__Transition__Group__1__Impl : ( RULE_TTRANSITION ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2286:1: ( ( RULE_TTRANSITION ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2287:1: ( RULE_TTRANSITION )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2287:1: ( RULE_TTRANSITION )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2288:1: RULE_TTRANSITION
            {
             before(grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_1()); 
            match(input,RULE_TTRANSITION,FollowSets000.FOLLOW_RULE_TTRANSITION_in_rule__Transition__Group__1__Impl4494); 
             after(grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2299:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2303:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2304:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__24523);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__24526);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2311:1: rule__Transition__Group__2__Impl : ( RULE_ID ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2315:1: ( ( RULE_ID ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2316:1: ( RULE_ID )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2316:1: ( RULE_ID )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2317:1: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getIDTerminalRuleCall_2()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Transition__Group__2__Impl4553); 
             after(grammarAccess.getTransitionAccess().getIDTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2328:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2332:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2333:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__34582);
            rule__Transition__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__34585);
            rule__Transition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2340:1: rule__Transition__Group__3__Impl : ( RULE_EQUALS ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2344:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2345:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2345:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2346:1: RULE_EQUALS
            {
             before(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_3()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__Transition__Group__3__Impl4612); 
             after(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2357:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl rule__Transition__Group__5 ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2361:1: ( rule__Transition__Group__4__Impl rule__Transition__Group__5 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2362:2: rule__Transition__Group__4__Impl rule__Transition__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__44641);
            rule__Transition__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__5_in_rule__Transition__Group__44644);
            rule__Transition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2369:1: rule__Transition__Group__4__Impl : ( ( rule__Transition__NameAssignment_4 ) ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2373:1: ( ( ( rule__Transition__NameAssignment_4 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2374:1: ( ( rule__Transition__NameAssignment_4 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2374:1: ( ( rule__Transition__NameAssignment_4 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2375:1: ( rule__Transition__NameAssignment_4 )
            {
             before(grammarAccess.getTransitionAccess().getNameAssignment_4()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2376:1: ( rule__Transition__NameAssignment_4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2376:2: rule__Transition__NameAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__NameAssignment_4_in_rule__Transition__Group__4__Impl4671);
            rule__Transition__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__5"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2386:1: rule__Transition__Group__5 : rule__Transition__Group__5__Impl rule__Transition__Group__6 ;
    public final void rule__Transition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2390:1: ( rule__Transition__Group__5__Impl rule__Transition__Group__6 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2391:2: rule__Transition__Group__5__Impl rule__Transition__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__5__Impl_in_rule__Transition__Group__54701);
            rule__Transition__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__6_in_rule__Transition__Group__54704);
            rule__Transition__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5"


    // $ANTLR start "rule__Transition__Group__5__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2398:1: rule__Transition__Group__5__Impl : ( ( rule__Transition__Group_5__0 )? ) ;
    public final void rule__Transition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2402:1: ( ( ( rule__Transition__Group_5__0 )? ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2403:1: ( ( rule__Transition__Group_5__0 )? )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2403:1: ( ( rule__Transition__Group_5__0 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2404:1: ( rule__Transition__Group_5__0 )?
            {
             before(grammarAccess.getTransitionAccess().getGroup_5()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2405:1: ( rule__Transition__Group_5__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_MAXDELAY) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2405:2: rule__Transition__Group_5__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__0_in_rule__Transition__Group__5__Impl4731);
                    rule__Transition__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5__Impl"


    // $ANTLR start "rule__Transition__Group__6"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2415:1: rule__Transition__Group__6 : rule__Transition__Group__6__Impl rule__Transition__Group__7 ;
    public final void rule__Transition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2419:1: ( rule__Transition__Group__6__Impl rule__Transition__Group__7 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2420:2: rule__Transition__Group__6__Impl rule__Transition__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__6__Impl_in_rule__Transition__Group__64762);
            rule__Transition__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__7_in_rule__Transition__Group__64765);
            rule__Transition__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6"


    // $ANTLR start "rule__Transition__Group__6__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2427:1: rule__Transition__Group__6__Impl : ( ( rule__Transition__Group_6__0 )? ) ;
    public final void rule__Transition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2431:1: ( ( ( rule__Transition__Group_6__0 )? ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2432:1: ( ( rule__Transition__Group_6__0 )? )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2432:1: ( ( rule__Transition__Group_6__0 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2433:1: ( rule__Transition__Group_6__0 )?
            {
             before(grammarAccess.getTransitionAccess().getGroup_6()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2434:1: ( rule__Transition__Group_6__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_MINDELAY) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2434:2: rule__Transition__Group_6__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__0_in_rule__Transition__Group__6__Impl4792);
                    rule__Transition__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTransitionAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__6__Impl"


    // $ANTLR start "rule__Transition__Group__7"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2444:1: rule__Transition__Group__7 : rule__Transition__Group__7__Impl rule__Transition__Group__8 ;
    public final void rule__Transition__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2448:1: ( rule__Transition__Group__7__Impl rule__Transition__Group__8 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2449:2: rule__Transition__Group__7__Impl rule__Transition__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__7__Impl_in_rule__Transition__Group__74823);
            rule__Transition__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__8_in_rule__Transition__Group__74826);
            rule__Transition__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__7"


    // $ANTLR start "rule__Transition__Group__7__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2456:1: rule__Transition__Group__7__Impl : ( RULE_GREATER ) ;
    public final void rule__Transition__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2460:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2461:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2461:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2462:1: RULE_GREATER
            {
             before(grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_7()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__Transition__Group__7__Impl4853); 
             after(grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__7__Impl"


    // $ANTLR start "rule__Transition__Group__8"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2473:1: rule__Transition__Group__8 : rule__Transition__Group__8__Impl rule__Transition__Group__9 ;
    public final void rule__Transition__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2477:1: ( rule__Transition__Group__8__Impl rule__Transition__Group__9 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2478:2: rule__Transition__Group__8__Impl rule__Transition__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__8__Impl_in_rule__Transition__Group__84882);
            rule__Transition__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__9_in_rule__Transition__Group__84885);
            rule__Transition__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__8"


    // $ANTLR start "rule__Transition__Group__8__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2485:1: rule__Transition__Group__8__Impl : ( RULE_LESS ) ;
    public final void rule__Transition__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2489:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2490:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2490:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2491:1: RULE_LESS
            {
             before(grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_8()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__Transition__Group__8__Impl4912); 
             after(grammarAccess.getTransitionAccess().getLESSTerminalRuleCall_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__8__Impl"


    // $ANTLR start "rule__Transition__Group__9"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2502:1: rule__Transition__Group__9 : rule__Transition__Group__9__Impl rule__Transition__Group__10 ;
    public final void rule__Transition__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2506:1: ( rule__Transition__Group__9__Impl rule__Transition__Group__10 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2507:2: rule__Transition__Group__9__Impl rule__Transition__Group__10
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__9__Impl_in_rule__Transition__Group__94941);
            rule__Transition__Group__9__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__10_in_rule__Transition__Group__94944);
            rule__Transition__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__9"


    // $ANTLR start "rule__Transition__Group__9__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2514:1: rule__Transition__Group__9__Impl : ( RULE_SLASH ) ;
    public final void rule__Transition__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2518:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2519:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2519:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2520:1: RULE_SLASH
            {
             before(grammarAccess.getTransitionAccess().getSLASHTerminalRuleCall_9()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__Transition__Group__9__Impl4971); 
             after(grammarAccess.getTransitionAccess().getSLASHTerminalRuleCall_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__9__Impl"


    // $ANTLR start "rule__Transition__Group__10"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2531:1: rule__Transition__Group__10 : rule__Transition__Group__10__Impl rule__Transition__Group__11 ;
    public final void rule__Transition__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2535:1: ( rule__Transition__Group__10__Impl rule__Transition__Group__11 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2536:2: rule__Transition__Group__10__Impl rule__Transition__Group__11
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__10__Impl_in_rule__Transition__Group__105000);
            rule__Transition__Group__10__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__11_in_rule__Transition__Group__105003);
            rule__Transition__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__10"


    // $ANTLR start "rule__Transition__Group__10__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2543:1: rule__Transition__Group__10__Impl : ( RULE_TTRANSITION ) ;
    public final void rule__Transition__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2547:1: ( ( RULE_TTRANSITION ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2548:1: ( RULE_TTRANSITION )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2548:1: ( RULE_TTRANSITION )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2549:1: RULE_TTRANSITION
            {
             before(grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_10()); 
            match(input,RULE_TTRANSITION,FollowSets000.FOLLOW_RULE_TTRANSITION_in_rule__Transition__Group__10__Impl5030); 
             after(grammarAccess.getTransitionAccess().getTTRANSITIONTerminalRuleCall_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__10__Impl"


    // $ANTLR start "rule__Transition__Group__11"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2560:1: rule__Transition__Group__11 : rule__Transition__Group__11__Impl ;
    public final void rule__Transition__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2564:1: ( rule__Transition__Group__11__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2565:2: rule__Transition__Group__11__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group__11__Impl_in_rule__Transition__Group__115059);
            rule__Transition__Group__11__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__11"


    // $ANTLR start "rule__Transition__Group__11__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2571:1: rule__Transition__Group__11__Impl : ( RULE_GREATER ) ;
    public final void rule__Transition__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2575:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2576:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2576:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2577:1: RULE_GREATER
            {
             before(grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_11()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__Transition__Group__11__Impl5086); 
             after(grammarAccess.getTransitionAccess().getGREATERTerminalRuleCall_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__11__Impl"


    // $ANTLR start "rule__Transition__Group_5__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2612:1: rule__Transition__Group_5__0 : rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1 ;
    public final void rule__Transition__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2616:1: ( rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2617:2: rule__Transition__Group_5__0__Impl rule__Transition__Group_5__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__0__Impl_in_rule__Transition__Group_5__05139);
            rule__Transition__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__1_in_rule__Transition__Group_5__05142);
            rule__Transition__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__0"


    // $ANTLR start "rule__Transition__Group_5__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2624:1: rule__Transition__Group_5__0__Impl : ( RULE_MAXDELAY ) ;
    public final void rule__Transition__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2628:1: ( ( RULE_MAXDELAY ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2629:1: ( RULE_MAXDELAY )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2629:1: ( RULE_MAXDELAY )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2630:1: RULE_MAXDELAY
            {
             before(grammarAccess.getTransitionAccess().getMAXDELAYTerminalRuleCall_5_0()); 
            match(input,RULE_MAXDELAY,FollowSets000.FOLLOW_RULE_MAXDELAY_in_rule__Transition__Group_5__0__Impl5169); 
             after(grammarAccess.getTransitionAccess().getMAXDELAYTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__0__Impl"


    // $ANTLR start "rule__Transition__Group_5__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2641:1: rule__Transition__Group_5__1 : rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2 ;
    public final void rule__Transition__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2645:1: ( rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2646:2: rule__Transition__Group_5__1__Impl rule__Transition__Group_5__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__1__Impl_in_rule__Transition__Group_5__15198);
            rule__Transition__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__2_in_rule__Transition__Group_5__15201);
            rule__Transition__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__1"


    // $ANTLR start "rule__Transition__Group_5__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2653:1: rule__Transition__Group_5__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__Transition__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2657:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2658:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2658:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2659:1: RULE_EQUALS
            {
             before(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_5_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__Transition__Group_5__1__Impl5228); 
             after(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__1__Impl"


    // $ANTLR start "rule__Transition__Group_5__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2670:1: rule__Transition__Group_5__2 : rule__Transition__Group_5__2__Impl ;
    public final void rule__Transition__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2674:1: ( rule__Transition__Group_5__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2675:2: rule__Transition__Group_5__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_5__2__Impl_in_rule__Transition__Group_5__25257);
            rule__Transition__Group_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__2"


    // $ANTLR start "rule__Transition__Group_5__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2681:1: rule__Transition__Group_5__2__Impl : ( ( rule__Transition__MaxDelayAssignment_5_2 ) ) ;
    public final void rule__Transition__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2685:1: ( ( ( rule__Transition__MaxDelayAssignment_5_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2686:1: ( ( rule__Transition__MaxDelayAssignment_5_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2686:1: ( ( rule__Transition__MaxDelayAssignment_5_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2687:1: ( rule__Transition__MaxDelayAssignment_5_2 )
            {
             before(grammarAccess.getTransitionAccess().getMaxDelayAssignment_5_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2688:1: ( rule__Transition__MaxDelayAssignment_5_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2688:2: rule__Transition__MaxDelayAssignment_5_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__MaxDelayAssignment_5_2_in_rule__Transition__Group_5__2__Impl5284);
            rule__Transition__MaxDelayAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getMaxDelayAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_5__2__Impl"


    // $ANTLR start "rule__Transition__Group_6__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2704:1: rule__Transition__Group_6__0 : rule__Transition__Group_6__0__Impl rule__Transition__Group_6__1 ;
    public final void rule__Transition__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2708:1: ( rule__Transition__Group_6__0__Impl rule__Transition__Group_6__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2709:2: rule__Transition__Group_6__0__Impl rule__Transition__Group_6__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__0__Impl_in_rule__Transition__Group_6__05320);
            rule__Transition__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__1_in_rule__Transition__Group_6__05323);
            rule__Transition__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__0"


    // $ANTLR start "rule__Transition__Group_6__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2716:1: rule__Transition__Group_6__0__Impl : ( RULE_MINDELAY ) ;
    public final void rule__Transition__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2720:1: ( ( RULE_MINDELAY ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2721:1: ( RULE_MINDELAY )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2721:1: ( RULE_MINDELAY )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2722:1: RULE_MINDELAY
            {
             before(grammarAccess.getTransitionAccess().getMINDELAYTerminalRuleCall_6_0()); 
            match(input,RULE_MINDELAY,FollowSets000.FOLLOW_RULE_MINDELAY_in_rule__Transition__Group_6__0__Impl5350); 
             after(grammarAccess.getTransitionAccess().getMINDELAYTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__0__Impl"


    // $ANTLR start "rule__Transition__Group_6__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2733:1: rule__Transition__Group_6__1 : rule__Transition__Group_6__1__Impl rule__Transition__Group_6__2 ;
    public final void rule__Transition__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2737:1: ( rule__Transition__Group_6__1__Impl rule__Transition__Group_6__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2738:2: rule__Transition__Group_6__1__Impl rule__Transition__Group_6__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__1__Impl_in_rule__Transition__Group_6__15379);
            rule__Transition__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__2_in_rule__Transition__Group_6__15382);
            rule__Transition__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__1"


    // $ANTLR start "rule__Transition__Group_6__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2745:1: rule__Transition__Group_6__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__Transition__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2749:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2750:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2750:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2751:1: RULE_EQUALS
            {
             before(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_6_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__Transition__Group_6__1__Impl5409); 
             after(grammarAccess.getTransitionAccess().getEQUALSTerminalRuleCall_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__1__Impl"


    // $ANTLR start "rule__Transition__Group_6__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2762:1: rule__Transition__Group_6__2 : rule__Transition__Group_6__2__Impl ;
    public final void rule__Transition__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2766:1: ( rule__Transition__Group_6__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2767:2: rule__Transition__Group_6__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__Group_6__2__Impl_in_rule__Transition__Group_6__25438);
            rule__Transition__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__2"


    // $ANTLR start "rule__Transition__Group_6__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2773:1: rule__Transition__Group_6__2__Impl : ( ( rule__Transition__MinDelayAssignment_6_2 ) ) ;
    public final void rule__Transition__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2777:1: ( ( ( rule__Transition__MinDelayAssignment_6_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2778:1: ( ( rule__Transition__MinDelayAssignment_6_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2778:1: ( ( rule__Transition__MinDelayAssignment_6_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2779:1: ( rule__Transition__MinDelayAssignment_6_2 )
            {
             before(grammarAccess.getTransitionAccess().getMinDelayAssignment_6_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2780:1: ( rule__Transition__MinDelayAssignment_6_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2780:2: rule__Transition__MinDelayAssignment_6_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Transition__MinDelayAssignment_6_2_in_rule__Transition__Group_6__2__Impl5465);
            rule__Transition__MinDelayAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getMinDelayAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group_6__2__Impl"


    // $ANTLR start "rule__OutputArc__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2796:1: rule__OutputArc__Group__0 : rule__OutputArc__Group__0__Impl rule__OutputArc__Group__1 ;
    public final void rule__OutputArc__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2800:1: ( rule__OutputArc__Group__0__Impl rule__OutputArc__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2801:2: rule__OutputArc__Group__0__Impl rule__OutputArc__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__0__Impl_in_rule__OutputArc__Group__05501);
            rule__OutputArc__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__1_in_rule__OutputArc__Group__05504);
            rule__OutputArc__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__0"


    // $ANTLR start "rule__OutputArc__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2808:1: rule__OutputArc__Group__0__Impl : ( RULE_LESS ) ;
    public final void rule__OutputArc__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2812:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2813:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2813:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2814:1: RULE_LESS
            {
             before(grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_0()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__OutputArc__Group__0__Impl5531); 
             after(grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__0__Impl"


    // $ANTLR start "rule__OutputArc__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2825:1: rule__OutputArc__Group__1 : rule__OutputArc__Group__1__Impl rule__OutputArc__Group__2 ;
    public final void rule__OutputArc__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2829:1: ( rule__OutputArc__Group__1__Impl rule__OutputArc__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2830:2: rule__OutputArc__Group__1__Impl rule__OutputArc__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__1__Impl_in_rule__OutputArc__Group__15560);
            rule__OutputArc__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__2_in_rule__OutputArc__Group__15563);
            rule__OutputArc__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__1"


    // $ANTLR start "rule__OutputArc__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2837:1: rule__OutputArc__Group__1__Impl : ( RULE_TOUTPUTARC ) ;
    public final void rule__OutputArc__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2841:1: ( ( RULE_TOUTPUTARC ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2842:1: ( RULE_TOUTPUTARC )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2842:1: ( RULE_TOUTPUTARC )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2843:1: RULE_TOUTPUTARC
            {
             before(grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_1()); 
            match(input,RULE_TOUTPUTARC,FollowSets000.FOLLOW_RULE_TOUTPUTARC_in_rule__OutputArc__Group__1__Impl5590); 
             after(grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__1__Impl"


    // $ANTLR start "rule__OutputArc__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2854:1: rule__OutputArc__Group__2 : rule__OutputArc__Group__2__Impl rule__OutputArc__Group__3 ;
    public final void rule__OutputArc__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2858:1: ( rule__OutputArc__Group__2__Impl rule__OutputArc__Group__3 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2859:2: rule__OutputArc__Group__2__Impl rule__OutputArc__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__2__Impl_in_rule__OutputArc__Group__25619);
            rule__OutputArc__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__3_in_rule__OutputArc__Group__25622);
            rule__OutputArc__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__2"


    // $ANTLR start "rule__OutputArc__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2866:1: rule__OutputArc__Group__2__Impl : ( ( rule__OutputArc__UnorderedGroup_2 ) ) ;
    public final void rule__OutputArc__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2870:1: ( ( ( rule__OutputArc__UnorderedGroup_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2871:1: ( ( rule__OutputArc__UnorderedGroup_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2871:1: ( ( rule__OutputArc__UnorderedGroup_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2872:1: ( rule__OutputArc__UnorderedGroup_2 )
            {
             before(grammarAccess.getOutputArcAccess().getUnorderedGroup_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2873:1: ( rule__OutputArc__UnorderedGroup_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2873:2: rule__OutputArc__UnorderedGroup_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2_in_rule__OutputArc__Group__2__Impl5649);
            rule__OutputArc__UnorderedGroup_2();

            state._fsp--;


            }

             after(grammarAccess.getOutputArcAccess().getUnorderedGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__2__Impl"


    // $ANTLR start "rule__OutputArc__Group__3"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2883:1: rule__OutputArc__Group__3 : rule__OutputArc__Group__3__Impl rule__OutputArc__Group__4 ;
    public final void rule__OutputArc__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2887:1: ( rule__OutputArc__Group__3__Impl rule__OutputArc__Group__4 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2888:2: rule__OutputArc__Group__3__Impl rule__OutputArc__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__3__Impl_in_rule__OutputArc__Group__35679);
            rule__OutputArc__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__4_in_rule__OutputArc__Group__35682);
            rule__OutputArc__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__3"


    // $ANTLR start "rule__OutputArc__Group__3__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2895:1: rule__OutputArc__Group__3__Impl : ( RULE_GREATER ) ;
    public final void rule__OutputArc__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2899:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2900:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2900:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2901:1: RULE_GREATER
            {
             before(grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_3()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__OutputArc__Group__3__Impl5709); 
             after(grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__3__Impl"


    // $ANTLR start "rule__OutputArc__Group__4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2912:1: rule__OutputArc__Group__4 : rule__OutputArc__Group__4__Impl rule__OutputArc__Group__5 ;
    public final void rule__OutputArc__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2916:1: ( rule__OutputArc__Group__4__Impl rule__OutputArc__Group__5 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2917:2: rule__OutputArc__Group__4__Impl rule__OutputArc__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__4__Impl_in_rule__OutputArc__Group__45738);
            rule__OutputArc__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__5_in_rule__OutputArc__Group__45741);
            rule__OutputArc__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__4"


    // $ANTLR start "rule__OutputArc__Group__4__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2924:1: rule__OutputArc__Group__4__Impl : ( RULE_LESS ) ;
    public final void rule__OutputArc__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2928:1: ( ( RULE_LESS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2929:1: ( RULE_LESS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2929:1: ( RULE_LESS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2930:1: RULE_LESS
            {
             before(grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_4()); 
            match(input,RULE_LESS,FollowSets000.FOLLOW_RULE_LESS_in_rule__OutputArc__Group__4__Impl5768); 
             after(grammarAccess.getOutputArcAccess().getLESSTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__4__Impl"


    // $ANTLR start "rule__OutputArc__Group__5"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2941:1: rule__OutputArc__Group__5 : rule__OutputArc__Group__5__Impl rule__OutputArc__Group__6 ;
    public final void rule__OutputArc__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2945:1: ( rule__OutputArc__Group__5__Impl rule__OutputArc__Group__6 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2946:2: rule__OutputArc__Group__5__Impl rule__OutputArc__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__5__Impl_in_rule__OutputArc__Group__55797);
            rule__OutputArc__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__6_in_rule__OutputArc__Group__55800);
            rule__OutputArc__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__5"


    // $ANTLR start "rule__OutputArc__Group__5__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2953:1: rule__OutputArc__Group__5__Impl : ( RULE_SLASH ) ;
    public final void rule__OutputArc__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2957:1: ( ( RULE_SLASH ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2958:1: ( RULE_SLASH )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2958:1: ( RULE_SLASH )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2959:1: RULE_SLASH
            {
             before(grammarAccess.getOutputArcAccess().getSLASHTerminalRuleCall_5()); 
            match(input,RULE_SLASH,FollowSets000.FOLLOW_RULE_SLASH_in_rule__OutputArc__Group__5__Impl5827); 
             after(grammarAccess.getOutputArcAccess().getSLASHTerminalRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__5__Impl"


    // $ANTLR start "rule__OutputArc__Group__6"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2970:1: rule__OutputArc__Group__6 : rule__OutputArc__Group__6__Impl rule__OutputArc__Group__7 ;
    public final void rule__OutputArc__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2974:1: ( rule__OutputArc__Group__6__Impl rule__OutputArc__Group__7 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2975:2: rule__OutputArc__Group__6__Impl rule__OutputArc__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__6__Impl_in_rule__OutputArc__Group__65856);
            rule__OutputArc__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__7_in_rule__OutputArc__Group__65859);
            rule__OutputArc__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__6"


    // $ANTLR start "rule__OutputArc__Group__6__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2982:1: rule__OutputArc__Group__6__Impl : ( RULE_TOUTPUTARC ) ;
    public final void rule__OutputArc__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2986:1: ( ( RULE_TOUTPUTARC ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2987:1: ( RULE_TOUTPUTARC )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2987:1: ( RULE_TOUTPUTARC )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2988:1: RULE_TOUTPUTARC
            {
             before(grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_6()); 
            match(input,RULE_TOUTPUTARC,FollowSets000.FOLLOW_RULE_TOUTPUTARC_in_rule__OutputArc__Group__6__Impl5886); 
             after(grammarAccess.getOutputArcAccess().getTOUTPUTARCTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__6__Impl"


    // $ANTLR start "rule__OutputArc__Group__7"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:2999:1: rule__OutputArc__Group__7 : rule__OutputArc__Group__7__Impl ;
    public final void rule__OutputArc__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3003:1: ( rule__OutputArc__Group__7__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3004:2: rule__OutputArc__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group__7__Impl_in_rule__OutputArc__Group__75915);
            rule__OutputArc__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__7"


    // $ANTLR start "rule__OutputArc__Group__7__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3010:1: rule__OutputArc__Group__7__Impl : ( RULE_GREATER ) ;
    public final void rule__OutputArc__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3014:1: ( ( RULE_GREATER ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3015:1: ( RULE_GREATER )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3015:1: ( RULE_GREATER )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3016:1: RULE_GREATER
            {
             before(grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_7()); 
            match(input,RULE_GREATER,FollowSets000.FOLLOW_RULE_GREATER_in_rule__OutputArc__Group__7__Impl5942); 
             after(grammarAccess.getOutputArcAccess().getGREATERTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group__7__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_0__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3043:1: rule__OutputArc__Group_2_0__0 : rule__OutputArc__Group_2_0__0__Impl rule__OutputArc__Group_2_0__1 ;
    public final void rule__OutputArc__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3047:1: ( rule__OutputArc__Group_2_0__0__Impl rule__OutputArc__Group_2_0__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3048:2: rule__OutputArc__Group_2_0__0__Impl rule__OutputArc__Group_2_0__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__0__Impl_in_rule__OutputArc__Group_2_0__05987);
            rule__OutputArc__Group_2_0__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__1_in_rule__OutputArc__Group_2_0__05990);
            rule__OutputArc__Group_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__0"


    // $ANTLR start "rule__OutputArc__Group_2_0__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3055:1: rule__OutputArc__Group_2_0__0__Impl : ( RULE_ID ) ;
    public final void rule__OutputArc__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3059:1: ( ( RULE_ID ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3060:1: ( RULE_ID )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3060:1: ( RULE_ID )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3061:1: RULE_ID
            {
             before(grammarAccess.getOutputArcAccess().getIDTerminalRuleCall_2_0_0()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__OutputArc__Group_2_0__0__Impl6017); 
             after(grammarAccess.getOutputArcAccess().getIDTerminalRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__0__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_0__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3072:1: rule__OutputArc__Group_2_0__1 : rule__OutputArc__Group_2_0__1__Impl rule__OutputArc__Group_2_0__2 ;
    public final void rule__OutputArc__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3076:1: ( rule__OutputArc__Group_2_0__1__Impl rule__OutputArc__Group_2_0__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3077:2: rule__OutputArc__Group_2_0__1__Impl rule__OutputArc__Group_2_0__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__1__Impl_in_rule__OutputArc__Group_2_0__16046);
            rule__OutputArc__Group_2_0__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__2_in_rule__OutputArc__Group_2_0__16049);
            rule__OutputArc__Group_2_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__1"


    // $ANTLR start "rule__OutputArc__Group_2_0__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3084:1: rule__OutputArc__Group_2_0__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__OutputArc__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3088:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3089:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3089:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3090:1: RULE_EQUALS
            {
             before(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_0__1__Impl6076); 
             after(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__1__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_0__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3101:1: rule__OutputArc__Group_2_0__2 : rule__OutputArc__Group_2_0__2__Impl ;
    public final void rule__OutputArc__Group_2_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3105:1: ( rule__OutputArc__Group_2_0__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3106:2: rule__OutputArc__Group_2_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__2__Impl_in_rule__OutputArc__Group_2_0__26105);
            rule__OutputArc__Group_2_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__2"


    // $ANTLR start "rule__OutputArc__Group_2_0__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3112:1: rule__OutputArc__Group_2_0__2__Impl : ( ( rule__OutputArc__NameAssignment_2_0_2 ) ) ;
    public final void rule__OutputArc__Group_2_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3116:1: ( ( ( rule__OutputArc__NameAssignment_2_0_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3117:1: ( ( rule__OutputArc__NameAssignment_2_0_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3117:1: ( ( rule__OutputArc__NameAssignment_2_0_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3118:1: ( rule__OutputArc__NameAssignment_2_0_2 )
            {
             before(grammarAccess.getOutputArcAccess().getNameAssignment_2_0_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3119:1: ( rule__OutputArc__NameAssignment_2_0_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3119:2: rule__OutputArc__NameAssignment_2_0_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__NameAssignment_2_0_2_in_rule__OutputArc__Group_2_0__2__Impl6132);
            rule__OutputArc__NameAssignment_2_0_2();

            state._fsp--;


            }

             after(grammarAccess.getOutputArcAccess().getNameAssignment_2_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_0__2__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_1__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3135:1: rule__OutputArc__Group_2_1__0 : rule__OutputArc__Group_2_1__0__Impl rule__OutputArc__Group_2_1__1 ;
    public final void rule__OutputArc__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3139:1: ( rule__OutputArc__Group_2_1__0__Impl rule__OutputArc__Group_2_1__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3140:2: rule__OutputArc__Group_2_1__0__Impl rule__OutputArc__Group_2_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__0__Impl_in_rule__OutputArc__Group_2_1__06168);
            rule__OutputArc__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__1_in_rule__OutputArc__Group_2_1__06171);
            rule__OutputArc__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__0"


    // $ANTLR start "rule__OutputArc__Group_2_1__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3147:1: rule__OutputArc__Group_2_1__0__Impl : ( RULE_SOURCE ) ;
    public final void rule__OutputArc__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3151:1: ( ( RULE_SOURCE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3152:1: ( RULE_SOURCE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3152:1: ( RULE_SOURCE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3153:1: RULE_SOURCE
            {
             before(grammarAccess.getOutputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 
            match(input,RULE_SOURCE,FollowSets000.FOLLOW_RULE_SOURCE_in_rule__OutputArc__Group_2_1__0__Impl6198); 
             after(grammarAccess.getOutputArcAccess().getSOURCETerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__0__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_1__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3164:1: rule__OutputArc__Group_2_1__1 : rule__OutputArc__Group_2_1__1__Impl rule__OutputArc__Group_2_1__2 ;
    public final void rule__OutputArc__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3168:1: ( rule__OutputArc__Group_2_1__1__Impl rule__OutputArc__Group_2_1__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3169:2: rule__OutputArc__Group_2_1__1__Impl rule__OutputArc__Group_2_1__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__1__Impl_in_rule__OutputArc__Group_2_1__16227);
            rule__OutputArc__Group_2_1__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__2_in_rule__OutputArc__Group_2_1__16230);
            rule__OutputArc__Group_2_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__1"


    // $ANTLR start "rule__OutputArc__Group_2_1__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3176:1: rule__OutputArc__Group_2_1__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__OutputArc__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3180:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3181:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3181:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3182:1: RULE_EQUALS
            {
             before(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_1__1__Impl6257); 
             after(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__1__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_1__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3193:1: rule__OutputArc__Group_2_1__2 : rule__OutputArc__Group_2_1__2__Impl ;
    public final void rule__OutputArc__Group_2_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3197:1: ( rule__OutputArc__Group_2_1__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3198:2: rule__OutputArc__Group_2_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__2__Impl_in_rule__OutputArc__Group_2_1__26286);
            rule__OutputArc__Group_2_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__2"


    // $ANTLR start "rule__OutputArc__Group_2_1__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3204:1: rule__OutputArc__Group_2_1__2__Impl : ( ( rule__OutputArc__FromAssignment_2_1_2 ) ) ;
    public final void rule__OutputArc__Group_2_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3208:1: ( ( ( rule__OutputArc__FromAssignment_2_1_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3209:1: ( ( rule__OutputArc__FromAssignment_2_1_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3209:1: ( ( rule__OutputArc__FromAssignment_2_1_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3210:1: ( rule__OutputArc__FromAssignment_2_1_2 )
            {
             before(grammarAccess.getOutputArcAccess().getFromAssignment_2_1_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3211:1: ( rule__OutputArc__FromAssignment_2_1_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3211:2: rule__OutputArc__FromAssignment_2_1_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__FromAssignment_2_1_2_in_rule__OutputArc__Group_2_1__2__Impl6313);
            rule__OutputArc__FromAssignment_2_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOutputArcAccess().getFromAssignment_2_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_1__2__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_2__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3227:1: rule__OutputArc__Group_2_2__0 : rule__OutputArc__Group_2_2__0__Impl rule__OutputArc__Group_2_2__1 ;
    public final void rule__OutputArc__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3231:1: ( rule__OutputArc__Group_2_2__0__Impl rule__OutputArc__Group_2_2__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3232:2: rule__OutputArc__Group_2_2__0__Impl rule__OutputArc__Group_2_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__0__Impl_in_rule__OutputArc__Group_2_2__06349);
            rule__OutputArc__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__1_in_rule__OutputArc__Group_2_2__06352);
            rule__OutputArc__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__0"


    // $ANTLR start "rule__OutputArc__Group_2_2__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3239:1: rule__OutputArc__Group_2_2__0__Impl : ( RULE_TARGET ) ;
    public final void rule__OutputArc__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3243:1: ( ( RULE_TARGET ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3244:1: ( RULE_TARGET )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3244:1: ( RULE_TARGET )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3245:1: RULE_TARGET
            {
             before(grammarAccess.getOutputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 
            match(input,RULE_TARGET,FollowSets000.FOLLOW_RULE_TARGET_in_rule__OutputArc__Group_2_2__0__Impl6379); 
             after(grammarAccess.getOutputArcAccess().getTARGETTerminalRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__0__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_2__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3256:1: rule__OutputArc__Group_2_2__1 : rule__OutputArc__Group_2_2__1__Impl rule__OutputArc__Group_2_2__2 ;
    public final void rule__OutputArc__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3260:1: ( rule__OutputArc__Group_2_2__1__Impl rule__OutputArc__Group_2_2__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3261:2: rule__OutputArc__Group_2_2__1__Impl rule__OutputArc__Group_2_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__1__Impl_in_rule__OutputArc__Group_2_2__16408);
            rule__OutputArc__Group_2_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__2_in_rule__OutputArc__Group_2_2__16411);
            rule__OutputArc__Group_2_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__1"


    // $ANTLR start "rule__OutputArc__Group_2_2__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3268:1: rule__OutputArc__Group_2_2__1__Impl : ( RULE_EQUALS ) ;
    public final void rule__OutputArc__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3272:1: ( ( RULE_EQUALS ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3273:1: ( RULE_EQUALS )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3273:1: ( RULE_EQUALS )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3274:1: RULE_EQUALS
            {
             before(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 
            match(input,RULE_EQUALS,FollowSets000.FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_2__1__Impl6438); 
             after(grammarAccess.getOutputArcAccess().getEQUALSTerminalRuleCall_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__1__Impl"


    // $ANTLR start "rule__OutputArc__Group_2_2__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3285:1: rule__OutputArc__Group_2_2__2 : rule__OutputArc__Group_2_2__2__Impl ;
    public final void rule__OutputArc__Group_2_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3289:1: ( rule__OutputArc__Group_2_2__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3290:2: rule__OutputArc__Group_2_2__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__2__Impl_in_rule__OutputArc__Group_2_2__26467);
            rule__OutputArc__Group_2_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__2"


    // $ANTLR start "rule__OutputArc__Group_2_2__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3296:1: rule__OutputArc__Group_2_2__2__Impl : ( ( rule__OutputArc__ToAssignment_2_2_2 ) ) ;
    public final void rule__OutputArc__Group_2_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3300:1: ( ( ( rule__OutputArc__ToAssignment_2_2_2 ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3301:1: ( ( rule__OutputArc__ToAssignment_2_2_2 ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3301:1: ( ( rule__OutputArc__ToAssignment_2_2_2 ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3302:1: ( rule__OutputArc__ToAssignment_2_2_2 )
            {
             before(grammarAccess.getOutputArcAccess().getToAssignment_2_2_2()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3303:1: ( rule__OutputArc__ToAssignment_2_2_2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3303:2: rule__OutputArc__ToAssignment_2_2_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__ToAssignment_2_2_2_in_rule__OutputArc__Group_2_2__2__Impl6494);
            rule__OutputArc__ToAssignment_2_2_2();

            state._fsp--;


            }

             after(grammarAccess.getOutputArcAccess().getToAssignment_2_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__Group_2_2__2__Impl"


    // $ANTLR start "rule__DOUBLE__Group__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3319:1: rule__DOUBLE__Group__0 : rule__DOUBLE__Group__0__Impl rule__DOUBLE__Group__1 ;
    public final void rule__DOUBLE__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3323:1: ( rule__DOUBLE__Group__0__Impl rule__DOUBLE__Group__1 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3324:2: rule__DOUBLE__Group__0__Impl rule__DOUBLE__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__0__Impl_in_rule__DOUBLE__Group__06530);
            rule__DOUBLE__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__1_in_rule__DOUBLE__Group__06533);
            rule__DOUBLE__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__0"


    // $ANTLR start "rule__DOUBLE__Group__0__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3331:1: rule__DOUBLE__Group__0__Impl : ( RULE_INT ) ;
    public final void rule__DOUBLE__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3335:1: ( ( RULE_INT ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3336:1: ( RULE_INT )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3336:1: ( RULE_INT )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3337:1: RULE_INT
            {
             before(grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__DOUBLE__Group__0__Impl6560); 
             after(grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__0__Impl"


    // $ANTLR start "rule__DOUBLE__Group__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3348:1: rule__DOUBLE__Group__1 : rule__DOUBLE__Group__1__Impl rule__DOUBLE__Group__2 ;
    public final void rule__DOUBLE__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3352:1: ( rule__DOUBLE__Group__1__Impl rule__DOUBLE__Group__2 )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3353:2: rule__DOUBLE__Group__1__Impl rule__DOUBLE__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__1__Impl_in_rule__DOUBLE__Group__16589);
            rule__DOUBLE__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__2_in_rule__DOUBLE__Group__16592);
            rule__DOUBLE__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__1"


    // $ANTLR start "rule__DOUBLE__Group__1__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3360:1: rule__DOUBLE__Group__1__Impl : ( '.' ) ;
    public final void rule__DOUBLE__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3364:1: ( ( '.' ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3365:1: ( '.' )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3365:1: ( '.' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3366:1: '.'
            {
             before(grammarAccess.getDOUBLEAccess().getFullStopKeyword_1()); 
            match(input,31,FollowSets000.FOLLOW_31_in_rule__DOUBLE__Group__1__Impl6620); 
             after(grammarAccess.getDOUBLEAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__1__Impl"


    // $ANTLR start "rule__DOUBLE__Group__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3379:1: rule__DOUBLE__Group__2 : rule__DOUBLE__Group__2__Impl ;
    public final void rule__DOUBLE__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3383:1: ( rule__DOUBLE__Group__2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3384:2: rule__DOUBLE__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__DOUBLE__Group__2__Impl_in_rule__DOUBLE__Group__26651);
            rule__DOUBLE__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__2"


    // $ANTLR start "rule__DOUBLE__Group__2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3390:1: rule__DOUBLE__Group__2__Impl : ( RULE_INT ) ;
    public final void rule__DOUBLE__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3394:1: ( ( RULE_INT ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3395:1: ( RULE_INT )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3395:1: ( RULE_INT )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3396:1: RULE_INT
            {
             before(grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_2()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__DOUBLE__Group__2__Impl6678); 
             after(grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOUBLE__Group__2__Impl"


    // $ANTLR start "rule__InputArc__UnorderedGroup_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3414:1: rule__InputArc__UnorderedGroup_2 : rule__InputArc__UnorderedGroup_2__0 {...}?;
    public final void rule__InputArc__UnorderedGroup_2() throws RecognitionException {

            	int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3419:1: ( rule__InputArc__UnorderedGroup_2__0 {...}?)
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3420:2: rule__InputArc__UnorderedGroup_2__0 {...}?
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__0_in_rule__InputArc__UnorderedGroup_26714);
            rule__InputArc__UnorderedGroup_2__0();

            state._fsp--;

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getInputArcAccess().getUnorderedGroup_2()) ) {
                throw new FailedPredicateException(input, "rule__InputArc__UnorderedGroup_2", "getUnorderedGroupHelper().canLeave(grammarAccess.getInputArcAccess().getUnorderedGroup_2())");
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__UnorderedGroup_2"


    // $ANTLR start "rule__InputArc__UnorderedGroup_2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3431:1: rule__InputArc__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) ) ) ;
    public final void rule__InputArc__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3436:1: ( ( ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3437:3: ( ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3437:3: ( ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) ) )
            int alt5=3;
            int LA5_0 = input.LA(1);

            if ( LA5_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt5=1;
            }
            else if ( LA5_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt5=2;
            }
            else if ( LA5_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt5=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3439:4: ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3439:4: ({...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3440:5: {...}? => ( ( ( rule__InputArc__Group_2_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
                        throw new FailedPredicateException(input, "rule__InputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3440:105: ( ( ( rule__InputArc__Group_2_0__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3441:6: ( ( rule__InputArc__Group_2_0__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3447:6: ( ( rule__InputArc__Group_2_0__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3449:7: ( rule__InputArc__Group_2_0__0 )
                    {
                     before(grammarAccess.getInputArcAccess().getGroup_2_0()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3450:7: ( rule__InputArc__Group_2_0__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3450:8: rule__InputArc__Group_2_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_0__0_in_rule__InputArc__UnorderedGroup_2__Impl6803);
                    rule__InputArc__Group_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInputArcAccess().getGroup_2_0()); 

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3456:4: ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3456:4: ({...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3457:5: {...}? => ( ( ( rule__InputArc__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
                        throw new FailedPredicateException(input, "rule__InputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3457:105: ( ( ( rule__InputArc__Group_2_1__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3458:6: ( ( rule__InputArc__Group_2_1__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3464:6: ( ( rule__InputArc__Group_2_1__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3466:7: ( rule__InputArc__Group_2_1__0 )
                    {
                     before(grammarAccess.getInputArcAccess().getGroup_2_1()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3467:7: ( rule__InputArc__Group_2_1__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3467:8: rule__InputArc__Group_2_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_1__0_in_rule__InputArc__UnorderedGroup_2__Impl6894);
                    rule__InputArc__Group_2_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInputArcAccess().getGroup_2_1()); 

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3473:4: ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3473:4: ({...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3474:5: {...}? => ( ( ( rule__InputArc__Group_2_2__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
                        throw new FailedPredicateException(input, "rule__InputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3474:105: ( ( ( rule__InputArc__Group_2_2__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3475:6: ( ( rule__InputArc__Group_2_2__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3481:6: ( ( rule__InputArc__Group_2_2__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3483:7: ( rule__InputArc__Group_2_2__0 )
                    {
                     before(grammarAccess.getInputArcAccess().getGroup_2_2()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3484:7: ( rule__InputArc__Group_2_2__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3484:8: rule__InputArc__Group_2_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__InputArc__Group_2_2__0_in_rule__InputArc__UnorderedGroup_2__Impl6985);
                    rule__InputArc__Group_2_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInputArcAccess().getGroup_2_2()); 

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInputArcAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__InputArc__UnorderedGroup_2__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3499:1: rule__InputArc__UnorderedGroup_2__0 : rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__1 )? ;
    public final void rule__InputArc__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3503:1: ( rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__1 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3504:2: rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__1 )?
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__07044);
            rule__InputArc__UnorderedGroup_2__Impl();

            state._fsp--;

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3505:2: ( rule__InputArc__UnorderedGroup_2__1 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( LA6_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt6=1;
            }
            else if ( LA6_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt6=1;
            }
            else if ( LA6_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3505:2: rule__InputArc__UnorderedGroup_2__1
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__1_in_rule__InputArc__UnorderedGroup_2__07047);
                    rule__InputArc__UnorderedGroup_2__1();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__UnorderedGroup_2__0"


    // $ANTLR start "rule__InputArc__UnorderedGroup_2__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3512:1: rule__InputArc__UnorderedGroup_2__1 : rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__2 )? ;
    public final void rule__InputArc__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3516:1: ( rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__2 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3517:2: rule__InputArc__UnorderedGroup_2__Impl ( rule__InputArc__UnorderedGroup_2__2 )?
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__17072);
            rule__InputArc__UnorderedGroup_2__Impl();

            state._fsp--;

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3518:2: ( rule__InputArc__UnorderedGroup_2__2 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( LA7_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt7=1;
            }
            else if ( LA7_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt7=1;
            }
            else if ( LA7_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getInputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3518:2: rule__InputArc__UnorderedGroup_2__2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__2_in_rule__InputArc__UnorderedGroup_2__17075);
                    rule__InputArc__UnorderedGroup_2__2();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__UnorderedGroup_2__1"


    // $ANTLR start "rule__InputArc__UnorderedGroup_2__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3525:1: rule__InputArc__UnorderedGroup_2__2 : rule__InputArc__UnorderedGroup_2__Impl ;
    public final void rule__InputArc__UnorderedGroup_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3529:1: ( rule__InputArc__UnorderedGroup_2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3530:2: rule__InputArc__UnorderedGroup_2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__27100);
            rule__InputArc__UnorderedGroup_2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__UnorderedGroup_2__2"


    // $ANTLR start "rule__OutputArc__UnorderedGroup_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3543:1: rule__OutputArc__UnorderedGroup_2 : rule__OutputArc__UnorderedGroup_2__0 {...}?;
    public final void rule__OutputArc__UnorderedGroup_2() throws RecognitionException {

            	int stackSize = keepStackSize();
        		getUnorderedGroupHelper().enter(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3548:1: ( rule__OutputArc__UnorderedGroup_2__0 {...}?)
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3549:2: rule__OutputArc__UnorderedGroup_2__0 {...}?
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__0_in_rule__OutputArc__UnorderedGroup_27130);
            rule__OutputArc__UnorderedGroup_2__0();

            state._fsp--;

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2()) ) {
                throw new FailedPredicateException(input, "rule__OutputArc__UnorderedGroup_2", "getUnorderedGroupHelper().canLeave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2())");
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	getUnorderedGroupHelper().leave(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__UnorderedGroup_2"


    // $ANTLR start "rule__OutputArc__UnorderedGroup_2__Impl"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3560:1: rule__OutputArc__UnorderedGroup_2__Impl : ( ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) ) ) ;
    public final void rule__OutputArc__UnorderedGroup_2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        		boolean selected = false;
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3565:1: ( ( ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3566:3: ( ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3566:3: ( ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) ) | ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) ) )
            int alt8=3;
            int LA8_0 = input.LA(1);

            if ( LA8_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt8=1;
            }
            else if ( LA8_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt8=2;
            }
            else if ( LA8_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt8=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3568:4: ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3568:4: ({...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3569:5: {...}? => ( ( ( rule__OutputArc__Group_2_0__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
                        throw new FailedPredicateException(input, "rule__OutputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3569:106: ( ( ( rule__OutputArc__Group_2_0__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3570:6: ( ( rule__OutputArc__Group_2_0__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3576:6: ( ( rule__OutputArc__Group_2_0__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3578:7: ( rule__OutputArc__Group_2_0__0 )
                    {
                     before(grammarAccess.getOutputArcAccess().getGroup_2_0()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3579:7: ( rule__OutputArc__Group_2_0__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3579:8: rule__OutputArc__Group_2_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_0__0_in_rule__OutputArc__UnorderedGroup_2__Impl7219);
                    rule__OutputArc__Group_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOutputArcAccess().getGroup_2_0()); 

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3585:4: ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3585:4: ({...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3586:5: {...}? => ( ( ( rule__OutputArc__Group_2_1__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
                        throw new FailedPredicateException(input, "rule__OutputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3586:106: ( ( ( rule__OutputArc__Group_2_1__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3587:6: ( ( rule__OutputArc__Group_2_1__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3593:6: ( ( rule__OutputArc__Group_2_1__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3595:7: ( rule__OutputArc__Group_2_1__0 )
                    {
                     before(grammarAccess.getOutputArcAccess().getGroup_2_1()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3596:7: ( rule__OutputArc__Group_2_1__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3596:8: rule__OutputArc__Group_2_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_1__0_in_rule__OutputArc__UnorderedGroup_2__Impl7310);
                    rule__OutputArc__Group_2_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOutputArcAccess().getGroup_2_1()); 

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3602:4: ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) )
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3602:4: ({...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3603:5: {...}? => ( ( ( rule__OutputArc__Group_2_2__0 ) ) )
                    {
                    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
                        throw new FailedPredicateException(input, "rule__OutputArc__UnorderedGroup_2__Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2)");
                    }
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3603:106: ( ( ( rule__OutputArc__Group_2_2__0 ) ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3604:6: ( ( rule__OutputArc__Group_2_2__0 ) )
                    {
                     
                    	 				  getUnorderedGroupHelper().select(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2);
                    	 				

                    	 				  selected = true;
                    	 				
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3610:6: ( ( rule__OutputArc__Group_2_2__0 ) )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3612:7: ( rule__OutputArc__Group_2_2__0 )
                    {
                     before(grammarAccess.getOutputArcAccess().getGroup_2_2()); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3613:7: ( rule__OutputArc__Group_2_2__0 )
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3613:8: rule__OutputArc__Group_2_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__OutputArc__Group_2_2__0_in_rule__OutputArc__UnorderedGroup_2__Impl7401);
                    rule__OutputArc__Group_2_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getOutputArcAccess().getGroup_2_2()); 

                    }


                    }


                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	if (selected)
            		getUnorderedGroupHelper().returnFromSelection(grammarAccess.getOutputArcAccess().getUnorderedGroup_2());
            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__UnorderedGroup_2__Impl"


    // $ANTLR start "rule__OutputArc__UnorderedGroup_2__0"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3628:1: rule__OutputArc__UnorderedGroup_2__0 : rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__1 )? ;
    public final void rule__OutputArc__UnorderedGroup_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3632:1: ( rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__1 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3633:2: rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__1 )?
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__07460);
            rule__OutputArc__UnorderedGroup_2__Impl();

            state._fsp--;

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3634:2: ( rule__OutputArc__UnorderedGroup_2__1 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( LA9_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt9=1;
            }
            else if ( LA9_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt9=1;
            }
            else if ( LA9_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3634:2: rule__OutputArc__UnorderedGroup_2__1
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__1_in_rule__OutputArc__UnorderedGroup_2__07463);
                    rule__OutputArc__UnorderedGroup_2__1();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__UnorderedGroup_2__0"


    // $ANTLR start "rule__OutputArc__UnorderedGroup_2__1"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3641:1: rule__OutputArc__UnorderedGroup_2__1 : rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__2 )? ;
    public final void rule__OutputArc__UnorderedGroup_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3645:1: ( rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__2 )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3646:2: rule__OutputArc__UnorderedGroup_2__Impl ( rule__OutputArc__UnorderedGroup_2__2 )?
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__17488);
            rule__OutputArc__UnorderedGroup_2__Impl();

            state._fsp--;

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3647:2: ( rule__OutputArc__UnorderedGroup_2__2 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( LA10_0 ==RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 0) ) {
                alt10=1;
            }
            else if ( LA10_0 ==RULE_SOURCE && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 1) ) {
                alt10=1;
            }
            else if ( LA10_0 ==RULE_TARGET && getUnorderedGroupHelper().canSelect(grammarAccess.getOutputArcAccess().getUnorderedGroup_2(), 2) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3647:2: rule__OutputArc__UnorderedGroup_2__2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__2_in_rule__OutputArc__UnorderedGroup_2__17491);
                    rule__OutputArc__UnorderedGroup_2__2();

                    state._fsp--;


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__UnorderedGroup_2__1"


    // $ANTLR start "rule__OutputArc__UnorderedGroup_2__2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3654:1: rule__OutputArc__UnorderedGroup_2__2 : rule__OutputArc__UnorderedGroup_2__Impl ;
    public final void rule__OutputArc__UnorderedGroup_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3658:1: ( rule__OutputArc__UnorderedGroup_2__Impl )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3659:2: rule__OutputArc__UnorderedGroup_2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__27516);
            rule__OutputArc__UnorderedGroup_2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__UnorderedGroup_2__2"


    // $ANTLR start "rule__PetriNet__ElementsAssignment_25"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3672:1: rule__PetriNet__ElementsAssignment_25 : ( ruleElement ) ;
    public final void rule__PetriNet__ElementsAssignment_25() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3676:1: ( ( ruleElement ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3677:1: ( ruleElement )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3677:1: ( ruleElement )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3678:1: ruleElement
            {
             before(grammarAccess.getPetriNetAccess().getElementsElementParserRuleCall_25_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleElement_in_rule__PetriNet__ElementsAssignment_257550);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getPetriNetAccess().getElementsElementParserRuleCall_25_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PetriNet__ElementsAssignment_25"


    // $ANTLR start "rule__InputArc__NameAssignment_2_0_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3687:1: rule__InputArc__NameAssignment_2_0_2 : ( RULE_STRING ) ;
    public final void rule__InputArc__NameAssignment_2_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3691:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3692:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3692:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3693:1: RULE_STRING
            {
             before(grammarAccess.getInputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__InputArc__NameAssignment_2_0_27581); 
             after(grammarAccess.getInputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__NameAssignment_2_0_2"


    // $ANTLR start "rule__InputArc__FromAssignment_2_1_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3702:1: rule__InputArc__FromAssignment_2_1_2 : ( ( RULE_STRING ) ) ;
    public final void rule__InputArc__FromAssignment_2_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3706:1: ( ( ( RULE_STRING ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3707:1: ( ( RULE_STRING ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3707:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3708:1: ( RULE_STRING )
            {
             before(grammarAccess.getInputArcAccess().getFromPlaceCrossReference_2_1_2_0()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3709:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3710:1: RULE_STRING
            {
             before(grammarAccess.getInputArcAccess().getFromPlaceSTRINGTerminalRuleCall_2_1_2_0_1()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__InputArc__FromAssignment_2_1_27616); 
             after(grammarAccess.getInputArcAccess().getFromPlaceSTRINGTerminalRuleCall_2_1_2_0_1()); 

            }

             after(grammarAccess.getInputArcAccess().getFromPlaceCrossReference_2_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__FromAssignment_2_1_2"


    // $ANTLR start "rule__InputArc__ToAssignment_2_2_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3721:1: rule__InputArc__ToAssignment_2_2_2 : ( ( RULE_STRING ) ) ;
    public final void rule__InputArc__ToAssignment_2_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3725:1: ( ( ( RULE_STRING ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3726:1: ( ( RULE_STRING ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3726:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3727:1: ( RULE_STRING )
            {
             before(grammarAccess.getInputArcAccess().getToTransitionCrossReference_2_2_2_0()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3728:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3729:1: RULE_STRING
            {
             before(grammarAccess.getInputArcAccess().getToTransitionSTRINGTerminalRuleCall_2_2_2_0_1()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__InputArc__ToAssignment_2_2_27655); 
             after(grammarAccess.getInputArcAccess().getToTransitionSTRINGTerminalRuleCall_2_2_2_0_1()); 

            }

             after(grammarAccess.getInputArcAccess().getToTransitionCrossReference_2_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InputArc__ToAssignment_2_2_2"


    // $ANTLR start "rule__Place__NameAssignment_4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3740:1: rule__Place__NameAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Place__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3744:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3745:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3745:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3746:1: RULE_STRING
            {
             before(grammarAccess.getPlaceAccess().getNameSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__Place__NameAssignment_47690); 
             after(grammarAccess.getPlaceAccess().getNameSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Place__NameAssignment_4"


    // $ANTLR start "rule__Transition__NameAssignment_4"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3755:1: rule__Transition__NameAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Transition__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3759:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3760:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3760:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3761:1: RULE_STRING
            {
             before(grammarAccess.getTransitionAccess().getNameSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__Transition__NameAssignment_47721); 
             after(grammarAccess.getTransitionAccess().getNameSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__NameAssignment_4"


    // $ANTLR start "rule__Transition__MaxDelayAssignment_5_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3770:1: rule__Transition__MaxDelayAssignment_5_2 : ( ruleDOUBLE ) ;
    public final void rule__Transition__MaxDelayAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3774:1: ( ( ruleDOUBLE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3775:1: ( ruleDOUBLE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3775:1: ( ruleDOUBLE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3776:1: ruleDOUBLE
            {
             before(grammarAccess.getTransitionAccess().getMaxDelayDOUBLEParserRuleCall_5_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_rule__Transition__MaxDelayAssignment_5_27752);
            ruleDOUBLE();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getMaxDelayDOUBLEParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__MaxDelayAssignment_5_2"


    // $ANTLR start "rule__Transition__MinDelayAssignment_6_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3785:1: rule__Transition__MinDelayAssignment_6_2 : ( ruleDOUBLE ) ;
    public final void rule__Transition__MinDelayAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3789:1: ( ( ruleDOUBLE ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3790:1: ( ruleDOUBLE )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3790:1: ( ruleDOUBLE )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3791:1: ruleDOUBLE
            {
             before(grammarAccess.getTransitionAccess().getMinDelayDOUBLEParserRuleCall_6_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDOUBLE_in_rule__Transition__MinDelayAssignment_6_27783);
            ruleDOUBLE();

            state._fsp--;

             after(grammarAccess.getTransitionAccess().getMinDelayDOUBLEParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__MinDelayAssignment_6_2"


    // $ANTLR start "rule__OutputArc__NameAssignment_2_0_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3800:1: rule__OutputArc__NameAssignment_2_0_2 : ( RULE_STRING ) ;
    public final void rule__OutputArc__NameAssignment_2_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3804:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3805:1: ( RULE_STRING )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3805:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3806:1: RULE_STRING
            {
             before(grammarAccess.getOutputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__OutputArc__NameAssignment_2_0_27814); 
             after(grammarAccess.getOutputArcAccess().getNameSTRINGTerminalRuleCall_2_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__NameAssignment_2_0_2"


    // $ANTLR start "rule__OutputArc__FromAssignment_2_1_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3815:1: rule__OutputArc__FromAssignment_2_1_2 : ( ( RULE_STRING ) ) ;
    public final void rule__OutputArc__FromAssignment_2_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3819:1: ( ( ( RULE_STRING ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3820:1: ( ( RULE_STRING ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3820:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3821:1: ( RULE_STRING )
            {
             before(grammarAccess.getOutputArcAccess().getFromTransitionCrossReference_2_1_2_0()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3822:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3823:1: RULE_STRING
            {
             before(grammarAccess.getOutputArcAccess().getFromTransitionSTRINGTerminalRuleCall_2_1_2_0_1()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__OutputArc__FromAssignment_2_1_27849); 
             after(grammarAccess.getOutputArcAccess().getFromTransitionSTRINGTerminalRuleCall_2_1_2_0_1()); 

            }

             after(grammarAccess.getOutputArcAccess().getFromTransitionCrossReference_2_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__FromAssignment_2_1_2"


    // $ANTLR start "rule__OutputArc__ToAssignment_2_2_2"
    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3834:1: rule__OutputArc__ToAssignment_2_2_2 : ( ( RULE_STRING ) ) ;
    public final void rule__OutputArc__ToAssignment_2_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3838:1: ( ( ( RULE_STRING ) ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3839:1: ( ( RULE_STRING ) )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3839:1: ( ( RULE_STRING ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3840:1: ( RULE_STRING )
            {
             before(grammarAccess.getOutputArcAccess().getToPlaceCrossReference_2_2_2_0()); 
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3841:1: ( RULE_STRING )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3842:1: RULE_STRING
            {
             before(grammarAccess.getOutputArcAccess().getToPlaceSTRINGTerminalRuleCall_2_2_2_0_1()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__OutputArc__ToAssignment_2_2_27888); 
             after(grammarAccess.getOutputArcAccess().getToPlaceSTRINGTerminalRuleCall_2_2_2_0_1()); 

            }

             after(grammarAccess.getOutputArcAccess().getToPlaceCrossReference_2_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputArc__ToAssignment_2_2_2"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_rulePetriNet_in_entryRulePetriNet61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePetriNet68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__0_in_rulePetriNet94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElement_in_entryRuleElement121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleElement128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Element__Alternatives_in_ruleElement154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInputArc_in_entryRuleInputArc181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleInputArc188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__0_in_ruleInputArc214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePlace_in_entryRulePlace241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePlace248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__0_in_rulePlace274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition301 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTransition308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__0_in_ruleTransition334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOutputArc_in_entryRuleOutputArc361 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleOutputArc368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__0_in_ruleOutputArc394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_entryRuleDOUBLE421 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDOUBLE428 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__0_in_ruleDOUBLE454 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleOutputArc_in_rule__Element__Alternatives490 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleInputArc_in_rule__Element__Alternatives507 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTransition_in_rule__Element__Alternatives524 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePlace_in_rule__Element__Alternatives541 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__0__Impl_in_rule__PetriNet__Group__0571 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__1_in_rule__PetriNet__Group__0574 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__1__Impl_in_rule__PetriNet__Group__1632 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__2_in_rule__PetriNet__Group__1635 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__PetriNet__Group__1__Impl662 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__2__Impl_in_rule__PetriNet__Group__2691 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__3_in_rule__PetriNet__Group__2694 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_QUESTION_in_rule__PetriNet__Group__2__Impl721 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__3__Impl_in_rule__PetriNet__Group__3750 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__4_in_rule__PetriNet__Group__3753 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_XML_in_rule__PetriNet__Group__3__Impl780 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__4__Impl_in_rule__PetriNet__Group__4809 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__5_in_rule__PetriNet__Group__4812 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VERSION_in_rule__PetriNet__Group__4__Impl839 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__5__Impl_in_rule__PetriNet__Group__5868 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__6_in_rule__PetriNet__Group__5871 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__5__Impl898 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__6__Impl_in_rule__PetriNet__Group__6927 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__7_in_rule__PetriNet__Group__6930 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__PetriNet__Group__6__Impl957 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__7__Impl_in_rule__PetriNet__Group__7986 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__8_in_rule__PetriNet__Group__7989 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_QUESTION_in_rule__PetriNet__Group__7__Impl1016 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__8__Impl_in_rule__PetriNet__Group__81045 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__9_in_rule__PetriNet__Group__81048 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__8__Impl1075 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__9__Impl_in_rule__PetriNet__Group__91104 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__10_in_rule__PetriNet__Group__91107 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__PetriNet__Group__9__Impl1134 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__10__Impl_in_rule__PetriNet__Group__101163 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__11_in_rule__PetriNet__Group__101166 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_PNML_in_rule__PetriNet__Group__10__Impl1193 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__11__Impl_in_rule__PetriNet__Group__111222 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__12_in_rule__PetriNet__Group__111225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_COLON_in_rule__PetriNet__Group__11__Impl1252 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__12__Impl_in_rule__PetriNet__Group__121281 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__13_in_rule__PetriNet__Group__121284 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_PNML_in_rule__PetriNet__Group__12__Impl1311 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__13__Impl_in_rule__PetriNet__Group__131340 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__14_in_rule__PetriNet__Group__131343 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_XMLNS_in_rule__PetriNet__Group__13__Impl1370 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__14__Impl_in_rule__PetriNet__Group__141399 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__15_in_rule__PetriNet__Group__141402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_COLON_in_rule__PetriNet__Group__14__Impl1429 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__15__Impl_in_rule__PetriNet__Group__151458 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__16_in_rule__PetriNet__Group__151461 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_PNML_in_rule__PetriNet__Group__15__Impl1488 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__16__Impl_in_rule__PetriNet__Group__161517 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__17_in_rule__PetriNet__Group__161520 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__16__Impl1547 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__17__Impl_in_rule__PetriNet__Group__171576 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__18_in_rule__PetriNet__Group__171579 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__PetriNet__Group__17__Impl1606 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__18__Impl_in_rule__PetriNet__Group__181635 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__19_in_rule__PetriNet__Group__181638 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__18__Impl1665 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__19__Impl_in_rule__PetriNet__Group__191694 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__20_in_rule__PetriNet__Group__191697 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__PetriNet__Group__19__Impl1724 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__20__Impl_in_rule__PetriNet__Group__201753 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__21_in_rule__PetriNet__Group__201756 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_NET_in_rule__PetriNet__Group__20__Impl1783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__21__Impl_in_rule__PetriNet__Group__211812 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__22_in_rule__PetriNet__Group__211815 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TYPE_in_rule__PetriNet__Group__21__Impl1842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__22__Impl_in_rule__PetriNet__Group__221871 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__23_in_rule__PetriNet__Group__221874 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__PetriNet__Group__22__Impl1901 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__23__Impl_in_rule__PetriNet__Group__231930 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__24_in_rule__PetriNet__Group__231933 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__PetriNet__Group__23__Impl1960 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__24__Impl_in_rule__PetriNet__Group__241989 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__25_in_rule__PetriNet__Group__241992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__24__Impl2019 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__25__Impl_in_rule__PetriNet__Group__252048 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__26_in_rule__PetriNet__Group__252051 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__ElementsAssignment_25_in_rule__PetriNet__Group__25__Impl2078 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__26__Impl_in_rule__PetriNet__Group__262109 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__27_in_rule__PetriNet__Group__262112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__PetriNet__Group__26__Impl2139 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__27__Impl_in_rule__PetriNet__Group__272168 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__28_in_rule__PetriNet__Group__272171 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__PetriNet__Group__27__Impl2198 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__28__Impl_in_rule__PetriNet__Group__282227 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__29_in_rule__PetriNet__Group__282230 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_NET_in_rule__PetriNet__Group__28__Impl2257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__29__Impl_in_rule__PetriNet__Group__292286 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__30_in_rule__PetriNet__Group__292289 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__29__Impl2316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__30__Impl_in_rule__PetriNet__Group__302345 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__31_in_rule__PetriNet__Group__302348 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__PetriNet__Group__30__Impl2375 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__31__Impl_in_rule__PetriNet__Group__312404 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__32_in_rule__PetriNet__Group__312407 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__PetriNet__Group__31__Impl2434 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__32__Impl_in_rule__PetriNet__Group__322463 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__33_in_rule__PetriNet__Group__322466 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_PNML_in_rule__PetriNet__Group__32__Impl2493 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__33__Impl_in_rule__PetriNet__Group__332522 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__34_in_rule__PetriNet__Group__332525 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_COLON_in_rule__PetriNet__Group__33__Impl2552 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__34__Impl_in_rule__PetriNet__Group__342581 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__35_in_rule__PetriNet__Group__342584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_PNML_in_rule__PetriNet__Group__34__Impl2611 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PetriNet__Group__35__Impl_in_rule__PetriNet__Group__352640 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__PetriNet__Group__35__Impl2667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__0__Impl_in_rule__InputArc__Group__02768 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__InputArc__Group__1_in_rule__InputArc__Group__02771 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__InputArc__Group__0__Impl2798 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__1__Impl_in_rule__InputArc__Group__12827 = new BitSet(new long[]{0x00000000001C0000L});
        public static final BitSet FOLLOW_rule__InputArc__Group__2_in_rule__InputArc__Group__12830 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TINPUTARC_in_rule__InputArc__Group__1__Impl2857 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__2__Impl_in_rule__InputArc__Group__22886 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__InputArc__Group__3_in_rule__InputArc__Group__22889 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2_in_rule__InputArc__Group__2__Impl2916 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__3__Impl_in_rule__InputArc__Group__32946 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__InputArc__Group__4_in_rule__InputArc__Group__32949 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__InputArc__Group__3__Impl2976 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__4__Impl_in_rule__InputArc__Group__43005 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__InputArc__Group__5_in_rule__InputArc__Group__43008 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__InputArc__Group__4__Impl3035 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__5__Impl_in_rule__InputArc__Group__53064 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__InputArc__Group__6_in_rule__InputArc__Group__53067 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__InputArc__Group__5__Impl3094 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__6__Impl_in_rule__InputArc__Group__63123 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__InputArc__Group__7_in_rule__InputArc__Group__63126 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TINPUTARC_in_rule__InputArc__Group__6__Impl3153 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group__7__Impl_in_rule__InputArc__Group__73182 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__InputArc__Group__7__Impl3209 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__0__Impl_in_rule__InputArc__Group_2_0__03254 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__1_in_rule__InputArc__Group_2_0__03257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__InputArc__Group_2_0__0__Impl3284 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__1__Impl_in_rule__InputArc__Group_2_0__13313 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__2_in_rule__InputArc__Group_2_0__13316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_0__1__Impl3343 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__2__Impl_in_rule__InputArc__Group_2_0__23372 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__NameAssignment_2_0_2_in_rule__InputArc__Group_2_0__2__Impl3399 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__0__Impl_in_rule__InputArc__Group_2_1__03435 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__1_in_rule__InputArc__Group_2_1__03438 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SOURCE_in_rule__InputArc__Group_2_1__0__Impl3465 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__1__Impl_in_rule__InputArc__Group_2_1__13494 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__2_in_rule__InputArc__Group_2_1__13497 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_1__1__Impl3524 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__2__Impl_in_rule__InputArc__Group_2_1__23553 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__FromAssignment_2_1_2_in_rule__InputArc__Group_2_1__2__Impl3580 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__0__Impl_in_rule__InputArc__Group_2_2__03616 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__1_in_rule__InputArc__Group_2_2__03619 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TARGET_in_rule__InputArc__Group_2_2__0__Impl3646 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__1__Impl_in_rule__InputArc__Group_2_2__13675 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__2_in_rule__InputArc__Group_2_2__13678 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__InputArc__Group_2_2__1__Impl3705 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__2__Impl_in_rule__InputArc__Group_2_2__23734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__ToAssignment_2_2_2_in_rule__InputArc__Group_2_2__2__Impl3761 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__0__Impl_in_rule__Place__Group__03797 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Place__Group__1_in_rule__Place__Group__03800 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__Place__Group__0__Impl3827 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__1__Impl_in_rule__Place__Group__13856 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__Place__Group__2_in_rule__Place__Group__13859 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TPLACE_in_rule__Place__Group__1__Impl3886 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__2__Impl_in_rule__Place__Group__23915 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__Place__Group__3_in_rule__Place__Group__23918 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Place__Group__2__Impl3945 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__3__Impl_in_rule__Place__Group__33974 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__Place__Group__4_in_rule__Place__Group__33977 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__Place__Group__3__Impl4004 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__4__Impl_in_rule__Place__Group__44033 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__Place__Group__5_in_rule__Place__Group__44036 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__NameAssignment_4_in_rule__Place__Group__4__Impl4063 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__5__Impl_in_rule__Place__Group__54093 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Place__Group__6_in_rule__Place__Group__54096 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__Place__Group__5__Impl4123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__6__Impl_in_rule__Place__Group__64152 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__Place__Group__7_in_rule__Place__Group__64155 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__Place__Group__6__Impl4182 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__7__Impl_in_rule__Place__Group__74211 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Place__Group__8_in_rule__Place__Group__74214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__Place__Group__7__Impl4241 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__8__Impl_in_rule__Place__Group__84270 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__Place__Group__9_in_rule__Place__Group__84273 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TPLACE_in_rule__Place__Group__8__Impl4300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Place__Group__9__Impl_in_rule__Place__Group__94329 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__Place__Group__9__Impl4356 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__04405 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__04408 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__Transition__Group__0__Impl4435 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__14464 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__14467 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TTRANSITION_in_rule__Transition__Group__1__Impl4494 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__24523 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__24526 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Transition__Group__2__Impl4553 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__34582 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__34585 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__Transition__Group__3__Impl4612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__44641 = new BitSet(new long[]{0x0000000001800400L});
        public static final BitSet FOLLOW_rule__Transition__Group__5_in_rule__Transition__Group__44644 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__NameAssignment_4_in_rule__Transition__Group__4__Impl4671 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__5__Impl_in_rule__Transition__Group__54701 = new BitSet(new long[]{0x0000000001800400L});
        public static final BitSet FOLLOW_rule__Transition__Group__6_in_rule__Transition__Group__54704 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__0_in_rule__Transition__Group__5__Impl4731 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__6__Impl_in_rule__Transition__Group__64762 = new BitSet(new long[]{0x0000000001800400L});
        public static final BitSet FOLLOW_rule__Transition__Group__7_in_rule__Transition__Group__64765 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__0_in_rule__Transition__Group__6__Impl4792 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__7__Impl_in_rule__Transition__Group__74823 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Transition__Group__8_in_rule__Transition__Group__74826 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__Transition__Group__7__Impl4853 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__8__Impl_in_rule__Transition__Group__84882 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__Transition__Group__9_in_rule__Transition__Group__84885 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__Transition__Group__8__Impl4912 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__9__Impl_in_rule__Transition__Group__94941 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Transition__Group__10_in_rule__Transition__Group__94944 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__Transition__Group__9__Impl4971 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__10__Impl_in_rule__Transition__Group__105000 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__Transition__Group__11_in_rule__Transition__Group__105003 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TTRANSITION_in_rule__Transition__Group__10__Impl5030 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group__11__Impl_in_rule__Transition__Group__115059 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__Transition__Group__11__Impl5086 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__0__Impl_in_rule__Transition__Group_5__05139 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__1_in_rule__Transition__Group_5__05142 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_MAXDELAY_in_rule__Transition__Group_5__0__Impl5169 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__1__Impl_in_rule__Transition__Group_5__15198 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__2_in_rule__Transition__Group_5__15201 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__Transition__Group_5__1__Impl5228 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_5__2__Impl_in_rule__Transition__Group_5__25257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__MaxDelayAssignment_5_2_in_rule__Transition__Group_5__2__Impl5284 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__0__Impl_in_rule__Transition__Group_6__05320 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__1_in_rule__Transition__Group_6__05323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_MINDELAY_in_rule__Transition__Group_6__0__Impl5350 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__1__Impl_in_rule__Transition__Group_6__15379 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__2_in_rule__Transition__Group_6__15382 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__Transition__Group_6__1__Impl5409 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__Group_6__2__Impl_in_rule__Transition__Group_6__25438 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Transition__MinDelayAssignment_6_2_in_rule__Transition__Group_6__2__Impl5465 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__0__Impl_in_rule__OutputArc__Group__05501 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__1_in_rule__OutputArc__Group__05504 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__OutputArc__Group__0__Impl5531 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__1__Impl_in_rule__OutputArc__Group__15560 = new BitSet(new long[]{0x00000000001C0000L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__2_in_rule__OutputArc__Group__15563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TOUTPUTARC_in_rule__OutputArc__Group__1__Impl5590 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__2__Impl_in_rule__OutputArc__Group__25619 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__3_in_rule__OutputArc__Group__25622 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2_in_rule__OutputArc__Group__2__Impl5649 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__3__Impl_in_rule__OutputArc__Group__35679 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__4_in_rule__OutputArc__Group__35682 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__OutputArc__Group__3__Impl5709 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__4__Impl_in_rule__OutputArc__Group__45738 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__5_in_rule__OutputArc__Group__45741 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_LESS_in_rule__OutputArc__Group__4__Impl5768 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__5__Impl_in_rule__OutputArc__Group__55797 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__6_in_rule__OutputArc__Group__55800 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SLASH_in_rule__OutputArc__Group__5__Impl5827 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__6__Impl_in_rule__OutputArc__Group__65856 = new BitSet(new long[]{0x0000000000000400L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__7_in_rule__OutputArc__Group__65859 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TOUTPUTARC_in_rule__OutputArc__Group__6__Impl5886 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group__7__Impl_in_rule__OutputArc__Group__75915 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_GREATER_in_rule__OutputArc__Group__7__Impl5942 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__0__Impl_in_rule__OutputArc__Group_2_0__05987 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__1_in_rule__OutputArc__Group_2_0__05990 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__OutputArc__Group_2_0__0__Impl6017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__1__Impl_in_rule__OutputArc__Group_2_0__16046 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__2_in_rule__OutputArc__Group_2_0__16049 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_0__1__Impl6076 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__2__Impl_in_rule__OutputArc__Group_2_0__26105 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__NameAssignment_2_0_2_in_rule__OutputArc__Group_2_0__2__Impl6132 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__0__Impl_in_rule__OutputArc__Group_2_1__06168 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__1_in_rule__OutputArc__Group_2_1__06171 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SOURCE_in_rule__OutputArc__Group_2_1__0__Impl6198 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__1__Impl_in_rule__OutputArc__Group_2_1__16227 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__2_in_rule__OutputArc__Group_2_1__16230 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_1__1__Impl6257 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__2__Impl_in_rule__OutputArc__Group_2_1__26286 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__FromAssignment_2_1_2_in_rule__OutputArc__Group_2_1__2__Impl6313 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__0__Impl_in_rule__OutputArc__Group_2_2__06349 = new BitSet(new long[]{0x0000000000000100L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__1_in_rule__OutputArc__Group_2_2__06352 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_TARGET_in_rule__OutputArc__Group_2_2__0__Impl6379 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__1__Impl_in_rule__OutputArc__Group_2_2__16408 = new BitSet(new long[]{0x0000000000000200L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__2_in_rule__OutputArc__Group_2_2__16411 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_EQUALS_in_rule__OutputArc__Group_2_2__1__Impl6438 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__2__Impl_in_rule__OutputArc__Group_2_2__26467 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__ToAssignment_2_2_2_in_rule__OutputArc__Group_2_2__2__Impl6494 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__0__Impl_in_rule__DOUBLE__Group__06530 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__1_in_rule__DOUBLE__Group__06533 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__DOUBLE__Group__0__Impl6560 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__1__Impl_in_rule__DOUBLE__Group__16589 = new BitSet(new long[]{0x0000000004000000L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__2_in_rule__DOUBLE__Group__16592 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_rule__DOUBLE__Group__1__Impl6620 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DOUBLE__Group__2__Impl_in_rule__DOUBLE__Group__26651 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__DOUBLE__Group__2__Impl6678 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__0_in_rule__InputArc__UnorderedGroup_26714 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_0__0_in_rule__InputArc__UnorderedGroup_2__Impl6803 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_1__0_in_rule__InputArc__UnorderedGroup_2__Impl6894 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__Group_2_2__0_in_rule__InputArc__UnorderedGroup_2__Impl6985 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__07044 = new BitSet(new long[]{0x00000000001C0002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__1_in_rule__InputArc__UnorderedGroup_2__07047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__17072 = new BitSet(new long[]{0x00000000001C0002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__2_in_rule__InputArc__UnorderedGroup_2__17075 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__InputArc__UnorderedGroup_2__Impl_in_rule__InputArc__UnorderedGroup_2__27100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__0_in_rule__OutputArc__UnorderedGroup_27130 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_0__0_in_rule__OutputArc__UnorderedGroup_2__Impl7219 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_1__0_in_rule__OutputArc__UnorderedGroup_2__Impl7310 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__Group_2_2__0_in_rule__OutputArc__UnorderedGroup_2__Impl7401 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__07460 = new BitSet(new long[]{0x00000000001C0002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__1_in_rule__OutputArc__UnorderedGroup_2__07463 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__17488 = new BitSet(new long[]{0x00000000001C0002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__2_in_rule__OutputArc__UnorderedGroup_2__17491 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__OutputArc__UnorderedGroup_2__Impl_in_rule__OutputArc__UnorderedGroup_2__27516 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElement_in_rule__PetriNet__ElementsAssignment_257550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__InputArc__NameAssignment_2_0_27581 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__InputArc__FromAssignment_2_1_27616 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__InputArc__ToAssignment_2_2_27655 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__Place__NameAssignment_47690 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__Transition__NameAssignment_47721 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_rule__Transition__MaxDelayAssignment_5_27752 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDOUBLE_in_rule__Transition__MinDelayAssignment_6_27783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__OutputArc__NameAssignment_2_0_27814 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__OutputArc__FromAssignment_2_1_27849 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__OutputArc__ToAssignment_2_2_27888 = new BitSet(new long[]{0x0000000000000002L});
    }


}