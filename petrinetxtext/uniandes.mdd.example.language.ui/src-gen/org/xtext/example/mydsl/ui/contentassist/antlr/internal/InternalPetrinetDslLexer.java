package org.xtext.example.mydsl.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPetrinetDslLexer extends Lexer {
    public static final int RULE_MAXDELAY=23;
    public static final int RULE_NET=14;
    public static final int RULE_TPLACE=21;
    public static final int RULE_XMLNS=13;
    public static final int RULE_SOURCE=19;
    public static final int RULE_STRING=9;
    public static final int RULE_XML=6;
    public static final int RULE_GREATER=10;
    public static final int RULE_TINPUTARC=17;
    public static final int RULE_SL_COMMENT=28;
    public static final int RULE_TOUTPUTARC=25;
    public static final int RULE_VERSION=7;
    public static final int RULE_EQUALS=8;
    public static final int RULE_TARGET=20;
    public static final int RULE_LESS=4;
    public static final int EOF=-1;
    public static final int T__31=31;
    public static final int RULE_ID=18;
    public static final int RULE_WS=29;
    public static final int RULE_PNML=11;
    public static final int RULE_COLON=12;
    public static final int RULE_ANY_OTHER=30;
    public static final int RULE_SLASH=16;
    public static final int RULE_TYPE=15;
    public static final int RULE_INT=26;
    public static final int RULE_MINDELAY=24;
    public static final int RULE_QUESTION=5;
    public static final int RULE_ML_COMMENT=27;
    public static final int RULE_TTRANSITION=22;

    // delegates
    // delegators

    public InternalPetrinetDslLexer() {;} 
    public InternalPetrinetDslLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalPetrinetDslLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g"; }

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:11:7: ( '.' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:11:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "RULE_XMLNS"
    public final void mRULE_XMLNS() throws RecognitionException {
        try {
            int _type = RULE_XMLNS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3854:12: ( 'xmlns' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3854:14: 'xmlns'
            {
            match("xmlns"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XMLNS"

    // $ANTLR start "RULE_COLON"
    public final void mRULE_COLON() throws RecognitionException {
        try {
            int _type = RULE_COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3856:12: ( ':' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3856:14: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLON"

    // $ANTLR start "RULE_PNML"
    public final void mRULE_PNML() throws RecognitionException {
        try {
            int _type = RULE_PNML;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3858:11: ( 'pnml' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3858:13: 'pnml'
            {
            match("pnml"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PNML"

    // $ANTLR start "RULE_VERSION"
    public final void mRULE_VERSION() throws RecognitionException {
        try {
            int _type = RULE_VERSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3860:14: ( 'version' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3860:16: 'version'
            {
            match("version"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VERSION"

    // $ANTLR start "RULE_XML"
    public final void mRULE_XML() throws RecognitionException {
        try {
            int _type = RULE_XML;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3862:10: ( 'xml' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3862:12: 'xml'
            {
            match("xml"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_XML"

    // $ANTLR start "RULE_MAXDELAY"
    public final void mRULE_MAXDELAY() throws RecognitionException {
        try {
            int _type = RULE_MAXDELAY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3864:15: ( 'maxDelay' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3864:17: 'maxDelay'
            {
            match("maxDelay"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MAXDELAY"

    // $ANTLR start "RULE_MINDELAY"
    public final void mRULE_MINDELAY() throws RecognitionException {
        try {
            int _type = RULE_MINDELAY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3866:15: ( 'minDelay' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3866:17: 'minDelay'
            {
            match("minDelay"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MINDELAY"

    // $ANTLR start "RULE_TYPE"
    public final void mRULE_TYPE() throws RecognitionException {
        try {
            int _type = RULE_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3868:11: ( 'type' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3868:13: 'type'
            {
            match("type"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TYPE"

    // $ANTLR start "RULE_NET"
    public final void mRULE_NET() throws RecognitionException {
        try {
            int _type = RULE_NET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3870:10: ( 'net' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3870:12: 'net'
            {
            match("net"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NET"

    // $ANTLR start "RULE_TARGET"
    public final void mRULE_TARGET() throws RecognitionException {
        try {
            int _type = RULE_TARGET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3872:13: ( 'target' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3872:15: 'target'
            {
            match("target"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TARGET"

    // $ANTLR start "RULE_SOURCE"
    public final void mRULE_SOURCE() throws RecognitionException {
        try {
            int _type = RULE_SOURCE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3874:13: ( 'source' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3874:15: 'source'
            {
            match("source"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SOURCE"

    // $ANTLR start "RULE_GREATER"
    public final void mRULE_GREATER() throws RecognitionException {
        try {
            int _type = RULE_GREATER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3876:14: ( '>' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3876:16: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_GREATER"

    // $ANTLR start "RULE_LESS"
    public final void mRULE_LESS() throws RecognitionException {
        try {
            int _type = RULE_LESS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3878:11: ( '<' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3878:13: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LESS"

    // $ANTLR start "RULE_SLASH"
    public final void mRULE_SLASH() throws RecognitionException {
        try {
            int _type = RULE_SLASH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3880:12: ( '/' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3880:14: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SLASH"

    // $ANTLR start "RULE_TPLACE"
    public final void mRULE_TPLACE() throws RecognitionException {
        try {
            int _type = RULE_TPLACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3882:13: ( 'place' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3882:15: 'place'
            {
            match("place"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TPLACE"

    // $ANTLR start "RULE_TTRANSITION"
    public final void mRULE_TTRANSITION() throws RecognitionException {
        try {
            int _type = RULE_TTRANSITION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3884:18: ( 'transition' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3884:20: 'transition'
            {
            match("transition"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TTRANSITION"

    // $ANTLR start "RULE_TOUTPUTARC"
    public final void mRULE_TOUTPUTARC() throws RecognitionException {
        try {
            int _type = RULE_TOUTPUTARC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3886:17: ( 'outputarc' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3886:19: 'outputarc'
            {
            match("outputarc"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TOUTPUTARC"

    // $ANTLR start "RULE_TINPUTARC"
    public final void mRULE_TINPUTARC() throws RecognitionException {
        try {
            int _type = RULE_TINPUTARC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3888:16: ( 'inputarc' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3888:18: 'inputarc'
            {
            match("inputarc"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TINPUTARC"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3890:9: ( 'id' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3890:11: 'id'
            {
            match("id"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_EQUALS"
    public final void mRULE_EQUALS() throws RecognitionException {
        try {
            int _type = RULE_EQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3892:13: ( '=' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3892:15: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EQUALS"

    // $ANTLR start "RULE_QUESTION"
    public final void mRULE_QUESTION() throws RecognitionException {
        try {
            int _type = RULE_QUESTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3894:15: ( '?' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3894:17: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_QUESTION"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3896:10: ( ( '0' .. '9' )+ )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3896:12: ( '0' .. '9' )+
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3896:12: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3896:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\"') ) {
                alt4=1;
            }
            else if ( (LA4_0=='\'') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop2:
                    do {
                        int alt2=3;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0=='\\') ) {
                            alt2=1;
                        }
                        else if ( ((LA2_0>='\u0000' && LA2_0<='!')||(LA2_0>='#' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='\uFFFF')) ) {
                            alt2=2;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop3:
                    do {
                        int alt3=3;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0=='\\') ) {
                            alt3=1;
                        }
                        else if ( ((LA3_0>='\u0000' && LA3_0<='&')||(LA3_0>='(' && LA3_0<='[')||(LA3_0>=']' && LA3_0<='\uFFFF')) ) {
                            alt3=2;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3898:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3900:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3900:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3900:24: ( options {greedy=false; } : . )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='*') ) {
                    int LA5_1 = input.LA(2);

                    if ( (LA5_1=='/') ) {
                        alt5=2;
                    }
                    else if ( ((LA5_1>='\u0000' && LA5_1<='.')||(LA5_1>='0' && LA5_1<='\uFFFF')) ) {
                        alt5=1;
                    }


                }
                else if ( ((LA5_0>='\u0000' && LA5_0<=')')||(LA5_0>='+' && LA5_0<='\uFFFF')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3900:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='\u0000' && LA6_0<='\t')||(LA6_0>='\u000B' && LA6_0<='\f')||(LA6_0>='\u000E' && LA6_0<='\uFFFF')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:40: ( ( '\\r' )? '\\n' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\n'||LA8_0=='\r') ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:41: ( '\\r' )? '\\n'
                    {
                    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:41: ( '\\r' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='\r') ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3902:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3904:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3904:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3904:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='\t' && LA9_0<='\n')||LA9_0=='\r'||LA9_0==' ') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3906:16: ( . )
            // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:3906:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:8: ( T__31 | RULE_XMLNS | RULE_COLON | RULE_PNML | RULE_VERSION | RULE_XML | RULE_MAXDELAY | RULE_MINDELAY | RULE_TYPE | RULE_NET | RULE_TARGET | RULE_SOURCE | RULE_GREATER | RULE_LESS | RULE_SLASH | RULE_TPLACE | RULE_TTRANSITION | RULE_TOUTPUTARC | RULE_TINPUTARC | RULE_ID | RULE_EQUALS | RULE_QUESTION | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt10=28;
        alt10 = dfa10.predict(input);
        switch (alt10) {
            case 1 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:10: T__31
                {
                mT__31(); 

                }
                break;
            case 2 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:16: RULE_XMLNS
                {
                mRULE_XMLNS(); 

                }
                break;
            case 3 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:27: RULE_COLON
                {
                mRULE_COLON(); 

                }
                break;
            case 4 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:38: RULE_PNML
                {
                mRULE_PNML(); 

                }
                break;
            case 5 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:48: RULE_VERSION
                {
                mRULE_VERSION(); 

                }
                break;
            case 6 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:61: RULE_XML
                {
                mRULE_XML(); 

                }
                break;
            case 7 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:70: RULE_MAXDELAY
                {
                mRULE_MAXDELAY(); 

                }
                break;
            case 8 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:84: RULE_MINDELAY
                {
                mRULE_MINDELAY(); 

                }
                break;
            case 9 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:98: RULE_TYPE
                {
                mRULE_TYPE(); 

                }
                break;
            case 10 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:108: RULE_NET
                {
                mRULE_NET(); 

                }
                break;
            case 11 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:117: RULE_TARGET
                {
                mRULE_TARGET(); 

                }
                break;
            case 12 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:129: RULE_SOURCE
                {
                mRULE_SOURCE(); 

                }
                break;
            case 13 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:141: RULE_GREATER
                {
                mRULE_GREATER(); 

                }
                break;
            case 14 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:154: RULE_LESS
                {
                mRULE_LESS(); 

                }
                break;
            case 15 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:164: RULE_SLASH
                {
                mRULE_SLASH(); 

                }
                break;
            case 16 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:175: RULE_TPLACE
                {
                mRULE_TPLACE(); 

                }
                break;
            case 17 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:187: RULE_TTRANSITION
                {
                mRULE_TTRANSITION(); 

                }
                break;
            case 18 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:204: RULE_TOUTPUTARC
                {
                mRULE_TOUTPUTARC(); 

                }
                break;
            case 19 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:220: RULE_TINPUTARC
                {
                mRULE_TINPUTARC(); 

                }
                break;
            case 20 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:235: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 21 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:243: RULE_EQUALS
                {
                mRULE_EQUALS(); 

                }
                break;
            case 22 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:255: RULE_QUESTION
                {
                mRULE_QUESTION(); 

                }
                break;
            case 23 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:269: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 24 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:278: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 25 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:290: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 26 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:306: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 27 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:322: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 28 :
                // ../uniandes.mdd.example.language.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalPetrinetDsl.g:1:330: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA10 dfa10 = new DFA10(this);
    static final String DFA10_eotS =
        "\2\uffff\1\25\1\uffff\6\25\2\uffff\1\47\2\25\3\uffff\2\25\34\uffff"+
        "\1\62\2\uffff";
    static final String DFA10_eofS =
        "\63\uffff";
    static final String DFA10_minS =
        "\1\0\1\uffff\1\155\1\uffff\1\154\1\145\2\141\1\145\1\157\2\uffff"+
        "\1\52\1\165\1\144\3\uffff\2\0\3\uffff\1\154\30\uffff\1\156\2\uffff";
    static final String DFA10_maxS =
        "\1\uffff\1\uffff\1\155\1\uffff\1\156\1\145\1\151\1\171\1\145\1"+
        "\157\2\uffff\1\57\1\165\1\156\3\uffff\2\uffff\3\uffff\1\154\30\uffff"+
        "\1\156\2\uffff";
    static final String DFA10_acceptS =
        "\1\uffff\1\1\1\uffff\1\3\6\uffff\1\15\1\16\3\uffff\1\25\1\26\1"+
        "\27\2\uffff\1\33\1\34\1\1\1\uffff\1\3\1\4\1\20\1\5\1\7\1\10\1\11"+
        "\1\13\1\21\1\12\1\14\1\15\1\16\1\31\1\32\1\17\1\22\1\23\1\24\1\25"+
        "\1\26\1\27\1\30\1\33\1\uffff\1\2\1\6";
    static final String DFA10_specialS =
        "\1\0\21\uffff\1\1\1\2\37\uffff}>";
    static final String[] DFA10_transitionS = {
            "\11\25\2\24\2\25\1\24\22\25\1\24\1\25\1\22\4\25\1\23\6\25\1"+
            "\1\1\14\12\21\1\3\1\25\1\13\1\17\1\12\1\20\51\25\1\16\3\25\1"+
            "\6\1\10\1\15\1\4\2\25\1\11\1\7\1\25\1\5\1\25\1\2\uff87\25",
            "",
            "\1\27",
            "",
            "\1\32\1\uffff\1\31",
            "\1\33",
            "\1\34\7\uffff\1\35",
            "\1\37\20\uffff\1\40\6\uffff\1\36",
            "\1\41",
            "\1\42",
            "",
            "",
            "\1\45\4\uffff\1\46",
            "\1\50",
            "\1\52\11\uffff\1\51",
            "",
            "",
            "",
            "\0\56",
            "\0\56",
            "",
            "",
            "",
            "\1\60",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\61",
            "",
            ""
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    static class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__31 | RULE_XMLNS | RULE_COLON | RULE_PNML | RULE_VERSION | RULE_XML | RULE_MAXDELAY | RULE_MINDELAY | RULE_TYPE | RULE_NET | RULE_TARGET | RULE_SOURCE | RULE_GREATER | RULE_LESS | RULE_SLASH | RULE_TPLACE | RULE_TTRANSITION | RULE_TOUTPUTARC | RULE_TINPUTARC | RULE_ID | RULE_EQUALS | RULE_QUESTION | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA10_0 = input.LA(1);

                        s = -1;
                        if ( (LA10_0=='.') ) {s = 1;}

                        else if ( (LA10_0=='x') ) {s = 2;}

                        else if ( (LA10_0==':') ) {s = 3;}

                        else if ( (LA10_0=='p') ) {s = 4;}

                        else if ( (LA10_0=='v') ) {s = 5;}

                        else if ( (LA10_0=='m') ) {s = 6;}

                        else if ( (LA10_0=='t') ) {s = 7;}

                        else if ( (LA10_0=='n') ) {s = 8;}

                        else if ( (LA10_0=='s') ) {s = 9;}

                        else if ( (LA10_0=='>') ) {s = 10;}

                        else if ( (LA10_0=='<') ) {s = 11;}

                        else if ( (LA10_0=='/') ) {s = 12;}

                        else if ( (LA10_0=='o') ) {s = 13;}

                        else if ( (LA10_0=='i') ) {s = 14;}

                        else if ( (LA10_0=='=') ) {s = 15;}

                        else if ( (LA10_0=='?') ) {s = 16;}

                        else if ( ((LA10_0>='0' && LA10_0<='9')) ) {s = 17;}

                        else if ( (LA10_0=='\"') ) {s = 18;}

                        else if ( (LA10_0=='\'') ) {s = 19;}

                        else if ( ((LA10_0>='\t' && LA10_0<='\n')||LA10_0=='\r'||LA10_0==' ') ) {s = 20;}

                        else if ( ((LA10_0>='\u0000' && LA10_0<='\b')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\u001F')||LA10_0=='!'||(LA10_0>='#' && LA10_0<='&')||(LA10_0>='(' && LA10_0<='-')||LA10_0==';'||(LA10_0>='@' && LA10_0<='h')||(LA10_0>='j' && LA10_0<='l')||(LA10_0>='q' && LA10_0<='r')||LA10_0=='u'||LA10_0=='w'||(LA10_0>='y' && LA10_0<='\uFFFF')) ) {s = 21;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA10_18 = input.LA(1);

                        s = -1;
                        if ( ((LA10_18>='\u0000' && LA10_18<='\uFFFF')) ) {s = 46;}

                        else s = 21;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA10_19 = input.LA(1);

                        s = -1;
                        if ( ((LA10_19>='\u0000' && LA10_19<='\uFFFF')) ) {s = 46;}

                        else s = 21;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 10, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}