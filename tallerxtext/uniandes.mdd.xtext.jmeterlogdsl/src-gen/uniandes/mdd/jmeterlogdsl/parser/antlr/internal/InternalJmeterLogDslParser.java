package uniandes.mdd.jmeterlogdsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import uniandes.mdd.jmeterlogdsl.services.JmeterLogDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJmeterLogDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_SEPARATOR", "RULE_VALELEM", "RULE_NEWLINE", "'Aggregate'", "'ResponseLatency'"
    };
    public static final int T__8=8;
    public static final int T__7=7;
    public static final int RULE_NEWLINE=6;
    public static final int RULE_SEPARATOR=4;
    public static final int RULE_VALELEM=5;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalJmeterLogDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJmeterLogDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJmeterLogDslParser.tokenNames; }
    public String getGrammarFileName() { return "../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g"; }



     	private JmeterLogDslGrammarAccess grammarAccess;
     	
        public InternalJmeterLogDslParser(TokenStream input, JmeterLogDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Reporte";	
       	}
       	
       	@Override
       	protected JmeterLogDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:68:1: entryRuleReporte returns [EObject current=null] : iv_ruleReporte= ruleReporte EOF ;
    public final EObject entryRuleReporte() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReporte = null;


        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:69:2: (iv_ruleReporte= ruleReporte EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:70:2: iv_ruleReporte= ruleReporte EOF
            {
             newCompositeNode(grammarAccess.getReporteRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReporte_in_entryRuleReporte75);
            iv_ruleReporte=ruleReporte();

            state._fsp--;

             current =iv_ruleReporte; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReporte85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReporte"


    // $ANTLR start "ruleReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:77:1: ruleReporte returns [EObject current=null] : ( ( (lv_TipoReporte_0_0= ruleTipoReporte ) ) this_SEPARATOR_1= RULE_SEPARATOR ( (lv_Arquitectura_2_0= RULE_VALELEM ) ) this_NEWLINE_3= RULE_NEWLINE ( (lv_Encabezado_4_0= ruleEncabezado ) ) this_NEWLINE_5= RULE_NEWLINE ( (lv_Registros_6_0= ruleRegistro ) ) (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )* ) ;
    public final EObject ruleReporte() throws RecognitionException {
        EObject current = null;

        Token this_SEPARATOR_1=null;
        Token lv_Arquitectura_2_0=null;
        Token this_NEWLINE_3=null;
        Token this_NEWLINE_5=null;
        Token this_NEWLINE_7=null;
        Enumerator lv_TipoReporte_0_0 = null;

        EObject lv_Encabezado_4_0 = null;

        EObject lv_Registros_6_0 = null;

        EObject lv_Registros_8_0 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:80:28: ( ( ( (lv_TipoReporte_0_0= ruleTipoReporte ) ) this_SEPARATOR_1= RULE_SEPARATOR ( (lv_Arquitectura_2_0= RULE_VALELEM ) ) this_NEWLINE_3= RULE_NEWLINE ( (lv_Encabezado_4_0= ruleEncabezado ) ) this_NEWLINE_5= RULE_NEWLINE ( (lv_Registros_6_0= ruleRegistro ) ) (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:81:1: ( ( (lv_TipoReporte_0_0= ruleTipoReporte ) ) this_SEPARATOR_1= RULE_SEPARATOR ( (lv_Arquitectura_2_0= RULE_VALELEM ) ) this_NEWLINE_3= RULE_NEWLINE ( (lv_Encabezado_4_0= ruleEncabezado ) ) this_NEWLINE_5= RULE_NEWLINE ( (lv_Registros_6_0= ruleRegistro ) ) (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:81:1: ( ( (lv_TipoReporte_0_0= ruleTipoReporte ) ) this_SEPARATOR_1= RULE_SEPARATOR ( (lv_Arquitectura_2_0= RULE_VALELEM ) ) this_NEWLINE_3= RULE_NEWLINE ( (lv_Encabezado_4_0= ruleEncabezado ) ) this_NEWLINE_5= RULE_NEWLINE ( (lv_Registros_6_0= ruleRegistro ) ) (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:81:2: ( (lv_TipoReporte_0_0= ruleTipoReporte ) ) this_SEPARATOR_1= RULE_SEPARATOR ( (lv_Arquitectura_2_0= RULE_VALELEM ) ) this_NEWLINE_3= RULE_NEWLINE ( (lv_Encabezado_4_0= ruleEncabezado ) ) this_NEWLINE_5= RULE_NEWLINE ( (lv_Registros_6_0= ruleRegistro ) ) (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )*
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:81:2: ( (lv_TipoReporte_0_0= ruleTipoReporte ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:82:1: (lv_TipoReporte_0_0= ruleTipoReporte )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:82:1: (lv_TipoReporte_0_0= ruleTipoReporte )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:83:3: lv_TipoReporte_0_0= ruleTipoReporte
            {
             
            	        newCompositeNode(grammarAccess.getReporteAccess().getTipoReporteTipoReporteEnumRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleTipoReporte_in_ruleReporte131);
            lv_TipoReporte_0_0=ruleTipoReporte();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReporteRule());
            	        }
                   		set(
                   			current, 
                   			"TipoReporte",
                    		lv_TipoReporte_0_0, 
                    		"TipoReporte");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            this_SEPARATOR_1=(Token)match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_ruleReporte142); 
             
                newLeafNode(this_SEPARATOR_1, grammarAccess.getReporteAccess().getSEPARATORTerminalRuleCall_1()); 
                
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:103:1: ( (lv_Arquitectura_2_0= RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:104:1: (lv_Arquitectura_2_0= RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:104:1: (lv_Arquitectura_2_0= RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:105:3: lv_Arquitectura_2_0= RULE_VALELEM
            {
            lv_Arquitectura_2_0=(Token)match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_ruleReporte158); 

            			newLeafNode(lv_Arquitectura_2_0, grammarAccess.getReporteAccess().getArquitecturaVALELEMTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReporteRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"Arquitectura",
                    		lv_Arquitectura_2_0, 
                    		"VALELEM");
            	    

            }


            }

            this_NEWLINE_3=(Token)match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_ruleReporte174); 
             
                newLeafNode(this_NEWLINE_3, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_3()); 
                
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:125:1: ( (lv_Encabezado_4_0= ruleEncabezado ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:126:1: (lv_Encabezado_4_0= ruleEncabezado )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:126:1: (lv_Encabezado_4_0= ruleEncabezado )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:127:3: lv_Encabezado_4_0= ruleEncabezado
            {
             
            	        newCompositeNode(grammarAccess.getReporteAccess().getEncabezadoEncabezadoParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEncabezado_in_ruleReporte194);
            lv_Encabezado_4_0=ruleEncabezado();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReporteRule());
            	        }
                   		set(
                   			current, 
                   			"Encabezado",
                    		lv_Encabezado_4_0, 
                    		"Encabezado");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            this_NEWLINE_5=(Token)match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_ruleReporte205); 
             
                newLeafNode(this_NEWLINE_5, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_5()); 
                
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:147:1: ( (lv_Registros_6_0= ruleRegistro ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:148:1: (lv_Registros_6_0= ruleRegistro )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:148:1: (lv_Registros_6_0= ruleRegistro )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:149:3: lv_Registros_6_0= ruleRegistro
            {
             
            	        newCompositeNode(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_ruleReporte225);
            lv_Registros_6_0=ruleRegistro();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReporteRule());
            	        }
                   		add(
                   			current, 
                   			"Registros",
                    		lv_Registros_6_0, 
                    		"Registro");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:165:2: (this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_NEWLINE) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:165:3: this_NEWLINE_7= RULE_NEWLINE ( (lv_Registros_8_0= ruleRegistro ) )
            	    {
            	    this_NEWLINE_7=(Token)match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_ruleReporte237); 
            	     
            	        newLeafNode(this_NEWLINE_7, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_7_0()); 
            	        
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:169:1: ( (lv_Registros_8_0= ruleRegistro ) )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:170:1: (lv_Registros_8_0= ruleRegistro )
            	    {
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:170:1: (lv_Registros_8_0= ruleRegistro )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:171:3: lv_Registros_8_0= ruleRegistro
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_7_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_ruleReporte257);
            	    lv_Registros_8_0=ruleRegistro();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getReporteRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"Registros",
            	            		lv_Registros_8_0, 
            	            		"Registro");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReporte"


    // $ANTLR start "entryRuleRegistro"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:195:1: entryRuleRegistro returns [EObject current=null] : iv_ruleRegistro= ruleRegistro EOF ;
    public final EObject entryRuleRegistro() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRegistro = null;


        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:196:2: (iv_ruleRegistro= ruleRegistro EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:197:2: iv_ruleRegistro= ruleRegistro EOF
            {
             newCompositeNode(grammarAccess.getRegistroRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_entryRuleRegistro295);
            iv_ruleRegistro=ruleRegistro();

            state._fsp--;

             current =iv_ruleRegistro; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleRegistro305); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRegistro"


    // $ANTLR start "ruleRegistro"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:204:1: ruleRegistro returns [EObject current=null] : ( () ( (lv_Elementos_1_0= ruleElemento ) )? (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )* ) ;
    public final EObject ruleRegistro() throws RecognitionException {
        EObject current = null;

        Token this_SEPARATOR_2=null;
        EObject lv_Elementos_1_0 = null;

        EObject lv_Elementos_3_0 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:207:28: ( ( () ( (lv_Elementos_1_0= ruleElemento ) )? (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:208:1: ( () ( (lv_Elementos_1_0= ruleElemento ) )? (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:208:1: ( () ( (lv_Elementos_1_0= ruleElemento ) )? (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:208:2: () ( (lv_Elementos_1_0= ruleElemento ) )? (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )*
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:208:2: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:209:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getRegistroAccess().getRegistroAction_0(),
                        current);
                

            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:214:2: ( (lv_Elementos_1_0= ruleElemento ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_VALELEM) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:215:1: (lv_Elementos_1_0= ruleElemento )
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:215:1: (lv_Elementos_1_0= ruleElemento )
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:216:3: lv_Elementos_1_0= ruleElemento
                    {
                     
                    	        newCompositeNode(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleElemento_in_ruleRegistro360);
                    lv_Elementos_1_0=ruleElemento();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRegistroRule());
                    	        }
                           		add(
                           			current, 
                           			"Elementos",
                            		lv_Elementos_1_0, 
                            		"Elemento");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:232:3: (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_SEPARATOR) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:232:4: this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Elementos_3_0= ruleElemento ) )
            	    {
            	    this_SEPARATOR_2=(Token)match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_ruleRegistro373); 
            	     
            	        newLeafNode(this_SEPARATOR_2, grammarAccess.getRegistroAccess().getSEPARATORTerminalRuleCall_2_0()); 
            	        
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:236:1: ( (lv_Elementos_3_0= ruleElemento ) )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:237:1: (lv_Elementos_3_0= ruleElemento )
            	    {
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:237:1: (lv_Elementos_3_0= ruleElemento )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:238:3: lv_Elementos_3_0= ruleElemento
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleElemento_in_ruleRegistro393);
            	    lv_Elementos_3_0=ruleElemento();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRegistroRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"Elementos",
            	            		lv_Elementos_3_0, 
            	            		"Elemento");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRegistro"


    // $ANTLR start "entryRuleEncabezado"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:262:1: entryRuleEncabezado returns [EObject current=null] : iv_ruleEncabezado= ruleEncabezado EOF ;
    public final EObject entryRuleEncabezado() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEncabezado = null;


        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:263:2: (iv_ruleEncabezado= ruleEncabezado EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:264:2: iv_ruleEncabezado= ruleEncabezado EOF
            {
             newCompositeNode(grammarAccess.getEncabezadoRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEncabezado_in_entryRuleEncabezado431);
            iv_ruleEncabezado=ruleEncabezado();

            state._fsp--;

             current =iv_ruleEncabezado; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEncabezado441); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEncabezado"


    // $ANTLR start "ruleEncabezado"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:271:1: ruleEncabezado returns [EObject current=null] : ( () ( (lv_Columnas_1_0= ruleDescripcionColumna ) ) (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )* ) ;
    public final EObject ruleEncabezado() throws RecognitionException {
        EObject current = null;

        Token this_SEPARATOR_2=null;
        EObject lv_Columnas_1_0 = null;

        EObject lv_Columnas_3_0 = null;


         enterRule(); 
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:274:28: ( ( () ( (lv_Columnas_1_0= ruleDescripcionColumna ) ) (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:275:1: ( () ( (lv_Columnas_1_0= ruleDescripcionColumna ) ) (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:275:1: ( () ( (lv_Columnas_1_0= ruleDescripcionColumna ) ) (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:275:2: () ( (lv_Columnas_1_0= ruleDescripcionColumna ) ) (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )*
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:275:2: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:276:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getEncabezadoAccess().getEncabezadoAction_0(),
                        current);
                

            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:281:2: ( (lv_Columnas_1_0= ruleDescripcionColumna ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:282:1: (lv_Columnas_1_0= ruleDescripcionColumna )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:282:1: (lv_Columnas_1_0= ruleDescripcionColumna )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:283:3: lv_Columnas_1_0= ruleDescripcionColumna
            {
             
            	        newCompositeNode(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_ruleEncabezado496);
            lv_Columnas_1_0=ruleDescripcionColumna();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEncabezadoRule());
            	        }
                   		add(
                   			current, 
                   			"Columnas",
                    		lv_Columnas_1_0, 
                    		"DescripcionColumna");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:299:2: (this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_SEPARATOR) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:299:3: this_SEPARATOR_2= RULE_SEPARATOR ( (lv_Columnas_3_0= ruleDescripcionColumna ) )
            	    {
            	    this_SEPARATOR_2=(Token)match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_ruleEncabezado508); 
            	     
            	        newLeafNode(this_SEPARATOR_2, grammarAccess.getEncabezadoAccess().getSEPARATORTerminalRuleCall_2_0()); 
            	        
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:303:1: ( (lv_Columnas_3_0= ruleDescripcionColumna ) )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:304:1: (lv_Columnas_3_0= ruleDescripcionColumna )
            	    {
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:304:1: (lv_Columnas_3_0= ruleDescripcionColumna )
            	    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:305:3: lv_Columnas_3_0= ruleDescripcionColumna
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_ruleEncabezado528);
            	    lv_Columnas_3_0=ruleDescripcionColumna();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEncabezadoRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"Columnas",
            	            		lv_Columnas_3_0, 
            	            		"DescripcionColumna");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEncabezado"


    // $ANTLR start "entryRuleElemento"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:329:1: entryRuleElemento returns [EObject current=null] : iv_ruleElemento= ruleElemento EOF ;
    public final EObject entryRuleElemento() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElemento = null;


        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:330:2: (iv_ruleElemento= ruleElemento EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:331:2: iv_ruleElemento= ruleElemento EOF
            {
             newCompositeNode(grammarAccess.getElementoRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleElemento_in_entryRuleElemento566);
            iv_ruleElemento=ruleElemento();

            state._fsp--;

             current =iv_ruleElemento; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleElemento576); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElemento"


    // $ANTLR start "ruleElemento"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:338:1: ruleElemento returns [EObject current=null] : ( () ( (lv_ValorElemento_1_0= RULE_VALELEM ) ) ) ;
    public final EObject ruleElemento() throws RecognitionException {
        EObject current = null;

        Token lv_ValorElemento_1_0=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:341:28: ( ( () ( (lv_ValorElemento_1_0= RULE_VALELEM ) ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:342:1: ( () ( (lv_ValorElemento_1_0= RULE_VALELEM ) ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:342:1: ( () ( (lv_ValorElemento_1_0= RULE_VALELEM ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:342:2: () ( (lv_ValorElemento_1_0= RULE_VALELEM ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:342:2: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:343:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getElementoAccess().getElementoAction_0(),
                        current);
                

            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:348:2: ( (lv_ValorElemento_1_0= RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:349:1: (lv_ValorElemento_1_0= RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:349:1: (lv_ValorElemento_1_0= RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:350:3: lv_ValorElemento_1_0= RULE_VALELEM
            {
            lv_ValorElemento_1_0=(Token)match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_ruleElemento627); 

            			newLeafNode(lv_ValorElemento_1_0, grammarAccess.getElementoAccess().getValorElementoVALELEMTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getElementoRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"ValorElemento",
                    		lv_ValorElemento_1_0, 
                    		"VALELEM");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElemento"


    // $ANTLR start "entryRuleDescripcionColumna"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:374:1: entryRuleDescripcionColumna returns [EObject current=null] : iv_ruleDescripcionColumna= ruleDescripcionColumna EOF ;
    public final EObject entryRuleDescripcionColumna() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescripcionColumna = null;


        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:375:2: (iv_ruleDescripcionColumna= ruleDescripcionColumna EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:376:2: iv_ruleDescripcionColumna= ruleDescripcionColumna EOF
            {
             newCompositeNode(grammarAccess.getDescripcionColumnaRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_entryRuleDescripcionColumna668);
            iv_ruleDescripcionColumna=ruleDescripcionColumna();

            state._fsp--;

             current =iv_ruleDescripcionColumna; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDescripcionColumna678); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescripcionColumna"


    // $ANTLR start "ruleDescripcionColumna"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:383:1: ruleDescripcionColumna returns [EObject current=null] : ( () ( (lv_Descripcion_1_0= RULE_VALELEM ) ) ) ;
    public final EObject ruleDescripcionColumna() throws RecognitionException {
        EObject current = null;

        Token lv_Descripcion_1_0=null;

         enterRule(); 
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:386:28: ( ( () ( (lv_Descripcion_1_0= RULE_VALELEM ) ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:387:1: ( () ( (lv_Descripcion_1_0= RULE_VALELEM ) ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:387:1: ( () ( (lv_Descripcion_1_0= RULE_VALELEM ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:387:2: () ( (lv_Descripcion_1_0= RULE_VALELEM ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:387:2: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:388:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getDescripcionColumnaAccess().getDescripcionColumnaAction_0(),
                        current);
                

            }

            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:393:2: ( (lv_Descripcion_1_0= RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:394:1: (lv_Descripcion_1_0= RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:394:1: (lv_Descripcion_1_0= RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:395:3: lv_Descripcion_1_0= RULE_VALELEM
            {
            lv_Descripcion_1_0=(Token)match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_ruleDescripcionColumna729); 

            			newLeafNode(lv_Descripcion_1_0, grammarAccess.getDescripcionColumnaAccess().getDescripcionVALELEMTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDescripcionColumnaRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"Descripcion",
                    		lv_Descripcion_1_0, 
                    		"VALELEM");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescripcionColumna"


    // $ANTLR start "ruleTipoReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:419:1: ruleTipoReporte returns [Enumerator current=null] : ( (enumLiteral_0= 'Aggregate' ) | (enumLiteral_1= 'ResponseLatency' ) ) ;
    public final Enumerator ruleTipoReporte() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:421:28: ( ( (enumLiteral_0= 'Aggregate' ) | (enumLiteral_1= 'ResponseLatency' ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:422:1: ( (enumLiteral_0= 'Aggregate' ) | (enumLiteral_1= 'ResponseLatency' ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:422:1: ( (enumLiteral_0= 'Aggregate' ) | (enumLiteral_1= 'ResponseLatency' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==7) ) {
                alt5=1;
            }
            else if ( (LA5_0==8) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:422:2: (enumLiteral_0= 'Aggregate' )
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:422:2: (enumLiteral_0= 'Aggregate' )
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:422:4: enumLiteral_0= 'Aggregate'
                    {
                    enumLiteral_0=(Token)match(input,7,FollowSets000.FOLLOW_7_in_ruleTipoReporte784); 

                            current = grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:428:6: (enumLiteral_1= 'ResponseLatency' )
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:428:6: (enumLiteral_1= 'ResponseLatency' )
                    // ../uniandes.mdd.xtext.jmeterlogdsl/src-gen/uniandes/mdd/jmeterlogdsl/parser/antlr/internal/InternalJmeterLogDsl.g:428:8: enumLiteral_1= 'ResponseLatency'
                    {
                    enumLiteral_1=(Token)match(input,8,FollowSets000.FOLLOW_8_in_ruleTipoReporte801); 

                            current = grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTipoReporte"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleReporte_in_entryRuleReporte75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReporte85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTipoReporte_in_ruleReporte131 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_ruleReporte142 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_ruleReporte158 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_ruleReporte174 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_ruleEncabezado_in_ruleReporte194 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_ruleReporte205 = new BitSet(new long[]{0x0000000000000070L});
        public static final BitSet FOLLOW_ruleRegistro_in_ruleReporte225 = new BitSet(new long[]{0x0000000000000042L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_ruleReporte237 = new BitSet(new long[]{0x0000000000000070L});
        public static final BitSet FOLLOW_ruleRegistro_in_ruleReporte257 = new BitSet(new long[]{0x0000000000000042L});
        public static final BitSet FOLLOW_ruleRegistro_in_entryRuleRegistro295 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleRegistro305 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElemento_in_ruleRegistro360 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_ruleRegistro373 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_ruleElemento_in_ruleRegistro393 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_ruleEncabezado_in_entryRuleEncabezado431 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEncabezado441 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_ruleEncabezado496 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_ruleEncabezado508 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_ruleEncabezado528 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_ruleElemento_in_entryRuleElemento566 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleElemento576 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_ruleElemento627 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_entryRuleDescripcionColumna668 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDescripcionColumna678 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_ruleDescripcionColumna729 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_7_in_ruleTipoReporte784 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_8_in_ruleTipoReporte801 = new BitSet(new long[]{0x0000000000000002L});
    }


}