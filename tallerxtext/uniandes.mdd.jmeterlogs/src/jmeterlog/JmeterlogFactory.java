/**
 */
package jmeterlog;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see jmeterlog.JmeterlogPackage
 * @generated
 */
public interface JmeterlogFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JmeterlogFactory eINSTANCE = jmeterlog.impl.JmeterlogFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Reporte</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reporte</em>'.
	 * @generated
	 */
	Reporte createReporte();

	/**
	 * Returns a new object of class '<em>Registro</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Registro</em>'.
	 * @generated
	 */
	Registro createRegistro();

	/**
	 * Returns a new object of class '<em>Elemento</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elemento</em>'.
	 * @generated
	 */
	Elemento createElemento();

	/**
	 * Returns a new object of class '<em>Descripcion Columna</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Descripcion Columna</em>'.
	 * @generated
	 */
	DescripcionColumna createDescripcionColumna();

	/**
	 * Returns a new object of class '<em>Encabezado</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Encabezado</em>'.
	 * @generated
	 */
	Encabezado createEncabezado();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	JmeterlogPackage getJmeterlogPackage();

} //JmeterlogFactory
