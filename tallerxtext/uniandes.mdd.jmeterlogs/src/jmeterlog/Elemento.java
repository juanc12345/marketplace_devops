/**
 */
package jmeterlog;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elemento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.Elemento#getValorElemento <em>Valor Elemento</em>}</li>
 * </ul>
 *
 * @see jmeterlog.JmeterlogPackage#getElemento()
 * @model
 * @generated
 */
public interface Elemento extends EObject {
	/**
	 * Returns the value of the '<em><b>Valor Elemento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valor Elemento</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valor Elemento</em>' attribute.
	 * @see #setValorElemento(String)
	 * @see jmeterlog.JmeterlogPackage#getElemento_ValorElemento()
	 * @model
	 * @generated
	 */
	String getValorElemento();

	/**
	 * Sets the value of the '{@link jmeterlog.Elemento#getValorElemento <em>Valor Elemento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Valor Elemento</em>' attribute.
	 * @see #getValorElemento()
	 * @generated
	 */
	void setValorElemento(String value);

} // Elemento
