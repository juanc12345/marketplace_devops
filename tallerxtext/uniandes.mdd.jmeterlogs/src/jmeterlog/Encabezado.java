/**
 */
package jmeterlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Encabezado</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.Encabezado#getColumnas <em>Columnas</em>}</li>
 * </ul>
 *
 * @see jmeterlog.JmeterlogPackage#getEncabezado()
 * @model
 * @generated
 */
public interface Encabezado extends EObject {
	/**
	 * Returns the value of the '<em><b>Columnas</b></em>' containment reference list.
	 * The list contents are of type {@link jmeterlog.DescripcionColumna}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columnas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columnas</em>' containment reference list.
	 * @see jmeterlog.JmeterlogPackage#getEncabezado_Columnas()
	 * @model containment="true"
	 * @generated
	 */
	EList<DescripcionColumna> getColumnas();

} // Encabezado
