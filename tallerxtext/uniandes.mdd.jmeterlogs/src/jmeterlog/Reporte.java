/**
 */
package jmeterlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reporte</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.Reporte#getTipoReporte <em>Tipo Reporte</em>}</li>
 *   <li>{@link jmeterlog.Reporte#getArquitectura <em>Arquitectura</em>}</li>
 *   <li>{@link jmeterlog.Reporte#getRegistros <em>Registros</em>}</li>
 *   <li>{@link jmeterlog.Reporte#getEncabezado <em>Encabezado</em>}</li>
 * </ul>
 *
 * @see jmeterlog.JmeterlogPackage#getReporte()
 * @model
 * @generated
 */
public interface Reporte extends EObject {
	/**
	 * Returns the value of the '<em><b>Tipo Reporte</b></em>' attribute.
	 * The default value is <code>"Aggregate"</code>.
	 * The literals are from the enumeration {@link jmeterlog.TipoReporte}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tipo Reporte</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tipo Reporte</em>' attribute.
	 * @see jmeterlog.TipoReporte
	 * @see #setTipoReporte(TipoReporte)
	 * @see jmeterlog.JmeterlogPackage#getReporte_TipoReporte()
	 * @model default="Aggregate"
	 * @generated
	 */
	TipoReporte getTipoReporte();

	/**
	 * Sets the value of the '{@link jmeterlog.Reporte#getTipoReporte <em>Tipo Reporte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tipo Reporte</em>' attribute.
	 * @see jmeterlog.TipoReporte
	 * @see #getTipoReporte()
	 * @generated
	 */
	void setTipoReporte(TipoReporte value);

	/**
	 * Returns the value of the '<em><b>Arquitectura</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arquitectura</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arquitectura</em>' attribute.
	 * @see #setArquitectura(String)
	 * @see jmeterlog.JmeterlogPackage#getReporte_Arquitectura()
	 * @model
	 * @generated
	 */
	String getArquitectura();

	/**
	 * Sets the value of the '{@link jmeterlog.Reporte#getArquitectura <em>Arquitectura</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arquitectura</em>' attribute.
	 * @see #getArquitectura()
	 * @generated
	 */
	void setArquitectura(String value);

	/**
	 * Returns the value of the '<em><b>Registros</b></em>' containment reference list.
	 * The list contents are of type {@link jmeterlog.Registro}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registros</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registros</em>' containment reference list.
	 * @see jmeterlog.JmeterlogPackage#getReporte_Registros()
	 * @model containment="true"
	 * @generated
	 */
	EList<Registro> getRegistros();

	/**
	 * Returns the value of the '<em><b>Encabezado</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Encabezado</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Encabezado</em>' containment reference.
	 * @see #setEncabezado(Encabezado)
	 * @see jmeterlog.JmeterlogPackage#getReporte_Encabezado()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Encabezado getEncabezado();

	/**
	 * Sets the value of the '{@link jmeterlog.Reporte#getEncabezado <em>Encabezado</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Encabezado</em>' containment reference.
	 * @see #getEncabezado()
	 * @generated
	 */
	void setEncabezado(Encabezado value);

} // Reporte
