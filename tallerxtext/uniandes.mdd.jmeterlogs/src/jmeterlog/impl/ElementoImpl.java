/**
 */
package jmeterlog.impl;

import jmeterlog.Elemento;
import jmeterlog.JmeterlogPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Elemento</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.impl.ElementoImpl#getValorElemento <em>Valor Elemento</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementoImpl extends MinimalEObjectImpl.Container implements Elemento {
	/**
	 * The default value of the '{@link #getValorElemento() <em>Valor Elemento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValorElemento()
	 * @generated
	 * @ordered
	 */
	protected static final String VALOR_ELEMENTO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValorElemento() <em>Valor Elemento</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValorElemento()
	 * @generated
	 * @ordered
	 */
	protected String valorElemento = VALOR_ELEMENTO_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JmeterlogPackage.Literals.ELEMENTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValorElemento() {
		return valorElemento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValorElemento(String newValorElemento) {
		String oldValorElemento = valorElemento;
		valorElemento = newValorElemento;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JmeterlogPackage.ELEMENTO__VALOR_ELEMENTO, oldValorElemento, valorElemento));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JmeterlogPackage.ELEMENTO__VALOR_ELEMENTO:
				return getValorElemento();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JmeterlogPackage.ELEMENTO__VALOR_ELEMENTO:
				setValorElemento((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JmeterlogPackage.ELEMENTO__VALOR_ELEMENTO:
				setValorElemento(VALOR_ELEMENTO_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JmeterlogPackage.ELEMENTO__VALOR_ELEMENTO:
				return VALOR_ELEMENTO_EDEFAULT == null ? valorElemento != null : !VALOR_ELEMENTO_EDEFAULT.equals(valorElemento);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ValorElemento: ");
		result.append(valorElemento);
		result.append(')');
		return result.toString();
	}

} //ElementoImpl
