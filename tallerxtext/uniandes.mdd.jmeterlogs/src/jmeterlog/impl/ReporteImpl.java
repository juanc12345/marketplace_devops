/**
 */
package jmeterlog.impl;

import java.util.Collection;

import jmeterlog.Encabezado;
import jmeterlog.JmeterlogPackage;
import jmeterlog.Registro;
import jmeterlog.Reporte;
import jmeterlog.TipoReporte;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reporte</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.impl.ReporteImpl#getTipoReporte <em>Tipo Reporte</em>}</li>
 *   <li>{@link jmeterlog.impl.ReporteImpl#getArquitectura <em>Arquitectura</em>}</li>
 *   <li>{@link jmeterlog.impl.ReporteImpl#getRegistros <em>Registros</em>}</li>
 *   <li>{@link jmeterlog.impl.ReporteImpl#getEncabezado <em>Encabezado</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReporteImpl extends MinimalEObjectImpl.Container implements Reporte {
	/**
	 * The default value of the '{@link #getTipoReporte() <em>Tipo Reporte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTipoReporte()
	 * @generated
	 * @ordered
	 */
	protected static final TipoReporte TIPO_REPORTE_EDEFAULT = TipoReporte.AGGREGATE;

	/**
	 * The cached value of the '{@link #getTipoReporte() <em>Tipo Reporte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTipoReporte()
	 * @generated
	 * @ordered
	 */
	protected TipoReporte tipoReporte = TIPO_REPORTE_EDEFAULT;

	/**
	 * The default value of the '{@link #getArquitectura() <em>Arquitectura</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArquitectura()
	 * @generated
	 * @ordered
	 */
	protected static final String ARQUITECTURA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArquitectura() <em>Arquitectura</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArquitectura()
	 * @generated
	 * @ordered
	 */
	protected String arquitectura = ARQUITECTURA_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRegistros() <em>Registros</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegistros()
	 * @generated
	 * @ordered
	 */
	protected EList<Registro> registros;

	/**
	 * The cached value of the '{@link #getEncabezado() <em>Encabezado</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEncabezado()
	 * @generated
	 * @ordered
	 */
	protected Encabezado encabezado;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReporteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JmeterlogPackage.Literals.REPORTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TipoReporte getTipoReporte() {
		return tipoReporte;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTipoReporte(TipoReporte newTipoReporte) {
		TipoReporte oldTipoReporte = tipoReporte;
		tipoReporte = newTipoReporte == null ? TIPO_REPORTE_EDEFAULT : newTipoReporte;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JmeterlogPackage.REPORTE__TIPO_REPORTE, oldTipoReporte, tipoReporte));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArquitectura() {
		return arquitectura;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArquitectura(String newArquitectura) {
		String oldArquitectura = arquitectura;
		arquitectura = newArquitectura;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JmeterlogPackage.REPORTE__ARQUITECTURA, oldArquitectura, arquitectura));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Registro> getRegistros() {
		if (registros == null) {
			registros = new EObjectContainmentEList<Registro>(Registro.class, this, JmeterlogPackage.REPORTE__REGISTROS);
		}
		return registros;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Encabezado getEncabezado() {
		return encabezado;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEncabezado(Encabezado newEncabezado, NotificationChain msgs) {
		Encabezado oldEncabezado = encabezado;
		encabezado = newEncabezado;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JmeterlogPackage.REPORTE__ENCABEZADO, oldEncabezado, newEncabezado);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEncabezado(Encabezado newEncabezado) {
		if (newEncabezado != encabezado) {
			NotificationChain msgs = null;
			if (encabezado != null)
				msgs = ((InternalEObject)encabezado).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JmeterlogPackage.REPORTE__ENCABEZADO, null, msgs);
			if (newEncabezado != null)
				msgs = ((InternalEObject)newEncabezado).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JmeterlogPackage.REPORTE__ENCABEZADO, null, msgs);
			msgs = basicSetEncabezado(newEncabezado, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JmeterlogPackage.REPORTE__ENCABEZADO, newEncabezado, newEncabezado));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JmeterlogPackage.REPORTE__REGISTROS:
				return ((InternalEList<?>)getRegistros()).basicRemove(otherEnd, msgs);
			case JmeterlogPackage.REPORTE__ENCABEZADO:
				return basicSetEncabezado(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JmeterlogPackage.REPORTE__TIPO_REPORTE:
				return getTipoReporte();
			case JmeterlogPackage.REPORTE__ARQUITECTURA:
				return getArquitectura();
			case JmeterlogPackage.REPORTE__REGISTROS:
				return getRegistros();
			case JmeterlogPackage.REPORTE__ENCABEZADO:
				return getEncabezado();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JmeterlogPackage.REPORTE__TIPO_REPORTE:
				setTipoReporte((TipoReporte)newValue);
				return;
			case JmeterlogPackage.REPORTE__ARQUITECTURA:
				setArquitectura((String)newValue);
				return;
			case JmeterlogPackage.REPORTE__REGISTROS:
				getRegistros().clear();
				getRegistros().addAll((Collection<? extends Registro>)newValue);
				return;
			case JmeterlogPackage.REPORTE__ENCABEZADO:
				setEncabezado((Encabezado)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JmeterlogPackage.REPORTE__TIPO_REPORTE:
				setTipoReporte(TIPO_REPORTE_EDEFAULT);
				return;
			case JmeterlogPackage.REPORTE__ARQUITECTURA:
				setArquitectura(ARQUITECTURA_EDEFAULT);
				return;
			case JmeterlogPackage.REPORTE__REGISTROS:
				getRegistros().clear();
				return;
			case JmeterlogPackage.REPORTE__ENCABEZADO:
				setEncabezado((Encabezado)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JmeterlogPackage.REPORTE__TIPO_REPORTE:
				return tipoReporte != TIPO_REPORTE_EDEFAULT;
			case JmeterlogPackage.REPORTE__ARQUITECTURA:
				return ARQUITECTURA_EDEFAULT == null ? arquitectura != null : !ARQUITECTURA_EDEFAULT.equals(arquitectura);
			case JmeterlogPackage.REPORTE__REGISTROS:
				return registros != null && !registros.isEmpty();
			case JmeterlogPackage.REPORTE__ENCABEZADO:
				return encabezado != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (TipoReporte: ");
		result.append(tipoReporte);
		result.append(", Arquitectura: ");
		result.append(arquitectura);
		result.append(')');
		return result.toString();
	}

} //ReporteImpl
