/**
 */
package jmeterlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Registro</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link jmeterlog.Registro#getElementos <em>Elementos</em>}</li>
 * </ul>
 *
 * @see jmeterlog.JmeterlogPackage#getRegistro()
 * @model
 * @generated
 */
public interface Registro extends EObject {
	/**
	 * Returns the value of the '<em><b>Elementos</b></em>' containment reference list.
	 * The list contents are of type {@link jmeterlog.Elemento}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elementos</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elementos</em>' containment reference list.
	 * @see jmeterlog.JmeterlogPackage#getRegistro_Elementos()
	 * @model containment="true"
	 * @generated
	 */
	EList<Elemento> getElementos();

} // Registro
